.PHONY: build run test clean

build:
	echo '.git' > .dockerignore
	cat .gitignore >> .dockerignore
	docker-compose -f docker-compose.yaml build

run:
	trap 'make clean' EXIT; docker-compose -f docker-compose.yaml up

test:
	trap 'make clean' EXIT; docker-compose -f docker-compose.yaml -f docker-compose.test.yaml run frontend test
	trap 'make clean' EXIT; docker-compose -f docker-compose.yaml -f docker-compose.test.yaml run backend test

test_watch:
	trap 'make clean' EXIT; docker-compose -f docker-compose.yaml -f docker-compose.test.yaml run frontend test_watch

clean:
	docker-compose -f docker-compose.yaml down --remove-orphans
