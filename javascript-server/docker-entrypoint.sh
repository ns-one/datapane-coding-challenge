#!/bin/bash

set -e

# Commands available using `docker-compose run frontend [COMMAND]`
case "$1" in
    node)
        node
    ;;
    test_watch)
        sh test_it.sh
    ;;
    test)
        sh test_it-once.sh
    ;;
    *)
        echo "Running Server..."
        sh run_it.sh
    ;;
esac
