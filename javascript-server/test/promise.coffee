{Promise, queue} = require '../app/lib/promise'
{spy, thread} = require '../app/lib/extras'


# describe.only 'Promise', ->
describe 'Promise', ->
  p    = null
  fire = null
  rejected = null
  resolved = null

  beforeEach (done) ->
    rejected = spy ->
    resolved = spy ->

    p = new Promise (reject, resolve) ->
      fire = (data) ->
        if data instanceof Error
          rejected(data)
          reject(data)

        else
          resolved(data)
          resolve(data)

      done()


  describe 'instance', ->
    it 'should have nested methods and statuses', ->
      p.status.should.equal 'pending'
      p.then.should.be.a.Function
      return


    it 'should change status on resolution', ->
      fire('ahoj')
      p.status.should.equal 'resolved'

      resolved.counter.should.equal 1
      rejected.counter.should.equal 0
      return


    it 'should change status on rejection', ->
      fire(new Error('chyba'))
      p.status.should.equal 'rejected'

      resolved.counter.should.equal 0
      rejected.counter.should.equal 1
      return


    describe 'then', ->
      it 'should fire function on resolution', (done) ->
        cb = spy (error, data...) ->
          (error is null).should.equal true
          data[0].should.equal 'ahoj'
          return

        pp = p.then(cb)
        fire('ahoj')

        pp.then ->
          cb.counter.should.equal 1
          done()


      it 'should fire function on rejection', (done) ->
        cb = spy (error, data...) ->
          error.should.be.a.Error
          data.should.eql []
          return

        pp = p.then(cb)
        fire(new Error('DummyErrorMessage'))

        pp.then ->
          cb.counter.should.equal 1
          done()

      it.skip 'should throw error when not awaited', (done) ->
        fire('ahoj')
        (-> p.then(->
          throw new Error('DummyErrorMessage')
          done()
        )).should.throw()
        return

      it 'should return error when awaited', (done) ->
        fire('ahoj')
        p.then(-> throw new Error('DummyErrorMessage'))

        .then (error, data...) ->
          error.should.be.a.Error
          data.should.eql []
          done()


      describe 'chained then', ->
        it 'should return another promise or similar', ->
          (typeof p.then(->)?.then).should.equal 'function'
          return


        it 'should handle error of callback function', (done) ->
          cb = spy (error, data...) ->
            (error is null).should.equal true
            data[0].should.equal 'ahoj'
            throw new Error('DummyErrorMessage')

          cb2 = spy (error, data...) ->
            error.should.be.a.Error
            data.should.eql []
            return

          pp = p.then(cb).then(cb2)
          fire('ahoj')

          pp.then ->
            cb.counter.should.equal 1
            cb2.counter.should.equal 1
            done()


        it 'should wait after given cb is resolved', (done) ->
          cb = spy ->

          pp = p
            .then (error, data) ->
              new Promise (reject, resolve) ->
                cb()
                resolve('testovani resolveni', data)

            .then (error, msg, data) ->
              cb()
              (error is null).should.equal true
              msg.should.equal 'testovani resolveni'
              data.should.equal 'ttxx'
              return

          fire('ttxx')

          pp.then ->
            cb.counter.should.equal 2
            done()


        it 'cascade Promise (if there\'s nothing but single Promise)', (done) ->
          t0 = new Date

          fire new Promise (rj, rs) ->
            thread 5, ->
              rs(['p1', 'p2'])

          p.then (err, data) ->
            data.should.eql(['p1', 'p2'])
            (5 <= ((new Date) - t0) < 10).should.be.equal true
            done()


        it 'no cascade, when there\'re more items to resolve', (done) ->
          t0 = new Date
          p2 = new Promise (rj, rs) ->
            thread 5, ->
              rs('p1.p2')

          fire ['p1', p2]

          p.then (err, data) ->
            data.should.eql(['p1', p2])
            (((new Date) - t0) < 5).should.be.equal true
            done()


  describe 'class', ->
    describe 'wrap', ->
      it 'should behave same as normal promise', (done) ->
        p1 = Promise.wrap('ahoj').then (error, data...) ->
          data[0].should.equal 'ahoj'
          'p1'

        p2 = Promise.wrap(p).then (error, data...) ->
          data[0].should.equal 'ahojte!'
          'p2'

        fire('ahojte!')

        Promise.all(p1, p2).then (err, values...) ->
          values.should.eql([['p1'], ['p2']])
          done()


      it 'should not create new promise, when given a promise', ->
        p2 = Promise.wrap(p)
        (p2 is p).should.equal true
        return


    it 'should wait for all promises', (done) ->
      p2 = new Promise (reject, resolve) ->
        thread 5, ->
          resolve('dalsi')

      cb = spy ->
      Promise.all(p, 'ahoj', p, p2).then (error, a, b, c, d) ->
        (error is null).should.equal true
        a.should.eql ['xxaa']
        b.should.eql ['ahoj']
        c.should.eql ['xxaa']
        d.should.eql ['dalsi']
        cb()

      fire('xxaa')
      thread 5, ->
        cb.counter.should.equal 1
        done()


    it 'should wait for all, even empty, promises', (done) ->
      p2 = new Promise (reject, resolve) ->
        thread 5, ->
          resolve('dalsi')

      cb = spy ->
      Promise.all().then (error, res...) ->
        (error is null).should.equal true
        res.should.eql []
        cb()

      fire('xxaa')
      thread 5, ->
        cb.counter.should.equal 1
        done()


    it 'should resolve racing promises', (done) ->
      cb = spy ->

      p2 = new Promise (reject, resolve) ->
        thread 5, ->
          resolve('text')

      Promise.race(p2, 'ahoj', p, 'dalsi').then (error, res) ->
        (error is null).should.equal true
        res.should.equal 'ahoj'
        cb()

      fire('xxaa')
      thread 5, ->
        cb.counter.should.equal 1
        done()


    it.skip 'TODO: should chain given promises', ->


describe 'queue', ->
  it 'has same number', (done) ->
    array = []
    cb = spy ->

    c  = 0
    t0 = new Date

    for i in range(5)
      queue 'test:range', ->
        new Promise (reject, resolve) ->
          thread 5, ->
            c++
            cb()
            array.push(i)
            resolve()

            if c is 5
              cb.counter.should.equal 5
              array.should.eql [4, 4, 4, 4, 4]
              (25 <= ((new Date) - t0)).should.equal true
              done()


  it 'is as one would expect', (done) ->
    array = []
    cb = spy ->

    c  = 0
    t0 = new Date

    for i in range(5)
      do (i) -> queue 'test:range', ->
        new Promise (reject, resolve) ->
          thread 5, ->
            c++
            cb()
            array.push(i)
            resolve()

            if c is 5
              cb.counter.should.equal 5
              array.should.eql range(5)
              (25 <= ((new Date) - t0)).should.equal true
              done()


  it 'is even easier', (done) ->
    array = []
    cb = spy ->

    c  = 0
    t0 = new Date

    for i in range(5)
      do (i) -> queue 'test:range', (q_done) ->
        thread 5, ->
          c++
          cb()
          array.push(i)
          q_done()

          if c is 5
            cb.counter.should.equal 5
            array.should.eql range(5)
            (25 <= ((new Date) - t0)).should.equal true
            done()


  it.skip 'has priority queue', (done) ->
    array = []
    cb = spy ->

    c  = 0
    t0 = new Date
    queue_key = 'test:range'

    for i in range(5)
      do (i) ->
        if 2 < i
          queue_key = '!' + queue_key

        queue queue_key, (q_done) ->
          thread 5, ->
            c++
            cb()
            array.push(i)
            q_done()

            if c is 5
              cb.counter.should.equal 5
              array.should.eql [0, 4, 3, 1, 2]
              # 0 je sync, zbytek se nahazi do fronty
              # a jede se podle priority
              #
              # test, jestli to trvalo alespoň těch 25 ms (5x 5 ms)
              unless cond = (25 <= ((new Date) - t0))
                console.log {cond, left: 25, right: ((new Date) - t0)}

              cond.should.equal true
              done()


  it.skip 'can be (un)paused', (done) ->
    array = []
    cb = spy ->

    c  = 0
    t0 = new Date
    queue_key = 'test:range'
    pauser = queue.pauser(queue_key)

    for i in range(5)
      do (i) ->
        if 2 < i
          queue_key = '!' + queue_key

        queue queue_key, (q_done) ->
          thread 5, ->
            c++
            cb()
            array.push(i)
            q_done()

            if c is 5
              cb.counter.should.equal 5
              array.should.eql [4, 3, 0, 1, 2]
              # 3 se hodí na začátek a pak to tam hodí i 4
              #
              # test, jestli to trvalo alespoň těch 25 ms (5x 5 ms)
              (25 <= ((new Date) - t0)).should.equal true
              done()

    pauser()


  describe '.promise', ->
    it 'should return Promise', ->
      p = queue.promise 'test:promise', ->
      (p instanceof Promise).should.equal true
      return


    it 'should resolve promise when queue item is evaluated', (done) ->
      computed_order = []

      p1 = queue.promise 'test:promise', (q_done) ->
        thread 5, ->
          computed_order.push(1)
          q_done()

      p2 = queue.promise 'test:promise', (q_done) ->
        thread 5, ->
          computed_order.push(3)
          q_done()

      p1.then(-> computed_order.push(2))
      p2.then(-> computed_order.push(4))
      .then ->
        computed_order.should.eql [1, 2, 3, 4]
        done()


    it.skip 'TODO: throws error caught in chained Promise', (done) ->
      console.log 'startxxx'

      p = queue.promise 'test:promise:error', ->
        new Promise (reject, resolve) ->
          console.log 'trow...'
          throw new Error('DummyErrorMessage')

      p.then (error, args...) ->
        error.should.be.a.Error
        data.should.eql []
        done()
