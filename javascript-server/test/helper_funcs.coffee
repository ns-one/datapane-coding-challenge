{apply_to_scope} = require '../app/lib/helper_funcs'
{distinct, flatten} = require '../app/lib/extras'


describe 'apply_to_scope', ->
  a       = null
  changes = null
  res     = null

  beforeEach ->
    a =
      $$id: 2
      array: [1, 2, 3]
      key1: 'val1'
      key2:
        key21: 'val21'
        key22: [
          'a',
          'b',
          {
            $$id: 3
            key2221: 'val2221'
            key2222: 'val2222'
          }
        ]
      key3: 'val3'

    changes =
      array: [2, 3, 4]
      key1: 'novy key1'
      key2:
        key21: 'dalsi val'
        key22:
          '1': 'c'
          '2':
            key2221: 'novy key2221'

    res = apply_to_scope(changes, a)
    res = flatten(res)
    res = distinct(res)

  describe 'applying changes', ->
    it 'should be able to apply all changes to given obj', ->
      a.key1.should.equal('novy key1')
      return

    it 'should apply changes also to nested objs', ->
      a.key2.key21.should.equal('dalsi val')
      a.key2.key22[1].should.equal('c')
      a.key2.key22[2].key2221.should.equal('novy key2221')
      return

    it 'should not touch other fields', ->
      a.key3.should.equal('val3')
      a.key2.key22[0].should.equal('a')
      a.key2.key22[2].key2222.should.equal('val2222')
      return

    it 'should be able to override whole obj if needed', ->
      changes =
        key2: [1, 2, 3]

      apply_to_scope(changes, a)

      (a.key2.key22?).should.equal(false)
      a.key2[1].should.equal(2)
      return

  it 'should return correct scopes', ->
    res[0].should.equal(a)
    res[1].should.equal(a.key2.key22[2])
    return

  it 'should not override value with same value', ->
    res = apply_to_scope(changes, a)
    res = flatten(res)
    res.should.eql([])
    return