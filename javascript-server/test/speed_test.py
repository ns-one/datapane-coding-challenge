import time


# t0 = None

# class Iterator:
#   def __init__(self, timeout=2):
#     self.timeout = timeout

#   def __iter__(self):
#     return self

#   def next(self):
#     global t0
#     time.sleep(self.timeout)
#     t0 = time.time()
#     return True


# total = 0
# i     = 0
# for it in Iterator(.1):
#   if it is True:
#     i     += 1
#     total += (time.time() - t0)

#   if 5 < i:
#     break


# print {'total': total, 'i': i}






t0 = time.time()


def memoize(f):
  cache = {}
  def decorated_function(*args):
    if args in cache:
      return cache[args]
    else:
      cache[args] = f(*args)
      return cache[args]

  return decorated_function

@memoize
def fibonnaci(n):
  if n <= 2: return 1
  else:      return fibonnaci(n - 2) + fibonnaci(n - 1)


def ffn(n):
  cache = {}
  i = 0
  while i < n:
    i += 1
    if i <= 2:
      cache[i] = 1

    else:
      cache[i] = cache[i - 2] + cache[i - 1]

  return cache[i]


# f  = fibonnaci(50)
f  = 0
ff = ffn(5000)

# i = 0
# while i < 10:
#   i += 1
#   f  = fibonnaci(35)

t1 = time.time() - t0

print {'f': f, 'ff': ff, 't1': t1}