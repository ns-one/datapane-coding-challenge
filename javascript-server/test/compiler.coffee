transform = require '../.BnX/coffee-react-transform'

          # TODO
          # <TaskItem id="x.tasks.{i}" />

describe 'testing compiling functions', ->
  it 'should eval custom coffee code', ->
    transform('
      <div>
        {this.$$scope.$$render.__component(moje("xxa"), this.$$scope, null)}
      </div>
    ')
    .should.equal('
      this.$$scope.$$render.__element("div", this.$$scope.$$params.pop(), null, " ", (this.$$scope.$$render.__component(moje("xxa"), this.$$scope, null)), " ")
    ')
    return

  it 'should compile dom elements to fn calls with attrs', ->
    transform('
      <div class="neco" atts="dalsi"></div>
    ')
    .should.equal('
      this.$$scope.$$render.__element("div", this.$$scope.$$params.pop(), {"class": "neco", "atts": "dalsi"})
    ')
    return


  it 'should compile correctly inner expressions', ->
    transform('
      <TaskItem scope={@neco} id={"x.tasks.#{i}"} />
    ')
    .should.equal('
      this.$$scope.$$render.__component(TaskItem, this.$$scope, {"scope": (@neco), "id": ("x.tasks.#{i}")})
    ')
    return

  describe 'components', ->
    it 'should compile components', ->
      transform('
        <Komponenta></Komponenta>
      ')
      .should.equal('
        this.$$scope.$$render.__component(Komponenta, this.$$scope, null)
      ')
      return

    it 'should pass class over components', ->
      transform('
        <Komponenta class="trida"></Komponenta>
      ')
      .should.equal('
        this.$$scope.$$render.__component(Komponenta, this.$$scope, {"class": "trida"})
      ')
      return

    it 'should pass conditional classes over components', ->
      transform('
        <Komponenta class={\'trida selected\': true, trida: false}></Komponenta>
      ')
      .should.equal('
        this.$$scope.$$render.__component(Komponenta, this.$$scope, {"class": (\'trida selected\': true, trida: false)})
      ')
      return

    it 'should be able to insert inner content', ->
      transform('
        <Cmp class="over"><span class="inner">ahoj</span></Cmp>
      ')
      .should.equal('
        this.$$scope.$$render.__component(Cmp, this.$$scope, {"class": "over"}, this.$$scope.$$render.__element("span", this.$$scope.$$params.pop(), {"class": "inner"}, "ahoj"))
      ')
      return

  it 'should be able to pass more advanced arguments', ->
    transform('
      <span arg1="ARG1" arg2={arg2: true} {...params}><a href="#">link</a></span>
    ')
    .should.equal('
      this.$$scope.$$render.__element("span", this.$$scope.$$params.pop(), this.$$scope.$$render.__spread({"arg1": "ARG1", "arg2": (arg2: true)}, params),
      this.$$scope.$$render.__element("a", this.$$scope.$$params.pop(), {"href": "#"}, "link"))
    ')
    return


  describe 'filters', ->
    it 'should be able to convert "filters" to fn calls (even with white spaces)', ->
      transform('
        <span>{@name |capitalize |trim: 5}</span>
      ')
      .should.equal('
        this.$$scope.$$render.__element("span", this.$$scope.$$params.pop(), null, (this.$$scope.$$render.__filter("trim", this.$$scope.$$render.__filter("capitalize", @name), 5)))
      ')
      return

    it.skip 'should be able convert this advanced filters', ->
      transform('''
        <NecoVelikeho
          xxa={dalsi: 'je', spravne: 'co?'|neco}
          xor={@props|neco|ne:x}
          arg={@props|neco|ne:'x'}
          {...@props}
        />
      ''')
      .should.equal('''
        this.$$scope.$$render.__component(NecoVelikeho, this.$$scope, this.$$scope.$$render.__spread({ \\
          "xxa": (dalsi: 'je', spravne: this.$$scope.$$render.__filter("neco", 'co?')),  \\
          "xor": (this.$$scope.$$render.__filter("ne", this.$$scope.$$render.__filter("neco", @props), x)),  \\
          "arg": (this.$$scope.$$render.__filter("ne", this.$$scope.$$render.__filter("neco", @props), 'x'))
          }, @props
        ))
      ''')
      return


    it 'should be able convert this complicated example', ->
      transform('''
        <div class={
          'is-active': false or true
          'neat-component': true
        }
          on-click="toggle_done:
            self as item,
            neco as neco_jineho
          "
          on-shift-click="clicked"
          on-rmb-click="clicked" dalsi="hmm"
        >
          {if @props.showTitle
            <h1>A Component is I</h1>
          }
          <hr />
          {if true
            <NecoVelikeho arg={@props|neco|ne:'x'} {...@props} />
            <div class="neco"></div>
          }
          {for n in [1..5]
            <p key={n}><Live>@neco.dalsi</Live>This line has been printed {n} times</p>
          }
        </div>
      ''').should.equal('''
        this.$$scope.$$render.__element("div", this.$$scope.$$params.pop(), {"class": (
          'is-active': false or true
          'neat-component': true
        ),  \\
          "on-click": "toggle_done:
            self as item,
            neco as neco_jineho
          ",  \\
          "on-shift-click": "clicked",  \\
          "on-rmb-click": "clicked", "dalsi": "hmm"
        },
          (if @props.showTitle
            this.$$scope.$$render.__element("h1", this.$$scope.$$params.pop(), null, "A Component is I")
          ),
          this.$$scope.$$render.__element("hr", this.$$scope.$$params.pop(), null),
          (if true
            this.$$scope.$$render.__component(NecoVelikeho, this.$$scope, this.$$scope.$$render.__spread({"arg": (this.$$scope.$$render.__filter("ne", this.$$scope.$$render.__filter("neco", @props), 'x'))}, @props ))
            this.$$scope.$$render.__element("div", this.$$scope.$$params.pop(), {"class": "neco"})
          ),
          (for n in [1..5]
            this.$$scope.$$render.__element("p", this.$$scope.$$params.pop(), {"key": (n)}, this.$$scope.$$render.__component(Live, this.$$scope, null, "@neco.dalsi"), "This line has been printed ", (n), " times")
          )
        )
      ''')
      return

    # it 'should not have any difficulties with rendering conditional statements', ->
    #   transform('''
    #     <div>
    #       {if true
    #         [<div>neco</div>,
    #         <div>dalsi</div>].join('')
    #       else
    #         <span>neco</span>
    #         <span>dalsi</span>
    #       }
    #     </div>
    #   ''')
    #   .should.equal('''
    #       ''
    #   ''')
    #   return