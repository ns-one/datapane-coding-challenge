{render, $render, Live} = require '../app/lib/rendering'



describe.skip 'testing DOM creating functions', ->
  beforeEach ->
    $render.$$debug = false


  it 'should render single DOM element with attrs', ->
    Test =
      template: ->
        this.$$scope.$$render.__element("div", this.$$params.pop(), {"class": "neco", "atts": "dalsi"})

    render(Test, {}, null, {id: "idx"}).should.equal('<div class="neco" atts="dalsi" id="idx"></div>')


  it 'should hide atts or create without value when boolean', ->
    Test =
      template: ->
        this.$$scope.$$render.__element("div", this.$$params.pop(), {"class": "neco", "contenteditable": true, "autofocus": false})

    render(Test, {}, null, {id: "idx"}).should.equal('<div class="neco" contenteditable id="idx"></div>')



  it 'should raise an error when there\'s no forwaded scope to a component', ->
    Neco = ->
      this.$$scope.$$render.__element("div", this.$$params.pop(), {"class": "neco", "atts": "dalsi"})

    Test = ->
      this.$$scope.$$render.__component(Neco, this.$$scope)

    (-> render(Test, {})).should.throw()


  it 'should render DOM element with inner elements - debug is on', ->
    NecoVelikeho =
      template: ->
        this.$$scope.$$render.__element("div", this.$$params.pop(), {"class": "neco", "atts": "dalsi"})


    Test =
      template: ->
        this.$$scope.$$render.__element(
          "div"
          this.$$params.pop()
          {
            "class": (
              'is-active': false or true
              'neat-component': true
              'no': @show_no
            )
            "on-click": "toggle_done:
              self as item,
              neco as neco_jineho
            "
            "on-shift-click": "clicked"
            "on-rmb-click": "clicked", "dalsi": "hmm"
          }
          (if @showTitle
            this.$$scope.$$render.__element("h1", this.$$params.pop(), null, "A Component is I")
          )
          this.$$scope.$$render.__element("hr", this.$$params.pop(), null)
          (if true
            this.$$scope.$$render.__component(NecoVelikeho, this.$$scope, {"scope": {}})
            this.$$scope.$$render.__element("div", this.$$params.pop(), {"class": "neco"}, 'kratky text')
          )
          (for n in [1..5]
            this.$$scope.$$render.__element("p", this.$$params.pop(), {"key": (n)}, this.$$scope.$$render.__component(Live, this.$$scope, {"scope": {}}, "@neco.dalsi"), "This line has been printed ", (n), " times")
          )
        )

    $render.$$debug = true
    render(Test, {showTitle: true}, null, {id: "idx"}).should.equal('''
<div class="is-active neat-component" on-click="toggle_done: self as item, neco as neco_jineho" on-shift-click="clicked" on-rmb-click="clicked" dalsi="hmm" id="idx">
<h1>A Component is I</h1>
<hr />
<div class="neco">kratky text</div>
<p key="1">This line has been printed 1 times</p>
<p key="2">This line has been printed 2 times</p>
<p key="3">This line has been printed 3 times</p>
<p key="4">This line has been printed 4 times</p>
<p key="5">This line has been printed 5 times</p>
</div>''')


  it 'should render DOM element with inner elements - debug is off', ->
    NecoVelikeho =
      template: ->
        this.$$scope.$$render.__element("div", this.$$params.pop(), {"class": "neco", "atts": "dalsi"})


    Test =
      template: ->
        this.$$scope.$$render.__element(
          "div"
          this.$$params.pop()
          {
            "class": (
              'is-active': false or true
              'neat-component': true
              'no': @show_no
            )
            "on-click": "toggle_done:
              self as item,
              neco as neco_jineho
            "
            "on-shift-click": "clicked"
            "on-rmb-click": "clicked", "dalsi": "hmm"
          }
          (if @showTitle
            this.$$scope.$$render.__element("h1", this.$$params.pop(), null, "A Component is I")
          )
          this.$$scope.$$render.__element("hr", this.$$params.pop(), null)
          (if true
            this.$$scope.$$render.__component(NecoVelikeho, this.$$scope, {"scope": {}})
            this.$$scope.$$render.__element("div", this.$$params.pop(), {"class": "neco"})
          )
          (for n in [1..5]
            this.$$scope.$$render.__element("p", this.$$params.pop(), {"key": (n)}, this.$$scope.$$render.__component(Live, this.$$scope, {"scope": {}}, "@neco.dalsi"), "This line has been printed ", (n), " times")
          )
        )

    render(Test, {showTitle: true}, null, {id: "idx"}).should.equal('<div class="is-active neat-component" on-click="toggle_done: self as item, neco as neco_jineho" on-shift-click="clicked" on-rmb-click="clicked" dalsi="hmm" id="idx"><h1>A Component is I</h1><hr /><div class="neco"></div><p key="1">This line has been printed 1 times</p><p key="2">This line has been printed 2 times</p><p key="3">This line has been printed 3 times</p><p key="4">This line has been printed 4 times</p><p key="5">This line has been printed 5 times</p></div>')


  describe 'components', ->
    it 'should raise an error when component has other param than scope', ->
      Component =
        template: ->
          this.$$scope.$$render.__element("div", this.$$params.pop(), {"class": "neco"}, "ahoj")

      Test =
        template: ->
          this.$$scope.$$render.__component(Component, this.$$scope, {"class": "trida"})

      (-> render(Test, {})).should.throw()


    it 'can be defined in direct way', ->
      Test = ->
        this.$$scope.$$render.__element("div", this.$$params.pop(), {"class": "neco"}, "ahoj")

      render(Test, {}, null, {id: "idx"}).should.equal('<div class="neco" id="idx">ahoj</div>')


  describe 'attrs mixins', ->
    it 'should be able to re-render components with attrs fallback', ->
      Test = ->
        this.$$scope.$$render.__element("div", this.$$params.pop(), {"class": "neco"}, "ahoj")

      test_scope = {}
      render(Test, test_scope, null, {id: "idx"}).should.equal('<div class="neco" id="idx">ahoj</div>')

      test_scope.$$id.should.equal('idx')
      render(Test, test_scope, null).should.equal('<div class="neco" id="idx">ahoj</div>')
      render(Test, test_scope, null, {id: "ids"}).should.equal('<div class="neco" id="ids">ahoj</div>')


    describe 'joining mixins', ->
      it 'can merge styles', ->
        render(
          ->
            this.$$scope.$$render.__element("div", this.$$params.pop(), {"style": "width: 10px; height: 20px"}, "ahoj")
          , {}, null
          id: "idx"
          style: 'width: 50px'
        ).should.equal('<div style="width: 10px; height: 20px; width: 50px" id="idx">ahoj</div>')


      it 'can join classes', ->
        render(
          ->
            this.$$scope.$$render.__element("div", this.$$params.pop(), {"class": "dve"}, "ahoj")
          , {}, null
          id: "idx"
          class: 'tridy'
        ).should.equal('<div class="dve tridy" id="idx">ahoj</div>')


      it 'can replace id, name and others', ->
        render(
          ->
            this.$$scope.$$render.__element("div", this.$$params.pop(), {"name": "puvodni", "id": "nove"}, "ahoj")
          , {}, null
          id: "idx"
          name: 'nove'
        ).should.equal('<div name="nove" id="idx">ahoj</div>')


    describe '$$params should override defined attrs', ->
      it 'boolean : whatever', ->
        render(
          ->
            this.$$scope.$$render.__element("div", this.$$params.pop(), {"class": "neco"}, "ahoj")
          , {}, null
          id: "idx"
          class: false
        ).should.equal('<div id="idx">ahoj</div>')

        render(
          ->
            this.$$scope.$$render.__element("div", this.$$params.pop(), {"class": "neco"}, "ahoj")
          , {}, null
          id: "idx"
          class: true
        ).should.equal('<div class="neco" id="idx">ahoj</div>')

      it 'string : whatever', ->
        render(
          ->
            this.$$scope.$$render.__element("div", this.$$params.pop(), {"class": "neco"}, "ahoj")
          , {}, null
          id: "idx"
          class: 'jineho'
        ).should.equal('<div class="neco jineho" id="idx">ahoj</div>')

        render(
          ->
            this.$$scope.$$render.__element("div", this.$$params.pop(), {"class": "neco"}, "ahoj")
          , {}, null
          id: "idx"
          class: 'neco'
        ).should.equal('<div class="neco" id="idx">ahoj</div>')

      it 'object : whatever', ->
        render(
          ->
            this.$$scope.$$render.__element("div", this.$$params.pop(), {"class": "neco"}, "ahoj")
          , {}, null
          id: "idx"
          class:
            'neco': false
            'dalsi': true

        ).should.equal('<div class="dalsi" id="idx">ahoj</div>')

        render(
          ->
            this.$$scope.$$render.__element("div", this.$$params.pop(), {"class": "neco"}, "ahoj")
          , {}, null
          id: "idx"
          class:
            'dalsi': true

        ).should.equal('<div class="neco dalsi" id="idx">ahoj</div>')

