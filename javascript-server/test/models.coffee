{Model, ModelArray, PersistentModel} = require '../app/lib/models'
{spy} = require '../app/lib/extras'



describe 'Model', ->
  Car   = null
  Wheel = null

  beforeEach ->
    class Car extends Model
      @configure 'Car',
        seats: true
        motor: false
        type: -> 'rwd'

    Car.create(id: 0, seats: 4)
    Car.create(id: 1, seats: 2)
    Car.create(id: 2, seats: 3)


    class Wheel extends Model
      @configure 'Wheel',
        car_id: true

    Wheel.create(car_id: 0)
    Wheel.create(car_id: 1)
    Wheel.create(car_id: 2)
    Wheel.create(car_id: 3)


  afterEach ->
    Car.unbind()


  describe 'creating', ->
    it 'should receive id on save', ->
      car = new Car(seats: 4)
      (car.id?).should.equal(false)
      car.save()
      (car.id?).should.equal(true)
      return


    it 'doesn\'t override id, when given', ->
      car = new Car(id: 21, seats: 4)
      (car.id?).should.equal(true)
      car.save()
      (car.id?).should.equal(true)
      car.id.should.equal(21)
      return



    it 'Model.create should return obj with id', ->
      car = Car.create(seats: 4)
      (car.id?).should.equal(true)
      return


    it 'should generate unique ids', ->
      car1 = Car.create(seats: 4)
      car2 = Car.create(seats: 4)
      (car1.id? and car2.id?).should.equal(true)
      (car1.id isnt car2.id).should.equal(true)
      return


    describe 'create with existing id', ->
      it 'should have correct id', ->
        car = Car.create(id: 1, seats: 5)
        car.id.should.equal(1)
        return


      it 'should not create another obj', ->
        car = Car.create(id: 1, seats: 5)
        Car.objects.length.should.equal(3)
        return


      it 'should have new attributes', ->
        car = Car.create(id: 1, seats: 5)
        Car.get(1).seats.should.equal(5)
        return


      it 'should call update instead of create', ->
        cbName = undefined
        cb     = spy (rec, _cbName) -> cbName = _cbName

        Car.bind 'change', cb

        car = Car.create(id: 1, seats: 5)
        cb.counter.should.equal(1)
        cbName.should.equal('update')
        return


    describe 'validation', ->
      describe 'default values', ->
        it 'init', ->
          car = Car.create(seats: 4)
          car.type.should.equal('rwd')
          return


        it 'dont rewrite given values', ->
          car = Car.create(seats: 4, type: 'fwd')
          car.type.should.equal('fwd')
          return


        it 'restore default values on deleting current values', ->
          car = Car.create(seats: 4, type: 'fwd')
          car.type.should.equal('fwd')
          delete car.type
          car.save()
          car.type.should.equal('rwd')
          return


        it 'should restore previous (valid) values', ->
          car = Car.objects[0]
          car.motor = 'engine'

          delete car.seats
          (-> car.save()).should.throw()
          return

          car = Car.objects[0]
          (car.attributes()).should.eql(
            id: 0
            motor: undefined
            seats: 4
            type: 'rwd'
          )
          return


      describe 'required values', ->
        it 'shouldn\'t save when missing required values', ->
          (-> Car.create()).should.throw()
          Car.objects.length.should.equal(3)
          return


      describe 'value types and format', ->
        # not needed now


  describe 'features', ->
    it 'should return attributes', ->
      Car.objects[0].attributes().should.eql(
        id: 0
        motor: undefined
        seats: 4
        type: 'rwd'
      )
      return

    describe 'getting items', ->
      it 'can get item by it\'s id', ->
        Car.get(0).id.should.equal(0)
        Car.get(1).id.should.equal(1)
        return


      it 'throws an error when there\'s no such object', ->
        (-> Car.get(5)).should.throw()
        return


      it 'can return lazy object without modifying objects array', ->
        lazy = Car.get(5, true)
        lazy.attributes().should.eql(id: 5, motor: undefined, seats: undefined, type: 'rwd')
        lazy.$lazy.should.equal(true)
        Car.objects.length.should.equal(3)
        return


      it 'lazy objs are shared', ->
        lazy  = Car.get(5, true)
        lazy2 = Car.get(5, true)

        (lazy is lazy2).should.equal(true)
        return


      it 'can refill lazy objects', ->
        lazy = Car.get(55, true)

        car = Car.create(id: 55, seats: 3)

        lazy.attributes().should.eql(id: 55, motor: undefined, seats: 3, type: 'rwd')
        (lazy is car).should.equal(true)
        return


    describe 'objects array', ->
      it 'shouldn\'t allow calling of push, unshift, shift, pop, etc.', ->
        (-> Car.objects.push()).should.throw()
        (-> Car.objects.pop()).should.throw()
        (-> Car.objects.unshift()).should.throw()
        (-> Car.objects.shift()).should.throw()
        return


      it 'should have .sort() method which takes fn, which shows comparable field', ->
        cars = Car.objects.sort (c) -> c.seats
        ids  = cars.map (c) -> c.id
        ids.should.eql [1, 2, 0]

        cars = Car.objects.sort (c) -> c.seats
        ids  = cars.reverse().map (c) -> c.id
        ids.should.eql [0, 2, 1]
        return


      it 'should be immutable to sort', ->
        cars = Car.objects.sort (c) -> c.seats
        ids  = cars.map (c) -> c.id
        ids.should.eql [1, 2, 0]

        ids = Car.objects.map (c) -> c.id
        ids.should.eql [0, 1, 2]
        return


      it 'should be immutable to reverse', ->
        ids = Car.objects.reverse().map (c) -> c.id
        ids.should.eql [2, 1, 0]

        ids = Car.objects.map (c) -> c.id
        ids.should.eql [0, 1, 2]
        return


      it 'checks correc type', ->
        (-> Car.objects.add({})).should.throw()

        wheel = Wheel.objects[0]
        (-> Car.objects.add(wheel)).should.throw()
        return


      it 'doesn\'t add duplicitely same items', ->
        Car.objects._add(Car.objects[0])
        Car.objects.length.should.equal(3)
        return


  describe 'length and index', ->
    it 'keeps up to date length attr', ->
      Car.objects.length.should.equal(3)
      return


    it 'updates index on create and delete', ->
      Car.objects[0].delete()
      Car.objects.length.should.equal(2)
      return


    it 'can correctly delete last item', ->
      Car.objects[-1..][0].delete()
      Car.objects.length.should.equal(2)
      return


    it 'can keep items in selected order', ->
      # todo - je to potreba? chci to?


  describe 'bindings', ->
    it 'tells when its beeing created', ->
      cb = spy ->

      Car.bind 'create', cb
      Car.create(seats: 2)

      cb.counter.should.equal(1)
      return


    it 'tells when its beeing deleted', ->
      cb  = spy ->
      car = Car.objects[0]

      Car.bind 'delete', cb
      car.bind 'delete', cb

      car.delete()

      cb.counter.should.equal(2)
      return


    it 'tells when its beeing updated or not changed', ->
      cb1 = spy ->
      cb2 = spy ->
      car = Car.objects[0]

      Car.bind 'update', cb1
      car.bind 'update', cb1
      Car.bind 'unchanged', cb2
      car.bind 'unchanged', cb2

      car.save()

      cb1.counter.should.equal(0)
      cb2.counter.should.equal(0)

      car.save(force_trigger: true)
      cb2.counter.should.equal(2)

      car.seats = 4
      car.save()

      cb1.counter.should.equal(0)
      cb2.counter.should.equal(2)

      car.seats = 3
      car.save()

      cb1.counter.should.equal(2)
      cb2.counter.should.equal(2)
      return


    it 'tells about any change (create, update, delete)', ->
      cb  = spy ->
      car = Car.objects[0]

      Car.bind 'change', cb
      car.bind 'change', cb

      car.save()
      cb.counter.should.equal(0)

      car.save(force_trigger: true)
      cb.counter.should.equal(2)

      car.delete()
      cb.counter.should.equal(4)
      return


    it 'handles correctly change and create events', ->
      cr = spy ->
      ch = spy ->
      bb = spy ->

      class Chat extends PersistentModel
        @configure 'Chat',
          last_message_id: false

      chat = Chat.create()
      return


      class ChatMessage extends PersistentModel
        @configure 'ChatMessage',
          chat_id: true

        @ForeignKey Chat,
          id_field: 'chat_id'
          self_field: 'chat'
          remote_field: 'messages'

        @bind 'create', (rec) ->
          rec.chat.last_message_id = rec.id
          unless rec.chat.is_open
            rec.chat.is_seen = false

          rec.chat.save()

        @bind 'create', cr
        @bind 'change', ch
        @bind 'create change', bb


      Chat.ForeignKey ChatMessage,
        id_field: 'last_message_id'
        self_field: 'last_message'
        remote_field: 'chat_set' # todo - nechci


      c = ChatMessage.create(chat_id: chat.id).delete()
      ChatMessage.create(chat_id: chat.id)

      cr.counter.should.equal(2)
      ch.counter.should.equal(3)
      bb.counter.should.equal(5)
      return


  describe 'relationship', ->
    describe 'ForeignKey', ->
      beforeEach ->
        Wheel.ForeignKey Car,
          id_field: 'car_id'
          self_field: 'car'
          remote_field: 'wheels'

        for obj in Car.objects
          obj.load()

        for obj in Wheel.objects
          obj.load()


      describe 'creation', ->
        it 'not saved object can see remote obj, but remote obj cannot see that object', ->
          wheel = new Wheel(car_id: 0)
          car   = Car.objects[0]

          (wheel.car is car).should.equal(true)
          (car.wheels.some (w) -> w is wheel).should.equal(false)
          return


      describe 'self', ->
        it 'should create nested remote object', ->
          wheel = Wheel.objects[0]
          (wheel.car instanceof Car).should.equal(true)
          return

        it 'should unlink remote object', ->
          wheel = Wheel.objects[0]
          car   = wheel.car

          car.delete.should.throw()
          wheel.car_id.should.equal(0)
          (car.$deleted?).should.equal(false)

          # mockup
          wheel.is_valid = -> true

          car.delete()
          (wheel.car_id?).should.equal(false)
          return



      describe 'remote', ->
        it 'should create nested ModelArray on on remote obj', ->
          car = Car.objects[0]
          (car.wheels instanceof Array).should.equal(true)
          (car.wheels.push).should.throw()
          return

        describe 'validation', ->
          it 'should\'t accept wrong instance', ->
            car = Car.objects[0]
            car.wheels.check(car).should.equal(false)
            return


          it 'should\'t accept plain object', ->
            car = Car.objects[0]
            car.wheels.check({}).should.equal(false)
            return


          it 'should\'t accept only correct instance', ->
            car = Car.objects[0]
            car.wheels.check(Wheel.objects[0]).should.equal(true)
            return


        describe 'adding/removing', ->
          it 'should have correct initial distribution', ->
            for car in Car.objects
              car.wheels.length.should.equal(1)

            return

          it 'should refill distrib on lazy objs', ->
            car = Car.create(id: 3, seats: 2)
            car.wheels.length.should.equal(1)
            return


          it 'should update remote connections on id_fields changes', ->
            car1  = Car.objects[0]
            car2  = Car.objects[1]
            wheel = car1.wheels[0]

            wheel.car_id.should.equal(car1.id)

            car2.wheels.add(wheel)

            wheel.car_id.should.equal(car2.id)

            car1.wheels.length.should.equal(0)
            car2.wheels.length.should.equal(2)
            return


          describe 'should raise error when removing (instead of replacing)', ->
            it 'removing', ->
              (-> Car.objects[0].wheels.remove(Car.objects[0].wheels[0])).should.throw()
              return

            it 'replacing', ->
              car1  = Car.objects[0]
              car2  = Car.objects[1]
              wheel = car1.wheels[0]

              wheel.car_id = car2.id
              (-> wheel.save()).should.not.throw()

              car1.wheels.length.should.equal(0)
              car2.wheels.length.should.equal(2)
              return


          it 'should create nested array even on $lazy objects', ->
            wheel = Wheel.objects[3]
            car   = wheel.car

            car.$lazy.should.equal(true)
            car.wheels.length.should.equal(1)
            return


          it 'should remove object from remote array', ->
            wheel = Wheel.objects[0]
            car   = wheel.car

            wheel.delete()

            car.wheels.length.should.equal(0)
            return

          it.skip 'TODO: should call save on right model in only necessary times', ->
            # správně volá updated, případně unchanged (na vyžádání)


        describe 'scenario', ->
          it 'self reccursion', ->
            class Person extends Model
              @configure 'Person',
                self_id: true

              @ForeignKey Person,
                id_field: 'self_id'
                self_field: 'myself'
                remote_field: 'they'

            Person.create(id: 1, self_id: 1)
            Person.create(id: 2, self_id: 2)
            Person.create(id: 3, self_id: 3)
            Person.create(id: 4, self_id: 4)
            Person.create(id: 5, self_id: 5)

            Person.objects.length.should.equal(5)
            Person.lazy_map.should.eql({})
            return


          it 'should create relationship on self', ->
            class Task extends Model
              @configure 'Task',
                parent_id: false
                title: true

              @ForeignKey Task,
                id_field: 'parent_id'
                self_field: 'parent'
                remote_field: 'children'


            Task.create(
              # id: '2.1.1'
              title: 'task 2.1.1'
              parent_id:
                Task.create(
                  # id: '2.1'
                  title: 'task 2.1'
                  parent_id: (t2 = Task.create(
                    # id: '2'
                    title: 'task 2'
                  )).id
                ).id
            )
            Task.create(
              # id: '1.1.1'
              title: 'task 1.1.1'
              parent_id:
                Task.create(
                  # id: '1.1'
                  title: 'task 1.1'
                  parent_id: (t1 = Task.create(
                    # id: '1'
                    title: 'task 1'
                  )).id
                ).id
            )

            t2.children.length.should.equal(1)
            (t2 is t2.children[0].parent).should.equal(true)

            t2.children[0].children.length.should.equal(1)
            (t2.children[0] is t2.children[0].children[0].parent).should.equal(true)

            t2.children[0].children[0].children.length.should.equal(0)


            t1.children.length.should.equal(1)
            (t1 is t1.children[0].parent).should.equal(true)

            t1.children[0].children.length.should.equal(1)
            (t1.children[0] is t1.children[0].children[0].parent).should.equal(true)

            t1.children[0].children[0].children.length.should.equal(0)
            return



          it 'full reccursion', ->
            class Person extends Model
              @configure 'Person',
                self_id: true

              @ForeignKey Person,
                id_field: 'self_id'
                self_field: 'myself'
                remote_field: 'they'

            Person.create(id: 1, self_id: 1)
            Person.create(id: 2, self_id: 2)
            Person.create(id: 3, self_id: 3)
            Person.create(id: 4, self_id: 4)
            Person.create(id: 5, self_id: 5)

            Person.objects.length.should.equal(5)
            Person.lazy_map.should.eql({})

            person = Person.objects[0]
            (person is person.myself).should.equal(true)
            (person.myself is person.they[0]).should.equal(true)
            return


    describe 'ManyToMany', ->
      Driver = null

      beforeEach ->
        class Driver extends Model
          @configure 'Driver',
            car_ids: -> []
            name: true

          @ManyToMany Car,
            ids_field: 'car_ids'
            self_field: 'cars'
            remote_field: 'drivers'

        for obj in Car.objects
          obj.load()

        Driver.create(name: 'Petr', car_ids: [0, 1, 2])
        Driver.create(name: 'Niki', car_ids: [0, 1])
        Driver.create(name: 'Ales', car_ids: [2])
        Driver.create(name: 'Lazy', car_ids: [5, 6])


      describe 'self', ->
        it 'should create nested remote object', ->
          driver = Driver.objects[0]
          (driver.cars instanceof Array).should.equal(true)
          (driver.cars.push).should.throw()
          return


        describe 'validation', ->
          it 'should\'t accept wrong instance', ->
            driver = Driver.objects[0]
            driver.cars.check(driver).should.equal(false)
            return


          it 'should\'t accept plain object', ->
            driver = Driver.objects[0]
            driver.cars.check({}).should.equal(false)
            return


          it 'should\'t accept only correct instance', ->
            driver = Driver.objects[0]
            driver.cars.check(Car.objects[0]).should.equal(true)
            return


        describe 'adding/removing', ->
          it 'should have correct initial distribution', ->
            Driver.objects[0].cars.length.should.equal(3)
            Driver.objects[1].cars.length.should.equal(2)
            Driver.objects[2].cars.length.should.equal(1)
            Driver.objects[3].cars.length.should.equal(2)
            return


          it 'should init distrib with lazy objs', ->
            driver = Driver.objects[3]
            driver.cars.length.should.equal(2)

            for car in driver.cars
              (!!car.$lazy).should.equal(true)

            car5 = Car.create(id: 5, seats: 5)
            car6 = Car.create(id: 6, seats: 5)

            for car in driver.cars
              (!!car.$lazy).should.equal(false)

            driver.cars[0].should.equal(car5)
            driver.cars[1].should.equal(car6)
            return


          describe 'should update remote connections on id_fields changes', ->
            it 'not changed', ->
              driver1 = Driver.objects[0]
              driver2 = Driver.objects[1]
              car1    = driver1.cars[0]
              car2    = driver1.cars[1]

              # test relations
              (car1.id in driver1.car_ids).should.equal(true)
              (car2.id in driver1.car_ids).should.equal(true)

              (car1.id in driver2.car_ids).should.equal(true)
              (car2.id in driver2.car_ids).should.equal(true)

              driver1.cars.length.should.equal(3)
              driver2.cars.length.should.equal(2)

              car1.drivers.length.should.equal(2)
              car2.drivers.length.should.equal(2)

              # these drivers already have these cars
              driver2.cars.add(car1)
              driver2.cars.add(car2)

              # test relations
              (car1.id in driver1.car_ids).should.equal(true)
              (car2.id in driver1.car_ids).should.equal(true)

              (car1.id in driver2.car_ids).should.equal(true)
              (car2.id in driver2.car_ids).should.equal(true)

              driver1.cars.length.should.equal(3)
              driver2.cars.length.should.equal(2)

              car1.drivers.length.should.equal(2)
              car2.drivers.length.should.equal(2)
              return


            it 'changed', ->
              driver4 = Driver.objects[3]
              driver2 = Driver.objects[1]
              car1    = driver4.cars[0]
              car2    = driver4.cars[1]

              # test relations
              (car1.id in driver4.car_ids).should.equal(true)
              (car2.id in driver4.car_ids).should.equal(true)

              (car1.id in driver2.car_ids).should.equal(false)
              (car2.id in driver2.car_ids).should.equal(false)

              driver4.cars.length.should.equal(2)
              driver2.cars.length.should.equal(2)

              car1.drivers.length.should.equal(1)
              car2.drivers.length.should.equal(1)

              # change
              driver2.cars.add(car1)
              driver2.cars.add(car2)

              # test relations
              (car1.id in driver4.car_ids).should.equal(true)
              (car2.id in driver4.car_ids).should.equal(true)

              (car1.id in driver2.car_ids).should.equal(true)
              (car2.id in driver2.car_ids).should.equal(true)

              driver4.cars.length.should.equal(2)
              driver2.cars.length.should.equal(4)

              car1.drivers.length.should.equal(2)
              car2.drivers.length.should.equal(2)

              # change
              driver2.cars.remove(car1)

              # test relations
              (car1.id in driver4.car_ids).should.equal(true)
              (car2.id in driver4.car_ids).should.equal(true)

              (car1.id in driver2.car_ids).should.equal(false)
              (car2.id in driver2.car_ids).should.equal(true)

              driver4.cars.length.should.equal(2)
              driver2.cars.length.should.equal(3)

              car1.drivers.length.should.equal(1)
              car2.drivers.length.should.equal(2)
              return


          it 'should remove object from remote array', ->
            driver = Driver.objects[0]
            car    = driver.cars[0]

            driver.delete()

            (car.drivers.filter (d) -> d.id is driver.id).length.should.equal(0)
            return


      describe 'remote', ->
        it 'should create nested ModelArray on on remote obj', ->
          car = Car.objects[0]
          (car.drivers instanceof Array).should.equal(true)
          (car.drivers.push).should.throw()
          return

        describe 'validation', ->
          it 'should\'t accept wrong instance', ->
            car = Car.objects[0]
            car.drivers.check(car).should.equal(false)
            return


          it 'should\'t accept plain object', ->
            car = Car.objects[0]
            car.drivers.check({}).should.equal(false)
            return


          it 'should\'t accept only correct instance', ->
            car = Car.objects[0]
            car.drivers.check(Driver.objects[0]).should.equal(true)
            return


        describe 'adding/removing', ->
          it 'should have correct initial distribution', ->
            Car.objects[0].drivers.length.should.equal(2)
            # for car in Car.objects
            #   car.drivers.length.should.equal(2)
            return


          it 'should refill distrib on lazy objs', ->
            for id in [5, 6]
              car = Car.create(id: id, seats: 2)
              car.drivers.length.should.equal(1)
            return


          describe 'should update remote connections on id_fields changes', ->
            it 'not changed', ->
              car1    = Car.objects[0]
              car2    = Car.objects[1]
              driver1 = car1.drivers[0]
              driver2 = car1.drivers[1]

              # test relations
              (car1.id in driver1.car_ids).should.equal(true)
              (car2.id in driver1.car_ids).should.equal(true)

              (car1.id in driver2.car_ids).should.equal(true)
              (car2.id in driver2.car_ids).should.equal(true)

              driver1.cars.length.should.equal(3)
              driver2.cars.length.should.equal(2)

              car1.drivers.length.should.equal(2)
              car2.drivers.length.should.equal(2)

              # these cars already have these drivers
              car2.drivers.add(driver1)
              car2.drivers.add(driver2)

              # test relations
              (car1.id in driver1.car_ids).should.equal(true)
              (car2.id in driver1.car_ids).should.equal(true)

              (car1.id in driver2.car_ids).should.equal(true)
              (car2.id in driver2.car_ids).should.equal(true)

              driver1.cars.length.should.equal(3)
              driver2.cars.length.should.equal(2)

              car1.drivers.length.should.equal(2)
              car2.drivers.length.should.equal(2)
              return


            it 'changed', ->
              car1    = Car.objects[0]
              car3    = Car.objects[2]
              driver1 = car1.drivers[0]
              driver2 = car1.drivers[1]

              # test relations
              (car1.id in driver1.car_ids).should.equal(true)
              (car3.id in driver1.car_ids).should.equal(true)

              (car1.id in driver2.car_ids).should.equal(true)
              (car3.id in driver2.car_ids).should.equal(false)

              driver1.cars.length.should.equal(3)
              driver2.cars.length.should.equal(2)

              car1.drivers.length.should.equal(2)
              car3.drivers.length.should.equal(2)

              # change relation
              car1.drivers.remove(driver1)
              car3.drivers.add(driver2)

              # test relations
              (car1.id in driver1.car_ids).should.equal(false)
              (car3.id in driver1.car_ids).should.equal(true)

              (car1.id in driver2.car_ids).should.equal(true)
              (car3.id in driver2.car_ids).should.equal(true)

              driver1.cars.length.should.equal(2)
              driver2.cars.length.should.equal(3)

              car1.drivers.length.should.equal(1)
              car3.drivers.length.should.equal(3)
              return


          it 'should remove object from remote array', ->
            car    = Car.objects[0]
            driver = car.drivers[0]

            car.delete()

            (driver.cars.filter (c) -> c.id is car.id).length.should.equal(0)
            return

