

# thread = (timeout, fn) ->
#   unless fn
#     [timeout, fn] = [1, timeout]

#   timeout = setTimeout(fn, timeout)
#   -> clearTimeout(timeout)


# t0 = undefined


# class Iterator
#   constructor: (timeout=2) ->
#     @timeout = 1e3 * timeout

#   bind: (@cb) ->
#     @next()
#     => @unbind()

#   next: ->
#     @tm = thread @timeout, =>
#       t0 = new Date
#       @cb(true)
#       unless @done
#         @next()

#   unbind: ->
#     console.log 'unbind'
#     @done = true
#     # @tm()

# total = 0
# i     = 0
# cb = (new Iterator(.1)).bind (it) ->
#   # console.log {total, i}
#   if it is true
#     i     += 1
#     total += (new Date) - t0

#   if 5 < i
#     cb()
#     console.log {total, i}



t0 = new Date

memoize = (fn) ->
  cache = {}

  decoraded_function = (key) ->
    if key of cache
      cache[key]
    else
      cache[key] = fn(key)


fibonnaci = (n) ->
  if n <= 2 then 1
  else           fibonnaci(n - 2) + fibonnaci(n - 1)


ffn = (n) ->
  cache = {}
  i     = 0
  while i++ < n
    if i <= 2
      cache[i] = 1
    else
      cache[i] = cache[i - 2] + cache[i - 1]

  cache[i - 1]



# ffn = memoize(fibonnaci)
ff   = ffn(5000)
# f    = fibonnaci(10)
f = 0

# i = 0
# while i++ < 10
# # f = ffn(35)
#   f = fibonnaci(35)

t1 = (new Date) - t0

console.log {f, ff, t1}