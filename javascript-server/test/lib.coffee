{eq, extend, flatten, get_value, distinct, pop_js_objects_out_of_the_string, thread, throttle, debounce} = require '../app/lib/extras'
{get_el_value} = require '../app/lib/events'
{queue} = require '../app/lib/promise'


describe 'some fns of extras', ->
  describe 'get_value', ->
    it 'should get keys', ->
      a =
        key1: 'val1'
        key2: 'val2'

      get_value(a, 'key1').should.equal('val1')
      get_value(a, 'key2').should.equal('val2')
      return

    it 'should get deeper keys', ->
      a =
        key1:
          key11: 'val11'
          key12: 'val12'

        key2: 'val2'

      get_value(a, 'key1.key11').should.equal('val11')
      get_value(a, 'key1.key12').should.equal('val12')
      get_value(a, 'key1').should.be.eql(key11: 'val11', key12: 'val12')
      get_value(a, 'key2').should.equal('val2')
      return

    it 'should be able to handle list and dicts as well', ->
      a = [
        'ahoj'
        'nene'
        {
          key1: 'val1'
          key2: 'val2'
        }
      ]

      get_value(a, '1').should.equal('nene')
      get_value(a, '2.key1').should.equal('val1')
      return


  describe 'flatten', ->
    it 'should flatten 2 levels', ->
      flatten([[1],[2]]).should.eql([1, 2])
      return

    it 'should turn single elm into flat list', ->
      flatten('a').should.eql(['a'])
      return

    it 'should not flatten dict', ->
      flatten(key: 1, val: 2).should.eql([key: 1, val: 2])
      return

    it 'should flatten mixed struct', ->
      flatten([1, [2, {val: 3}]]).should.eql([1, 2, {val: 3}])
      return

  describe 'distinct', ->
    it 'should perserve order', ->
      distinct([1, 3, 2]).should.eql([1, 3, 2])
      distinct([1, 3, 2, 3]).should.eql([1, 3, 2])
      return



  describe 'eq', ->
    it 'should eq arrays', ->
      eq.array([1, 2], [1, 2]).should.equal(true)
      eq.array([1, 2], [1, 3]).should.equal(false)
      return


    it 'should eq dicts', ->
      eq.object({'a': 'A', 'b': 'B'}, {'b':'B', 'a': 'A'}).should.equal(true)

      d1 = {"id":3,"scenario_id":1,"following_block_ids":[4,6],"answer":"Nezajímá mě to, nechci si s tebou psát.","following_questions":["A proč?","Bojíš se taťky?","Mi taťka zakazuje bavit se s cizími...je hrozně opatrnej...","Já hledám někoho, komu bych se mohla svěřit.","Dneska je mi smutno :("]}
      d2 = {"id":3,"scenario_id":1,"following_block_ids":[4,6],"answer":"Nezajímá mě to, nechci si s tebou psát.","following_questions":["A proč?","Bojíš se taťky?","Mi taťka zakazuje bavit se s cizími...je hrozně opatrnej...","Já hledám někoho, komu bych se mohla svěřit.","Dneska je mi smutno :("]}

      eq.object(d1, d2).should.equal(true)
      return


  describe 'pop_js_objects_out_of_the_string', ->
    it 'should pop js objects out', ->
      pop_js_objects_out_of_the_string('{a: "key", b: "dalsi"} [a, b, c] "ahoj" \'ty\'')
        .should.eql([
          '$value0 $value1 $value2 $value3'
          $value0: '{a: "key", b: "dalsi"}'
          $value1: '[a, b, c]'
          $value2: '"ahoj"'
          $value3: '\'ty\''
        ])
      return


  describe 'throttle', ->
    it 'runs in sync when is nothing in the queue', ->
      result = ''
      key = Math.random()

      throttle 10, key, ->
        result += 'Hello World'

      result.should.equal 'Hello World'
      return

    it 'should process all items well', ->
      result = ''
      key = Math.random()

      for ch in 'hello world'
        do (ch) ->
          throttle 10, key,
            -> result += ch.toUpperCase()
            -> result += ch

      result.should.equal 'Hello worl' # one character is missing since it's waiting to be fired or skipped
      return

    it 'should be able to handle separate queues', ->
      result = ''
      key = Math.random()

      for i in [0...3]
        do (i) ->
          queue key, (_done) ->
            thread 1, ->
              throttle 5, 'key',
                -> result += 'Hello'

              _done()

      queue key, ->
        for ch in ' World'
          throttle 5, ch, ->
            result += ch

      queue.promise key, ->
        result.should.equal 'Hello World'
        return




describe 'lib/events', ->
  describe.skip 'get_el_value', ->
    el = (tagName, atts, children...) ->
      extend {}, atts,
        tagName: tagName.toUpperCase()
        children: (el(child...) for child in children)
        getAttribute: (key) -> @[key]
        querySelectorAll: -> @children

    _get_el_value = (elem...) -> get_el_value(el(elem...))


    describe 'should return value for various inputs', ->
      it 'text input', ->
        _get_el_value(
          'input'
          type: 'text'
          name: 'inputName'
          value: 'correct'
          innerHTML: 'wrong'
        ).should.equal('correct')
        return

      it 'textarea', ->
        _get_el_value(
          'textarea'
          name: 'inputName'
          value: 'correct'
          innerHTML: 'wrong'
        ).should.equal('correct')
        return

      it 'select', ->
        _get_el_value(
          'select'
          name: 'inputName'
          value: 'correct'
          innerHTML: 'wrong'
        ).should.equal('correct')
        return

      describe 'checkbox', ->
        it 'with value', ->
          _get_el_value(
            'input'
            type: 'checkbox'
            name: 'inputName'
            value: 'correct'
            checked: true
          ).should.equal('correct')

          (_get_el_value(
            'input'
            type: 'checkbox'
            name: 'inputName'
            value: 'wrong'
            checked: false
          ) is undefined).should.equal(true)
          return

        it 'with no value', ->
          _get_el_value(
            'input'
            type: 'checkbox'
            name: 'inputName'
            checked: true
          ).should.equal(true)

          _get_el_value(
            'input'
            type: 'checkbox'
            name: 'inputName'
            checked: false
          ).should.equal(false)
          return

      describe 'radio', ->
        it 'with value', ->
          _get_el_value(
            'input'
            type: 'radio'
            name: 'inputName'
            value: 'correct'
            checked: true
          ).should.equal('correct')

          console.log (_get_el_value(
            'input'
            type: 'radio'
            name: 'inputName'
            value: 'wrong'
            checked: false
          ))

          (_get_el_value(
            'input'
            type: 'radio'
            name: 'inputName'
            value: 'wrong'
            checked: false
          ) is undefined).should.equal(true)
          return

        it 'with no value', ->
          (-> _get_el_value(
            'input'
            type: 'radio'
            name: 'inputName'
            checked: true
          )).should.throw()

          (-> _get_el_value(
            'input'
            type: 'radio'
            name: 'inputName'
            checked: false
          )).should.throw()
          return

    describe 'should return value for each input in a dict', ->
      it 'particular inputs', ->
        _get_el_value(
          'form'
          {}
          [
            'input'
            type: 'text'
            name: 'key'
            value: 'value'
          ]
          [
            'select'
            name: 'dalsi'
            value: 'ahoj'
          ]
          [
            'input'
            type: 'checkbox'
            name: 'checkbox'
            checked: true
          ]
        ).should.eql(key: 'value', dalsi: 'ahoj', checkbox: true)
        return

      it 'should group more checkboxes with same name into an array', ->
        _get_el_value(
          'form'
          {}
          [
            'input'
            type: 'checkbox'
            name: 'groupped'
            value: 'val1'
            checked: true
          ]
          [
            'input'
            type: 'checkbox'
            name: 'groupped'
            value: 'val2'
            checked: false
          ]
          [
            'input'
            type: 'checkbox'
            name: 'groupped'
            value: 'val3'
            checked: true
          ]
          [
            'input'
            type: 'checkbox'
            name: 'solo'
            value: 'val'
            checked: true
          ]
          [
            'input'
            type: 'checkbox'
            name: 'solo2'
            checked: true
          ]
          [
            'input'
            type: 'checkbox'
            name: 'solo3'
            checked: false
          ]
        ).should.eql(groupped: ['val1', 'val3'], solo: ['val'], solo2: true, solo3: false)
        return

      it 'should get only selected value of groups of radios fields', ->
        _get_el_value(
          'form'
          {}
          [
            'input'
            type: 'radio'
            name: 'groupped'
            value: 'val1'
            checked: false
          ]
          [
            'input'
            type: 'radio'
            name: 'groupped'
            value: 'val2'
            checked: true
          ]
          [
            'input'
            type: 'radio'
            name: 'groupped'
            value: 'val3'
            checked: false
          ]
          [
            'input'
            type: 'radio'
            name: 'solo'
            value: 'val'
            checked: true
          ]
          [
            'input'
            type: 'radio'
            name: 'solo2'
            value: 'val'
            checked: true
          ]
          [
            'input'
            type: 'radio'
            name: 'solo3'
            value: 'val'
            checked: false
          ]
        ).should.eql(groupped: 'val2', solo: 'val', solo2: 'val')
        return
