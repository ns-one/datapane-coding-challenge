{from_k} = require '../app/lib/extras'


describe 'measure performance and some features', ->
  describe 'quicker implementation', ->
    Cars = {}
    Cars_length = 0

    front_left_seat  = {name: 'predni leve sedadlo'}
    front_right_seat = {name: 'predni prave sedadlo'}
    rear_left_seat   = {name: 'zadni leve sedadlo'}
    rear_right_seat  = {name: 'zadni prave sedadlo'}

    car =
      brand: 'VW'
      model: 'Golf mk V'
      wheel: true
      seats: [
        front_left_seat
        front_right_seat
        rear_left_seat
        rear_right_seat
      ]


    it.skip 'should fill Cars object (slow)', ->
      Cars_length = 1e6
      i = 0
      while i < Cars_length
        Cars[i++] = car

      return



    it 'should fill Cars object (fast)', ->
      Cars_length = 100
      i = 0
      while i < Cars_length
        Cars[i++] = car
      return


    it 'shows that Cars should be still defined', ->
      from_k(Cars).length.should.equal 100
      return


    it 'should be able to change all objects at once', ->
      Cars[50].model = 'Passat'
      Cars[0].model.should.equal 'Passat'
      return


  describe 'slower implementation', ->
    Cars = {}

    it.skip 'should fill Cars object (slow)', ->
      i = 0
      while i < 1e6
        Cars[i++] =
          brand: 'VW'
          model: 'Golf mk V'
          wheel: true
          seats: [
            {name: 'predni leve sedadlo'}
            {name: 'predni prave sedadlo'}
            {name: 'zadni leve sedadlo'}
            {name: 'zadni prave sedadlo'}
          ]
    return

    it 'should fill Cars object (fast)', ->
      i = 0
      while i < 100
        Cars[i++] =
          brand: 'VW'
          model: 'Golf mk V'
          wheel: true
          seats: [
            {name: 'predni leve sedadlo'}
            {name: 'predni prave sedadlo'}
            {name: 'zadni leve sedadlo'}
            {name: 'zadni prave sedadlo'}
          ]
      return
