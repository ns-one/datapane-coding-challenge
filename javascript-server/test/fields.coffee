


describe 'Fields', ->
  clean_value = (value) ->
    # remove unwanted styles
    strings_to_remove = (value.match(/\ style="[^"]+" ?/g) ? [])
      .map (string) ->
        ending = string.split('"')[2]
        out    = ''

        if match = string.match(/text-align: ?\w+/)?[0]
          out = ' style="' + match + ';"'

        out += ending
        {string, out}

    for {string, out} in strings_to_remove
      value = value.replace(string, out)

    # replace ,. to &nbsp;
    value = value?.replace(/,\./ig, '&nbsp;')
    value



  strip_value = (value) ->
    value
      .replace(/^(<p>|<\/p>|<br ?\/?>)+\s*/, '')
      .replace(/\s*(<p>|<\/p>|<br ?\/?>)+$/, '')


  it 'clean_value - should remove only styles, except text-align', ->
    clean_value('ahoj').should.equal('ahoj')
    clean_value('<a href="ahoj">neco</a>').should.equal('<a href="ahoj">neco</a>')
    clean_value('<a href="ahoj" style="neco; text-align: center;">neco</a>').should.equal('<a href="ahoj" style="text-align: center;">neco</a>')
    clean_value('<a href="ahoj" style="neco;">neco</a>').should.equal('<a href="ahoj">neco</a>')
    clean_value('<a href="ahoj" style="text-align: center">neco</a>').should.equal('<a href="ahoj" style="text-align: center;">neco</a>')
    return

  it 'strip_value', ->
    strip_value('<p>ahoj</p>').should.equal('ahoj')
    return