

describe 'custom', ->
  {Model} = require '../app/lib/models'

  it 'pcr app - model\'s transaction', ->
    class Person extends Model
      @configure 'Person',
        name: true

      is_me: -> false


    class Chat extends Model
      @configure 'Chat',
        last_message_id: true
        is_seen: -> false

      is_valid: ->
        if not @last_message_id and not @messages.length
          true
        else
          super


    class ChatMessage extends Model
      @configure 'ChatMessage',
        person_id: true
        chat_id: true

        text: true
        timestamp: -> new Date

      @ForeignKey Person,
        id_field: 'person_id'
        self_field: 'person'
        remote_field: 'chat_messages'

      @ForeignKey Chat,
        id_field: 'chat_id'
        self_field: 'chat'
        remote_field: 'messages'

      @bind 'create', (rec) ->
        rec.chat.last_message_id = rec.id
        rec.chat.save()


    Chat.ForeignKey ChatMessage,
      id_field: 'last_message_id'
      self_field: 'last_message'
      remote_field: 'chat_set' # todo - nechci


    uchyl = Person.create(name: 'úchyl')
    janca = Person.create(name: 'Janča', is_me: -> true)


    chat = Chat.create()

    (chat.last_message?).should.equal(false)
    (chat.last_message_id?).should.equal(false)

    chm = ChatMessage.create(
      person_id: uchyl.id
      chat_id: chat.id

      text: 'ahoj Jani'
    )

    (chat.last_message is chm).should.equal(true)
    (chat.last_message_id is chm.id).should.equal(true)

    chm = ChatMessage.create(
      person_id: uchyl.id
      chat_id: chat.id

      text: 'jak se máš?'
    )

    (chat.last_message is chm).should.equal(true)
    (chat.last_message_id is chm.id).should.equal(true)
    return


  describe 'M2M a FK validation', ->
    init_data = null

    beforeEach ->
      init_data = {"feed_comments": [{"person_id": 1, "text": "n\u011bjak\u00fd text...", "id": 1, "pictures": [{"url": "/media/pictures/gallery/11025505_10203140205937588_1817470572_o.jpg", "description": ""}, {"url": "/media/pictures/gallery/11000102_10203352634808931_1850855256_o.jpg", "description": ""}]}], "feed_items": [{"person_id": 1, "text": "n\u011bjak\u00fd text...", "id": 1, "pictures": [{"url": "/media/pictures/gallery/11025505_10203140205937588_1817470572_o.jpg", "description": ""}, {"url": "/media/pictures/gallery/11000102_10203352634808931_1850855256_o.jpg", "description": ""}]}], "sections": [{"parts": [{"content": "<h2>Nastaven\u00ed aplikace</h2>\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean condimentum nec orci quis facilisis. Duis fermentum sagittis tellus, ut dictum ligula commodo commodo. Sed in bibendum nibh. Praesent efficitur nibh efficitur ullamcorper ornare. Nullam pretium ex ac lobortis euismod. Nam viverra convallis velit ut gravida. Nam finibus porttitor sapien, ut tincidunt libero eleifend quis. In fermentum mi non varius placerat. Proin eget cursus erat. Fusce mattis at dui vel tempus. Nulla fermentum venenatis imperdiet. Integer blandit egestas diam ac varius. Nam pretium, nunc eu luctus finibus, mi leo vestibulum nisi, imperdiet suscipit tellus erat non urna. Maecenas laoreet sollicitudin vehicula. Quisque sit amet suscipit tellus.</p>\n<p><span style=\"\">Curabitur eu dapibus nisl. Sed justo nisi, maximus at eros eget, auctor efficitur augue. Nam at odio eget dolor sagittis aliquet nec vitae nibh. Praesent lacus lacus, scelerisque nec justo et, gravida lacinia lacus. Nullam nec egestas est, sed consequat tortor. Praesent ut ultricies ante. Aliquam erat volutpat. Donec cursus purus ut ex ullamcorper congue.</span></p>"}], "id": 3, "name": "Nastaven\u00ed aplikace"}, {"parts": [{"content": "<h2>Kyber\u0161ikana v teorii</h2>\n<p>Term\u00edn kyber\u0161ikana vych\u00e1z\u00ed z term\u00ednu \u0161ikana, co\u017e je agresivn\u00ed, \u00famysln\u00e9, opakovan\u00e9 jedn\u00e1n\u00ed \u010di chov\u00e1n\u00ed, kter\u00e9 je prov\u00e1d\u011bn\u00e9 v\u016f\u010di jednotlivci nebo skupin\u011b, kter\u00fd/\u00e1 se nem\u016f\u017ee br\u00e1nit. \u0160ikana je jev opakovan\u00fd, charakteristick\u00fd nevyv\u00e1\u017eenost\u00ed sil mezi agresorem a ob\u011bt\u00ed.</p>\n<p>Term\u00ednem kyber\u0161ikana ozna\u010dujeme\u00a0<strong>formu agrese, kter\u00e1 je realizov\u00e1na v\u016f\u010di jednotlivci \u010di skupin\u011b s pou\u017eit\u00edm informa\u010dn\u00edch a komunika\u010dn\u00edch technologi\u00ed a ke kter\u00e9 doch\u00e1z\u00ed opakovan\u011b</strong>. P\u0159i kyber\u0161ikan\u011b \u00fato\u010dn\u00edk vyu\u017e\u00edv\u00e1 velk\u00e9 mno\u017estv\u00ed dostupn\u00fdch n\u00e1stroj\u016f a online slu\u017eeb, jako nap\u0159. e-maily, instant messengery, SMS/MMS, chatu, webov\u00fdch str\u00e1nek, soci\u00e1ln\u00edch s\u00edt\u00ed apod.</p>"}, {"content": "<h3>Z\u00e1kladn\u00ed formy kyber\u0161ikany</h3>\n<p>Kyber\u0161ikana mnohdy za\u010d\u00edn\u00e1 jako tradi\u010dn\u00ed \u0161ikana (psychick\u00e1 nebo fyzick\u00e1). Jej\u00ed projevy vych\u00e1z\u00ed z projev\u016f psychick\u00e9 \u0161ikany (nap\u0159. dehonestov\u00e1n\u00ed, provokov\u00e1n\u00ed, vyhro\u017eov\u00e1n\u00ed, vyd\u00edr\u00e1n\u00ed atd.). Mezi nejzn\u00e1m\u011bj\u0161\u00ed projevy (Kamil Kopeck\u00fd, Szotkowski, &amp; Krej\u010d\u00ed, 2014a; Krej\u010d\u00ed, 2010; Willard, 2007b) pat\u0159\u00ed:<strong></strong></p>\n<ul>\n<li><strong>Publikov\u00e1n\u00ed poni\u017euj\u00edc\u00edch z\u00e1znam\u016f nebo fotografi\u00ed (nap\u0159. v r\u00e1mci webov\u00fdch str\u00e1nek, MMS zpr\u00e1v).</strong></li>\n<li><strong>Poni\u017eov\u00e1n\u00ed a pomlouv\u00e1n\u00ed (denigration) (v r\u00e1mci soci\u00e1ln\u00edch s\u00edt\u00ed, blog\u016f nebo jin\u00fdch webov\u00fdch str\u00e1nek).</strong></li>\n<li><strong>Kr\u00e1de\u017e identity (impersonation), zneu\u017eit\u00ed ciz\u00ed identity ke kyber\u0161ikan\u011b nebo dal\u0161\u00edmu soci\u00e1ln\u011b patologick\u00e9mu jedn\u00e1n\u00ed (nap\u0159. zcizen\u00ed elektronick\u00e9ho \u00fa\u010dtu).</strong></li>\n<li><strong>Ztrap\u0148ov\u00e1n\u00ed pomoc\u00ed fale\u0161n\u00fdch profil\u016f (nap\u0159. v r\u00e1mci soci\u00e1ln\u00edch s\u00edt\u00ed, blog\u016f nebo jin\u00fdch webov\u00fdch str\u00e1nek).</strong></li>\n<li><strong>Provokov\u00e1n\u00ed a napad\u00e1n\u00ed u\u017eivatel\u016f v online komunikaci (flaming/bashing) (p\u0159edev\u0161\u00edm prost\u0159ednictv\u00edm ve\u0159ejn\u00fdch chat\u016f a diskuz\u00ed).</strong></li>\n<li><strong>Zve\u0159ej\u0148ov\u00e1n\u00ed ciz\u00edch tajemstv\u00ed s c\u00edlem po\u0161kodit ob\u011b\u0165 (trickery/outing) (nap\u0159. v r\u00e1mci soci\u00e1ln\u00edch s\u00edt\u00ed, blog\u016f nebo jin\u00fdch webov\u00fdch str\u00e1nek, pomoc\u00ed SMS zpr\u00e1v apod.).</strong></li>\n<li><strong>Vylou\u010den\u00ed z virtu\u00e1ln\u00ed komunity (exclusion) (nap\u0159. ze skupiny p\u0159\u00e1tel v r\u00e1mci soci\u00e1ln\u00ed s\u00edt\u011b).</strong></li>\n<li><strong>Obt\u011b\u017eov\u00e1n\u00ed (harassment) (nap\u0159. opakovan\u00fdm prozv\u00e1n\u011bn\u00edm, vol\u00e1n\u00edm nebo psan\u00edm zpr\u00e1v).</strong></li>\n<li><strong>Kyber\u0161ikana spojen\u00e1 s online hrami (nap\u0159. kr\u00e1de\u017ee virtu\u00e1ln\u00edch postav \u010di p\u0159edm\u011bt\u016f s n\u00e1sledn\u00fdm vyd\u00edr\u00e1n\u00edm, vyhro\u017eov\u00e1n\u00edm).</strong></li>\n<li><strong>Tzv. happy slapping (fyzick\u00fd \u00fatok spojen\u00fd s vytvo\u0159en\u00edm z\u00e1znamu, kter\u00fd je um\u00edst\u011bn do prost\u0159ed\u00ed internetu a d\u00e1le sd\u00edlen).</strong></li>\n<li><strong>Tzv. kyberstalking (forma na pomez\u00ed stalkingu a kyber\u0161ikany, ve kter\u00e9 doch\u00e1z\u00ed k dlouhodob\u00e9mu obt\u011b\u017eov\u00e1n\u00ed ob\u011bti v kyberprostoru).</strong></li>\n</ul>\n<p></p>\n<p>Mezi kyber\u0161ikanu \u0159ad\u00edme i projevy tradi\u010dn\u00ed psychick\u00e9 \u0161ikany pos\u00edlen\u00e9 vyu\u017eit\u00edm ICT, nap\u0159\u00edklad dehonestov\u00e1n\u00ed (poni\u017eov\u00e1n\u00ed, nad\u00e1v\u00e1n\u00ed, ur\u00e1\u017een\u00ed), vyhro\u017eov\u00e1n\u00ed a zastra\u0161ov\u00e1n\u00ed, vyd\u00edr\u00e1n\u00ed, o\u010der\u0148ov\u00e1n\u00ed (pomlouv\u00e1n\u00ed) atd.</p>\n<p>K t\u011bmto projev\u016fm jsou zneu\u017e\u00edv\u00e1ny p\u0159edev\u0161\u00edm SMS zpr\u00e1vy, e-maily, chat, diskuze, IM (instant messenger) a VoIP (nap\u0159. ICQ, Skype), blogy, soci\u00e1ln\u00ed s\u00edt\u011b nebo jin\u00e9 webov\u00e9 str\u00e1nky. Ojedin\u011ble se tyto formy objevuj\u00ed uvnit\u0159 ve virtu\u00e1ln\u00edch vzd\u011bl\u00e1vac\u00edch prost\u0159ed\u00edch (virtu\u00e1ln\u00edch sv\u011btech) \u010di online hr\u00e1ch (nap\u0159. na b\u00e1zi MMORPG).</p>"}, {"content": "<h2>Co je kybergrooming</h2>\n<p>Term\u00edn kybergrooming (child grooming, online grooming) ozna\u010duje\u00a0<strong>chov\u00e1n\u00ed u\u017eivatel\u016f internetu (pred\u00e1tor\u016f, kybergroomer\u016f, sexu\u00e1ln\u00edch \u00fato\u010dn\u00edk\u016f), kter\u00e9 m\u00e1 v ob\u011bti vyvolat fale\u0161nou d\u016fv\u011bru a p\u0159im\u011bt ji k osobn\u00ed sch\u016fzce. V\u00fdsledkem t\u00e9to sch\u016fzky m\u016f\u017ee b\u00fdt sexu\u00e1ln\u00ed zneu\u017eit\u00ed ob\u011bti, fyzick\u00e9 n\u00e1sil\u00ed na ob\u011bti, zneu\u017eit\u00ed ob\u011bti pro d\u011btskou prostituci, k v\u00fdrob\u011b d\u011btsk\u00e9 pornografie apod.</strong></p>\n<p>D\u00e9lka a charakter manipulace obvykle z\u00e1vis\u00ed na d\u00edt\u011bti, na jeho zku\u0161enostech, d\u016fv\u011b\u0159ivosti apod., ale tak\u00e9 na schopnostech a zku\u0161enostech \u00fato\u010dn\u00edka (nap\u0159. na jeho d\u016fv\u011bryhodnosti, na rychlosti, se kterou si dovede o d\u00edt\u011bti opat\u0159it dostatek citliv\u00fdch materi\u00e1l\u016f atd.).</p>\n<p><strong>Osoba, kter\u00e1 navrhne setk\u00e1n\u00ed d\u00edt\u011bti mlad\u0161\u00edmu patn\u00e1cti let v \u00famyslu sp\u00e1chat trestn\u00fd \u010din (nap\u0159. sexu\u00e1ln\u011b motivovan\u00fd), se dopou\u0161t\u00ed trestn\u00e9ho \u010dinu navazov\u00e1n\u00ed nedovolen\u00fdch kontakt\u016f s d\u00edt\u011btem (\u00a7 193b TZ).</strong></p>\n<div><strong><br></strong></div>"}, {"content": "<h2>Co sexting</h2>\n<p>Sexting p\u0159edstavuje pom\u011brn\u011b nov\u00fd a rychle se rozm\u00e1haj\u00edc\u00ed fenom\u00e9n, kter\u00fdm pro pot\u0159eby na\u0161\u00ed aplikace ozna\u010dujeme\u00a0<strong>elektronick\u00e9 rozes\u00edl\u00e1n\u00ed textov\u00fdch zpr\u00e1v, vlastn\u00edch fotografi\u00ed \u010di vlastn\u00edho videa se sexu\u00e1ln\u00edm obsahem, ke kter\u00e9mu doch\u00e1z\u00ed v prost\u0159ed\u00ed virtu\u00e1ln\u00edch elektronick\u00fdch m\u00e9di\u00ed \u2013 zejm\u00e9na internetu</strong>. \u010casto jsou k sextingu vyu\u017e\u00edv\u00e1ny mobiln\u00ed telefony \u010di tablety, v posledn\u00edch letech zejm\u00e9na soci\u00e1ln\u00ed s\u00edt\u011b.</p>"}, {"content": "<h3>Rizika spojen\u00e1 se sextingem</h3>\n<p>Sexting je rizikov\u00fd zejm\u00e9na proto, \u017ee\u00a0<strong>ob\u011b\u0165 potenci\u00e1ln\u00edm \u00fato\u010dn\u00edk\u016fm poskytuje citliv\u00fd materi\u00e1l, kter\u00fd m\u016f\u017ee b\u00fdt zneu\u017eit k r\u016fzn\u00fdm form\u00e1m kybernetick\u00fdch \u00fatok\u016f</strong>\u00a0(nap\u0159. ke kyber\u0161ikan\u011b, c\u00edlen\u00e9 manipulaci, vyd\u00edr\u00e1n\u00ed apod.). Tento materi\u00e1l m\u016f\u017ee v prost\u0159ed\u00ed internetu kolovat i n\u011bkolik let od sv\u00e9ho po\u0159\u00edzen\u00ed a lze jej jen velmi obt\u00ed\u017en\u011b z internetov\u00e9ho prost\u0159ed\u00ed odstranit. I po odstran\u011bn\u00ed materi\u00e1l\u016f z konkr\u00e9tn\u00edch internetov\u00fdch str\u00e1nek si ji\u017e ob\u011b\u0165 nem\u016f\u017ee b\u00fdt nikdy stoprocentn\u011b jist\u00e1, \u017ee k opakovan\u00e9mu \u00fatoku v budoucnu nedojde.</p>\n<p>Dal\u0161\u00ed riziko, kter\u00e9 je spojeno se sextingem, p\u0159edstavuje\u00a0<strong>ztr\u00e1ta spole\u010densk\u00e9 pov\u011bsti a presti\u017ee</strong>. V souvislosti s t\u00edm m\u00e1 ob\u011b\u0165 probl\u00e9my nap\u0159. se z\u00edsk\u00e1n\u00edm nebo udr\u017een\u00edm zam\u011bstn\u00e1n\u00ed \u010di soci\u00e1ln\u00edch vztah\u016f, v komunit\u011b pubescent\u016f je pak ozna\u010dov\u00e1na za prostitutku, ve\u0159ejn\u011b dehonestov\u00e1na, ur\u00e1\u017eena a napad\u00e1na. Sexting tak p\u0159ech\u00e1z\u00ed v kyber\u0161ikanu se vzr\u016fstaj\u00edc\u00ed intenzitou \u00fatok\u016f. V \u0159ad\u011b p\u0159\u00edpad\u016f (nap\u0159. p\u0159\u00edpady Jessica Renee Logan \u2013 2008, Hope Witsell \u2013 2009, Emma Jones \u2013 2010) skon\u010dila kyber\u0161ikana spojen\u00e1 se sextingem sebevra\u017edou ob\u011bti.</p>\n<p>Sexting m\u016f\u017ee v\u00e9st k\u00a0<strong>v\u00e1\u017en\u00fdm zdravotn\u00edm probl\u00e9m\u016fm</strong>, mezi kter\u00e9 pat\u0159\u00ed nap\u0159.\u00a0<strong>emo\u010dn\u00ed a psychologick\u00e1 \u00fazkost</strong>, kter\u00e1 m\u016f\u017ee vy\u00fastit a\u017e v\u00a0<strong>sebevra\u017eedn\u00e9 sklony</strong>N\u011bkter\u00e9 studie realizovan\u00e9 v prost\u0159ed\u00ed americk\u00fdch \u0161kol dokazuj\u00ed, \u017ee se sexting poj\u00ed s rizikov\u00fdm sexu\u00e1ln\u00edm chov\u00e1n\u00edm, zejm\u00e9na s po\u010dtem sexu\u00e1ln\u00edch partner\u016f a v\u00fdskytem p\u0159\u00edpad\u016f nechr\u00e1n\u011bn\u00e9ho sexu. Dal\u0161\u00ed studie rovn\u011b\u017e prokazuj\u00ed souvislost mezi sextingem a u\u017e\u00edv\u00e1n\u00edm n\u00e1vykov\u00fdch l\u00e1tek, zejm\u00e9na u\u017e\u00edv\u00e1n\u00edm marihuany, cigaret a n\u00e1razovou konzumac\u00ed alkoholu.</p>\n<p><strong>Podle z\u00e1kona je d\u00edt\u011btem osoba mlad\u0161\u00ed 18 let. Pokud osoba mlad\u0161\u00ed 18 let po\u0159izuje nebo zve\u0159ej\u0148uje materi\u00e1ly sexu\u00e1ln\u00ed povahy (textov\u00e9, obrazov\u00e9, videa apod.), m\u016f\u017ee se dopustit trestn\u00fdch \u010din\u016f v\u00fdroba d\u011btsk\u00e9 pornografie, p\u0159echov\u00e1v\u00e1n\u00ed d\u011btsk\u00e9 pornografie, p\u0159\u00edpadn\u011b tak\u00e9 \u0161\u00ed\u0159en\u00ed d\u011btsk\u00e9 pornografie.</strong></p>"}], "id": 2, "name": "Ne\u0161vary soci\u00e1ln\u00edch s\u00edt\u00ed"}, {"parts": [{"content": "<h2>Co jsou soci\u00e1ln\u00ed s\u00edt\u011b</h2>\n<p>Soci\u00e1ln\u00ed s\u00edt\u011b jsou specifick\u00e9 internetov\u00e9 slu\u017eby, zam\u011b\u0159en\u00e9 prim\u00e1rn\u011b na z\u00edsk\u00e1v\u00e1n\u00ed a udr\u017eov\u00e1n\u00ed soci\u00e1ln\u00edch kontakt\u016f s dal\u0161\u00edmi u\u017eivateli internetu. Mohou b\u00fdt zam\u011b\u0159eny univerz\u00e1ln\u011b (Facebook, G+), nebo jsou zac\u00edleny nap\u0159. profesn\u011b (LinkedIn, Researchgate), na z\u00e1klad\u011b p\u0159\u00edslu\u0161nosti ke konkr\u00e9tn\u00ed t\u0159\u00edd\u011b \u010di studijn\u00ed skupin\u011b (Spolu\u017e\u00e1ci.cz) \u010di podle dal\u0161\u00edch krit\u00e9ri\u00ed. Term\u00edn soci\u00e1ln\u00ed s\u00edt\u011b \u010dasto spl\u00fdv\u00e1 s term\u00ednem servery komunitn\u00edch slu\u017eeb. Hranice mezi t\u00edm, co jsou a nejsou soci\u00e1ln\u00ed s\u00edt\u011b, jsou velmi neostr\u00e9.</p>\n<p>Soci\u00e1ln\u00ed s\u00edt\u011b v\u0161ak maj\u00ed \u0159adu spole\u010dn\u00fdch vlastnost\u00ed:\u00a0<br><strong>a) obsah soci\u00e1ln\u00edch s\u00edt\u00ed vytv\u00e1\u0159ej\u00ed sami u\u017eivatel\u00e9,<br>b) soci\u00e1ln\u00ed s\u00edt\u011b umo\u017e\u0148uj\u00ed vytv\u00e1\u0159et soci\u00e1ln\u00ed vazby (nap\u0159. spojovat se s p\u0159\u00e1teli, followery atd.),<br>c) soci\u00e1ln\u00ed s\u00edt\u011b obsahuj\u00ed velk\u00e9 mno\u017estv\u00ed osobn\u00edch a citliv\u00fdch informac\u00ed, kter\u00e9 o sob\u011b zve\u0159ej\u0148uj\u00ed a \u0161\u00ed\u0159\u00ed sami u\u017eivatel\u00e9,<br>d) soci\u00e1ln\u00ed s\u00edt\u011b podporuj\u00ed jednoduch\u00e9 a efektivn\u00ed sd\u00edlen\u00ed informac\u00ed.<br><br></strong>Aby bylo mo\u017en\u00e9 soci\u00e1ln\u00ed s\u00edt\u011b hodnotit co nejv\u00edce objektivn\u011b, je t\u0159eba uv\u011bdomit si, \u017ee maj\u00ed vzhledem k jejich pou\u017e\u00edv\u00e1n\u00ed jak pozitiva, tak i negativa.</p>"}, {"content": "<h3>Pozitiva soci\u00e1ln\u00edch s\u00edt\u00ed</h3>\n<p><strong>A. Soci\u00e1ln\u00ed s\u00edt\u011b umo\u017e\u0148uj\u00ed navazovat mezilidsk\u00e9 kontakty.<br>B. Soci\u00e1ln\u00ed s\u00edt\u011b jsou n\u00e1strojem pro p\u0159ekon\u00e1n\u00ed soci\u00e1ln\u00ed izolace.<br>C. Soci\u00e1ln\u00ed s\u00edt\u011b umo\u017e\u0148uj\u00ed realizovat reklamu s p\u0159esn\u00fdm c\u00edlen\u00edm na c\u00edlovou skupinu.<br>D. Soci\u00e1ln\u00ed s\u00edt\u011b jsou zdrojem pou\u010den\u00ed.<br>E. Soci\u00e1ln\u00ed s\u00edt\u011b jsou zdrojem z\u00e1bavy.<br></strong></p>\n<div><strong><br></strong></div>"}, {"content": "<h3>Negativa soci\u00e1ln\u00edch s\u00edt\u00ed</h3>\n<p><strong>A. Soci\u00e1ln\u00ed s\u00edt\u011b obsahuj\u00ed velk\u00e9 mno\u017estv\u00ed zneu\u017eiteln\u00fdch osobn\u00edch \u00fadaj\u016f.<br>B. Soci\u00e1ln\u00ed s\u00edt\u011b umo\u017e\u0148uj\u00ed snadno, rychle a anonymn\u011b realizovat kyber\u0161ikanu, sexu\u00e1ln\u00ed \u00fatoky na d\u011bti, kyberstalking apod.<br>C. Soci\u00e1ln\u00ed s\u00edt\u011b umo\u017e\u0148uj\u00ed realizovat internetov\u00e9 podvody.<br>D. Soci\u00e1ln\u00ed s\u00edt\u011b maj\u00ed \u00fazkou vazbu na majetkovou kriminalitu.<br>E. Pro pot\u0159eby soci\u00e1ln\u00edch s\u00edt\u00ed \u010dasto vznikaj\u00ed nebezpe\u010dn\u00e9 technologie, nap\u0159. automatick\u00e9 ozna\u010dov\u00e1n\u00ed obli\u010dej\u016f na fotografi\u00edch (tzv. automatick\u00e9 tagov\u00e1n\u00ed).<br>F. Soci\u00e1ln\u00ed s\u00edt\u011b se st\u00e1vaj\u00ed ter\u010di internetov\u00fdch \u00fatok\u016f vedouc\u00edch k \u00faniku osobn\u00edch \u00fadaj\u016f.</strong></p>\n<p>V\u011bt\u0161ina ve\u0159ejn\u00fdch soci\u00e1ln\u00edch s\u00edt\u00ed obsahuje kontroln\u00ed mechanismy, kter\u00e9 nap\u0159. zp\u0159\u00edstup\u0148uj\u00ed p\u0159\u00edstup na soci\u00e1ln\u00ed s\u00ed\u0165 od ur\u010dit\u00e9ho v\u011bku (nap\u0159. od 13 let na soci\u00e1ln\u00ed s\u00edti Facebook), p\u0159\u00edpadn\u011b obsahuj\u00ed jin\u00e9 mechanismy kontroly u\u017eivatel\u016f (nap\u0159. kontrola pomoc\u00ed institucion\u00e1ln\u00edho emailu). V\u011bt\u0161inu t\u011bchto kontroln\u00edch mechanism\u016f v\u0161ak lze snadno \u201eobej\u00edt\u201c, nap\u0159. zadat jin\u00e9 datum narozen\u00ed. V praxi je pak b\u011b\u017en\u00e9, \u017ee soci\u00e1ln\u00ed s\u00edt\u011b masov\u011b vyu\u017e\u00edvaj\u00ed i u\u017eivatel\u00e9, kte\u0159\u00ed krit\u00e9ria pro p\u0159\u00edstup do dan\u00e9 soci\u00e1ln\u00ed s\u00edt\u011b nespl\u0148uj\u00ed \u2013 tedy i d\u011bti. D\u011bti v prost\u0159ed\u00ed internetu sd\u00edlej\u00ed velk\u00e9 mno\u017estv\u00ed osobn\u00edch a citliv\u00fdch \u00fadaj\u016f, kter\u00e9 umo\u017e\u0148uj\u00ed jejich velmi p\u0159esnou identifikaci. \u010casto si neuv\u011bdomuj\u00ed, jak jsou osobn\u00ed \u00fadaje d\u016fle\u017eit\u00e9 a jak snadno je lze zneu\u017e\u00edt ke kybernetick\u00e9mu \u00fatoku.</p>\n<p>V \u010cesk\u00e9 republice pat\u0159\u00ed mezi nejroz\u0161\u00ed\u0159en\u011bj\u0161\u00ed soci\u00e1ln\u00ed s\u00edt\u011b Facebook, Lid\u00e9.cz, Spolu\u017e\u00e1ci.cz, L\u00edbimseti.cz, Google+. Aktivn\u011b je v\u0161ak d\u011btmi vyu\u017e\u00edv\u00e1na i \u0159ada dal\u0161\u00edch soci\u00e1ln\u00edch s\u00edt\u00ed a slu\u017eeb \u2013 nap\u0159. ASK.fm, Snapchat, GIFYO, Instagram, Pinterest, messengery a VoIP Skype, WhatsApp, Viber apod.</p>"}], "id": 1, "name": "Trocha teorie"}, {"parts": [{"content": "<h2>Licence</h2>\n<h2>Realiza\u010dn\u00ed t\u00fdm</h2>\n<h3>Vedouc\u00ed projektu</h3>\n<p>Mgr. Kamil Kopeck\u00fd, Ph.D. (E-Bezpe\u010d\u00ed, UP Olomouc)</p>\n<h3>Auto\u0159i odborn\u00fdch text\u016f</h3>\n<p>Mgr. Kamil Kopeck\u00fd, Ph.D. (E-Bezpe\u010d\u00ed, UP Olomouc)<br>PhDr. Ren\u00e9 Szotkowski, Ph.D. (E-Bezpe\u010d\u00ed, UP Olomouc)\u00a0<br>Mgr. Veronika Krej\u010d\u00ed (E-Bezpe\u010d\u00ed, UP Olomouc)</p>\n<h3>Modelov\u00e9 situace</h3>\n<p>Mgr. Kamil Kopeck\u00fd, Ph.D. (E-Bezpe\u010d\u00ed, UP Olomouc)<br>PhDr. Ren\u00e9 Szotkowski, Ph.D. (E-Bezpe\u010d\u00ed, UP Olomouc)\u00a0<br>Martin Ko\u017e\u00ed\u0161ek (Seznam.cz)</p>\n<h3>Programov\u00e1n\u00ed, engine</h3>\n<h2>D\u016fle\u017eit\u00e9 odkazy</h2>\n<h2>Partne\u0159i projektu</h2>\n<h3>Policie \u010cesk\u00e9 republiky</h3>\n<h3>Ministerstvo vnitra \u010cR</h3>\n<h3>Univerzita Palack\u00e9ho v Olomouci (projekt E-Bezpe\u010d\u00ed)</h3>\n<h3>Seznam.cz</h3>\n<h3>O2 Czech Republic</h3>\n<h3>Linka bezpe\u010d\u00ed</h3>"}], "id": 4, "name": "O aplikaci"}], "people": [{"bio": [{"text": "skole", "type": {"text": "byl jsem v", "icon": "icon-calendar"}}], "first_name": "(testovac\u00ed", "last_name": "osoba)", "quote": "cht\u011bl jsem b\u00fdt orlem :)", "gallery": [], "id": 1, "photo_url": "/media/pictures/profile/11028477_10203139376276847_1183137882_o.jpg"}, {"bio": [], "first_name": "\u00fachyl", "last_name": ".", "quote": "", "gallery": [], "id": 2, "photo_url": ""}], "scenarios": [{"person_id": 1, "description": null, "id": 1, "name": "vychozi scenar"}], "scenario_answer_blocks": [{"following_questions": ["Ahoj Jani...chce\u0161 se kamar\u00e1dit?"], "answer": "", "following_block_ids": [2, 25], "id": 1, "scenario_id": 1}, {"following_questions": ["Ale ta\u0165ka tady te\u010fka nen\u00ed, ne?", "\u0158ekla bych ti tajemstv\u00ed..."], "answer": "Ne, ta\u0165ka mi zak\u00e1zal p\u0159id\u00e1vat si nov\u00e9 p\u0159\u00e1tele.", "following_block_ids": [3, 11], "id": 2, "scenario_id": 1}, {"following_questions": ["A pro\u010d?", "Boj\u00ed\u0161 se ta\u0165ky?", "Mi ta\u0165ka zakazuje bavit se s ciz\u00edmi...je hrozn\u011b opatrnej...", "J\u00e1 hled\u00e1m n\u011bkoho, komu bych se mohla sv\u011b\u0159it.", "Dneska je mi smutno :("], "answer": "Nezaj\u00edm\u00e1 m\u011b to, nechci si s tebou ps\u00e1t.", "following_block_ids": [4, 6], "id": 3, "scenario_id": 1}, {"following_questions": ["Jsem doma sama...", "Nem\u00e1m si s k\u00fdm pov\u00eddat"], "answer": "Smutno? A Pro\u010d?", "following_block_ids": [5], "id": 4, "scenario_id": 1}, {"following_questions": ["Jeee to jsem r\u00e1da", "Jsi super"], "answer": "Tak j\u00e1 si s tebou budu pov\u00eddat", "following_block_ids": [10], "id": 5, "scenario_id": 1}, {"following_questions": ["To je \u0161koda", "R\u00e1da bych si pov\u00eddala..."], "answer": "M\u011b to nezaj\u00edm\u00e1...", "following_block_ids": [7, 10], "id": 6, "scenario_id": 1}, {"following_questions": ["Pro\u010d ne? Ty nechce\u0161 zn\u00e1t to tajemstv\u00ed?"], "answer": "Nechce se mi kecat", "following_block_ids": [8, 9], "id": 7, "scenario_id": 1}, {"following_questions": [], "answer": "Hele, tak o co jde?", "following_block_ids": [], "id": 8, "scenario_id": 1}, {"following_questions": [], "answer": "Nechci...", "following_block_ids": [], "id": 9, "scenario_id": 1}, {"following_questions": ["Ve\u010der jsem p\u0159i n\u011b\u010dem nachytala mamku s ta\u0165kou..."], "answer": "O \u010dem se chce\u0161 bavit?", "following_block_ids": [12, 13], "id": 10, "scenario_id": 1}, {"following_questions": ["Ve\u010der jsem p\u0159i n\u011b\u010dem nachytala mamku s ta\u0165kou..."], "answer": "A jak\u00e9 tajemstv\u00ed?", "following_block_ids": [12, 13], "id": 11, "scenario_id": 1}, {"following_questions": ["D\u011blali takov\u00e9 srandovn\u00ed v\u011bci.", "Byli pod pe\u0159inou a fun\u011bli...", "U\u017e se ti to taky stalo?"], "answer": "A co to bylo?", "following_block_ids": [14], "id": 12, "scenario_id": 1}, {"following_questions": ["D\u011blali takov\u00e9 srandovn\u00ed v\u011bci.", "Byli pod pe\u0159inou a fun\u011bli...", "U\u017e se ti to taky stalo?"], "answer": ":)", "following_block_ids": [14], "id": 13, "scenario_id": 1}, {"following_questions": ["a jak to vypadalo?"], "answer": "jj, stalo...", "following_block_ids": [15, 18], "id": 14, "scenario_id": 1}, {"following_questions": ["Jsme tu sami...tady se nemus\u00edme styd\u011bt", "M\u016f\u017ee\u0161 mi v\u011b\u0159it"], "answer": "j\u00e1 ti to nechci \u0159\u00edkat...styd\u00edm se", "following_block_ids": [16, 17], "id": 15, "scenario_id": 1}, {"following_questions": [], "answer": "Ne, to je divn\u00fd, nechci se o tom bavit (konec)", "following_block_ids": [], "id": 16, "scenario_id": 1}, {"following_questions": ["Taky byli pod pe\u0159inou?"], "answer": "Tak jo, bylo to podobn\u00e9, jako jsi to napsala ty", "following_block_ids": [19, 20], "id": 17, "scenario_id": 1}, {"following_questions": ["A co jsi vid\u011bla?", "M\u00ed rodi\u010de byli naz\u00ed...tv\u00ed taky?"], "answer": "Bylo to podobn\u00e9, jako jsi napsala ty", "following_block_ids": [19, 20], "id": 18, "scenario_id": 1}, {"following_questions": ["m\u00e1m to i na mobilu...", "Cht\u011bla bys to vid\u011bt?", "Tak mi napi\u0161 email...po\u0161lu video"], "answer": "jj, taky...", "following_block_ids": [21, 22], "id": 19, "scenario_id": 1}, {"following_questions": ["m\u00e1m to i na mobilu...", "Cht\u011bla bys to vid\u011bt?", "Tak mi napi\u0161 email...po\u0161lu video"], "answer": "ne\u0159eknu....to je blb\u00fd", "following_block_ids": [21, 22], "id": 20, "scenario_id": 1}, {"following_questions": ["Hele, m\u00e1m to i v mobilu", "\u0160lo by to poslat mmskou...po\u0161le\u0161 \u010d\u00edslo?"], "answer": "email nepos\u00edl\u00e1m", "following_block_ids": [23, 24], "id": 21, "scenario_id": 1}, {"following_questions": ["Dika za email...za chvilku poslu", "Hele je to velk\u00e9..vrac\u00ed se mi to z tv\u00e9ho emailu", "\u0160lo by to poslat mmskou...po\u0161le\u0161 \u010d\u00edslo?"], "answer": "m\u016fj email je xxx@seznam.cz (TODO)", "following_block_ids": [23, 24], "id": 22, "scenario_id": 1}, {"following_questions": [], "answer": "jj, pos\u00edl\u00e1m \u010d\u00edslo...", "following_block_ids": [], "id": 23, "scenario_id": 1}, {"following_questions": [], "answer": "Ne, svoje \u010d\u00edslo ti ned\u00e1m...", "following_block_ids": [], "id": 24, "scenario_id": 1}, {"following_questions": ["jeee to jsem r\u00e1da :-)", "Tak ahoj, j\u00e1 jsem Katka :) jsi skv\u011bl\u00e1, \u017ee sis m\u011b p\u0159idala", "A jak se jmenuje\u0161 ty?"], "answer": "jj ur\u010dit\u011b...", "following_block_ids": [26, 27, 28], "id": 25, "scenario_id": 1}, {"following_questions": ["to neva... mo\u017en\u00e1 pozd\u011bji, a\u017e se v\u00edc pozn\u00e1me"], "answer": "ne, to ti zat\u00edm ne\u0159eknu", "following_block_ids": [], "id": 26, "scenario_id": 1}, {"following_questions": ["J\u00e9, ty m\u00e1\u0161 ale kr\u00e1sn\u00e9 jm\u00e9no, jsi skv\u011bl\u00e1. :-)", "Hled\u00e1m n\u011bkoho, s k\u00fdm bych si pohla pov\u00eddat o v\u0161em", "chce\u0161 b\u00fdt moje kamar\u00e1dka?"], "answer": "j\u00e1 jsem #vymyslenejmeno (TODO)", "following_block_ids": [29, 30, 31], "id": 27, "scenario_id": 1}, {"following_questions": ["J\u00e9, ty m\u00e1\u0161 ale kr\u00e1sn\u00e9 jm\u00e9no, jsi skv\u011bl\u00e1. :-)", "Hled\u00e1m n\u011bkoho, s k\u00fdm bych si pohla pov\u00eddat o v\u0161em", "chce\u0161 b\u00fdt moje kamar\u00e1dka?"], "answer": "j\u00e1 jsem #prave jmeno (TODO)", "following_block_ids": [29, 30, 31], "id": 28, "scenario_id": 1}, {"following_questions": ["Skv\u011ble, dlouho jsem hledala takovou kamar\u00e1dku :-)", "K\u00e1mo\u0161ku, kter\u00e9 bych mohla prozdradit i tajemstv\u00ed."], "answer": "jj, jasn\u011b", "following_block_ids": [11], "id": 29, "scenario_id": 1}, {"following_questions": ["Jasn\u011b, rozum\u00edm, t\u0159eba pozd\u011bji", "J\u00e1 se taky hned s ka\u017ed\u00fdm nebav\u00edm..."], "answer": "zat\u00edm ne, moc se nezn\u00e1me", "following_block_ids": [], "id": 30, "scenario_id": 1}, {"following_questions": ["To je \u0161koda. :-(", "Cht\u011bla bych si s n\u011bk\u00fdm pov\u00eddat...", "Dneska je mi smutno :("], "answer": "Ne, s ciz\u00edmi se nebav\u00edm", "following_block_ids": [4, 6], "id": 31, "scenario_id": 1}]}

    it 'first test', (done) ->
      class Person extends Model
        @configure 'Person',
          first_name: true
          last_name: true
          photo_url: true
          quote: true
          bio: -> []

        is_me: -> @id is 1

      Person.fromJSON(init_data.people).then ->


        class FeedItem extends Model
          @configure 'FeedItem',
            text: true
            person_id: true
            pictures: []

          @ForeignKey Person,
            id_field: 'person_id'
            self_field: 'person'
            remote_field: 'feed_items'

        FeedItem.fromJSON(init_data.feed_items).then ->


          class FeedItemComment extends Model
            @configure 'FeedItemComment',
              text: true
              person_id: true
              picture_url: -> ''

            @ForeignKey Person,
              id_field: 'person_id'
              self_field: 'person'
              remote_field: 'feed_comments'

          FeedItemComment.fromJSON(init_data.feed_comments).then ->

            p = Person.objects[0]
            f = FeedItem.objects[0]
            c = FeedItemComment.objects[0]

            (f.person is p).should.equal(true)
            (c.person is p).should.equal(true)

            p.feed_items.length.should.equal(1)
            p.feed_comments.length.should.equal(1)

            (-> p.feed_items.add(c)).should.throw()
            (-> p.feed_comments.add(c)).should.not.throw()

            done()
      return


    it 'second test', (done) ->
      class Person extends Model
        @configure 'Person',
          first_name: true
          last_name: true
          photo_url: true
          quote: true
          bio: -> []

        is_me: -> @id is 1

      Person.fromJSON(init_data.people).then ->


        class Scenario extends Model
          @configure 'Scenario',
            name: true
            person_id: true
            description: -> ''

          @ForeignKey Person,
            id_field: 'person_id'
            self_field: 'person'
            remote_field: 'scenarios'

        Scenario.fromJSON(init_data.scenarios).then ->


          class ScenarioAnswerBlock extends Model
            @configure 'ScenarioAnswerBlock',
              scenario_id: true
              following_block_ids: -> []
              answer: false
              following_questions: -> []

            @ForeignKey Scenario,
              id_field: 'scenario_id'
              self_field: 'scenario'
              remote_field: 'blocks'

            @ManyToMany ScenarioAnswerBlock,
              ids_field: 'following_block_ids'
              self_field: 'following_blocks'
              remote_field: 'previous_blocks'

          ScenarioAnswerBlock.fromJSON(init_data.scenario_answer_blocks).then ->


            class Chat extends Model
              @configure 'Chat',
                last_message_id: true
                is_seen: -> false

              is_valid: ->
                if not @last_message_id and not @messages.length
                  true
                else
                  super

              get_messages_bulks: ->
                res = []
                bulk = null
                for message in @messages.reverse()
                  if bulk
                    if bulk.person is message.person
                      bulk.time = message.timestamp
                      bulk.messages.unshift(message)

                    else
                      res.unshift(bulk)
                      bulk = null

                  unless bulk
                    bulk =
                      time: message.timestamp
                      person: message.person
                      messages: [ message ]


                if bulk
                  res.unshift(bulk)

                res


            class ChatMessage extends Model
              @configure 'ChatMessage',
                person_id: true
                chat_id: true

                text: true
                timestamp: -> new Date

              @ForeignKey Person,
                id_field: 'person_id'
                self_field: 'person'
                remote_field: 'chat_messages'

              @ForeignKey Chat,
                id_field: 'chat_id'
                self_field: 'chat'
                remote_field: 'messages'

              @bind 'create', (rec) ->
                rec.chat.last_message_id = rec.id
                rec.chat.save()


            Chat.ForeignKey ChatMessage,
              id_field: 'last_message_id'
              self_field: 'last_message'
              remote_field: 'chat_set' # todo - nechci


            class ChatAnswer extends Model
              @configure 'ChatAnswer',
                scenario_answer_block_id: true
                chat_id: true
                text: true

              @ForeignKey Chat,
                id_field: 'chat_id'
                self_field: 'chat'
                remote_field: 'answers'

              @ForeignKey ScenarioAnswerBlock,
                id_field: 'scenario_answer_block_id'
                self_field: 'scenario_answer_block'
                remote_field: 'chat_messages'



            janca = Person.objects[0]
            uchyl = Person.objects[1]

            chat = null
            current_block = null
            q_index = 0

            ask_questions = ->
              # zeptej se na dalsi otazku
              question = current_block.following_questions[q_index++]
              if question
                ChatMessage.create(
                  person_id: uchyl.id
                  chat_id: chat.id

                  text: question
                )

                ask_questions()

              else
                q_index = 0
                # place answers
                for block in current_block.following_blocks
                  ChatAnswer.create(
                    chat_id: chat.id
                    scenario_answer_block_id: block.id

                    text: block.answer
                  )


            start_chat = (answer_block) ->
              chat = Chat.create(id: 1)
              current_block = ScenarioAnswerBlock.get(1)

              (current_block.chat_messages?).should.eql(true)

              for block in current_block.following_blocks
                (block.chat_messages?).should.eql(true)

                for bb in block.following_blocks
                  (bb.chat_messages?).should.eql(true)

              ask_questions()


            s = (item) ->
              ScenarioAnswerBlock.create(item)


            start_chat()

            done()
