{int, from_k, floor, len} = require '../app/lib/extras'


params =
  SHOULD_WORKED : -> '08:00'
  WORKED        : -> '08:51'
  IS_WEEKEND    : -> '1'
  IS_HOLIDAY    : -> '0'


TIME_REGEX = /(\d{1,3})\:(\d{2})/


TIME =
  to_string: (value) ->
    hours   = floor(value / 60)
    minutes = value % 60

    hours   = "0#{hours}"   if len(hours)   is 1
    minutes = "0#{minutes}" if len(minutes) is 1

    "#{hours}:#{minutes}"


  to_number: (value) ->
    return value if typeof value is 'number'
    [_matched, hours, minutes] = value.match(TIME_REGEX)
    int(hours) * 60 + int(minutes)


NUMBER =
  to_string: (value) -> str(value)
  to_number: (value) -> int(value)


parse_to_string = (value) ->
  while match = value.match(/(\{[^\}]+\})/)?[1]
    value = value.replace(
      match,
      params[match[1...-1]]
    )

  value


convert_times_to_numbers = (value) ->
  while match = value.match(TIME_REGEX)?[0]
    value = value.replace(
      match,
      str(TIME.to_number(match))
    )

  value


eval_numbers = (value) ->
  value = value.replace /\ /g, ''
  throw new Error('not valid pattern') unless /^[0-9\-\+\/\*\(\)\.\<\>\=]+$/.test(value)

  eval(value)


eval_expresion = (value) ->
  eval_numbers(
    convert_times_to_numbers(
      parse_to_string(value)
    )
  )



describe 'Maerks test suites', ->
  it 'should be able to convert time to numbers and back', ->
    TIME.to_string(123).should.equal('02:03')
    TIME.to_string(636).should.equal('10:36')

    TIME.to_number('02:03').should.equal(123)
    TIME.to_number('2:03').should.equal(123)
    TIME.to_number('160:05').should.equal(9605)
    return


  it 'should not fail when given already number', ->
    TIME.to_number(9605).should.equal(9605)
    return


  describe 'complex expressions', ->
    it 'should convert params to string numbers', ->
      parse_to_string('{SHOULD_WORKED} - 2 + {IS_WEEKEND}').should.equal('08:00 - 2 + 1')
      return

    it 'should be able to match all numbers and times and convert it just to numbers', ->
      convert_times_to_numbers('5:31 - 600 + 201 - 01:00').should.equal('331 - 600 + 201 - 60')
      return

    it 'should be able to eval expression', ->
      eval_numbers('331 - 600 + 201 - 60.5').should.equal(-128.5)
      eval_numbers('(331 - 600) * 2 < 201').should.equal(true)
      eval_numbers('600 < 201').should.equal(false)
      return

    it 'should raise an exception when given wrong (evil) expression', ->
      eval_numbers.bind(null, '600 < a = 5').should.throw()
      return

    it 'should be able to compute whole expression', ->
      eval_expresion('({SHOULD_WORKED} - 200) * {IS_WEEKEND} * {IS_HOLIDAY}').should.equal(0)
      return