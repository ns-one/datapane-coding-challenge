{process_event} = require '../app/lib/events'
{extend, thread} = require '../app/lib/extras'
{queue, Promise} = require '../app/lib/promise'



describe 'Events', ->
  $event = undefined
  $scope = undefined
  $el    = undefined
  cbDefs = undefined

  beforeEach ->
    $event =
      target: 'CSS selector'
      $$is_generated: true

    $scope =
      key: 'value'
      deep:
        value: 'deep value'


  describe 'process_event (+ apply_handlers, etc.)', ->
    describe.skip 'sync', ->
      it 'simple scenario', ->
        processed_order = []

        $scope.$$component =
          handlers:
            cb_name_1: (atts) ->
              processed_order.push('cb_name_1')
              return

            cb_name_2: (atts) ->
              processed_order.push('cb_name_2')
              return

            cb_name_3: (atts) ->
              processed_order.push('cb_name_3')
              return

            cb_name_4: (atts) ->
              processed_order.push('cb_name_4')
              return

            cb_name_5: (atts) ->
              processed_order.push('cb_name_5')
              return

        processed_order = []
        cbDefs = 'cb_name_1'
        process_event($event, $el, $scope, cbDefs)
        processed_order.should.eql(['cb_name_1'])

        processed_order = []
        cbDefs = 'cb_name_1; cb_name_2'
        process_event($event, $el, $scope, cbDefs)
        processed_order.should.eql(['cb_name_1', 'cb_name_2'])

        processed_order = []
        cbDefs = 'cb_name_1; cb_name_2; cb_name_3'
        process_event($event, $el, $scope, cbDefs)
        processed_order.should.eql(['cb_name_1', 'cb_name_2', 'cb_name_3'])

        processed_order = []
        cbDefs = 'cb_name_1; cb_name_2; cb_name_3; cb_name_4'
        process_event($event, $el, $scope, cbDefs)
        processed_order.should.eql(['cb_name_1', 'cb_name_2', 'cb_name_3', 'cb_name_4'])

        processed_order = []
        cbDefs = 'cb_name_1; cb_name_2; cb_name_3; cb_name_4; cb_name_5'
        process_event($event, $el, $scope, cbDefs)
        processed_order.should.eql(['cb_name_1', 'cb_name_2', 'cb_name_3', 'cb_name_4', 'cb_name_5'])
        return

      it 'virtual events\' scenario', ->
        # mock event's handlers, that will set some outer-scope's value to true
        processed_order = []

        $scope.$$component =
          handlers:
            cb_name_1: (atts) ->
              processed_order.push('cb_name_1')

              # add handler for it
              cbd = 'cb_name_4; cb_name_5; cb_name_6'

              process_event($event, $el, $scope, cbd)
              return

            cb_name_2: (atts) ->
              {key} = atts
              processed_order.push('cb_name_2')
              return

            cb_name_3: (atts) ->
              {key} = atts
              processed_order.push('cb_name_3')
              return

            cb_name_4: (atts) ->
              {key} = atts
              processed_order.push('cb_name_4')
              return

            cb_name_5: (atts) ->
              processed_order.push('cb_name_5')

              # add handler for it
              cbd = 'cb_name_7; cb_name_8;'

              process_event($event, $el, $scope, cbd)
              return

            cb_name_6: (atts) ->
              {key} = atts
              processed_order.push('cb_name_6')
              return

            cb_name_7: (atts) ->
              {key} = atts
              processed_order.push('cb_name_7')
              return

            cb_name_8: (atts) ->
              {key} = atts
              processed_order.push('cb_name_8')
              return

        # cbDefs - on-click's value
        cbDefs = 'cb_name_1; cb_name_2; cb_name_3;'

        # process event
        process_event($event, $el, $scope, cbDefs)

        # assert testing values
        processed_order.should.eql(['cb_name_1', 'cb_name_4', 'cb_name_5', 'cb_name_7', 'cb_name_8', 'cb_name_6', 'cb_name_2', 'cb_name_3'])
        return


    describe 'async', ->
      it 'can process chained promise', (done) ->
        processed_order = []

        $scope.$$component =
          handlers:
            cb_name_1: (atts) ->
              processed_order.push('cb_name_1')
              new Promise (reject, resolve) ->
                thread ->

                  resolve()

              .then ->
                processed_order.push('cb_name1.1')
                return

        cbDefs = 'cb_name_1'
        process_event($event, $el, $scope, cbDefs).then ->
          processed_order.should.eql(['cb_name_1', 'cb_name1.1'])
          done()
          return


      it 'simple scenario', ->
        processed_order = []

        $scope.$$component =
          handlers:
            cb_name_1: (atts) ->
              processed_order.push('cb_name_1')
              new Promise (reject, resolve) ->
                thread -> resolve()

            cb_name_2: (atts) ->
              processed_order.push('cb_name_2')
              new Promise (reject, resolve) ->
                thread -> resolve()

            cb_name_3: (atts) ->
              processed_order.push('cb_name_3')
              new Promise (reject, resolve) ->
                thread -> resolve()

            cb_name_4: (atts) ->
              processed_order.push('cb_name_4')
              new Promise (reject, resolve) ->
                thread -> resolve()

            cb_name_5: (atts) ->
              processed_order.push('cb_name_5')
              new Promise (reject, resolve) ->
                thread -> resolve()

        queue 'test_q', ->
          processed_order = []
          cbDefs = 'cb_name_1'
          process_event($event, $el, $scope, cbDefs).then ->
            processed_order.should.eql(['cb_name_1'])
            return

        queue 'test_q', ->
          processed_order = []
          cbDefs = 'cb_name_1; cb_name_2'
          process_event($event, $el, $scope, cbDefs).then ->
            processed_order.should.eql(['cb_name_1', 'cb_name_2'])
            return

        queue 'test_q', ->
          processed_order = []
          cbDefs = 'cb_name_1; cb_name_2; cb_name_3'
          process_event($event, $el, $scope, cbDefs).then ->
            processed_order.should.eql(['cb_name_1', 'cb_name_2', 'cb_name_3'])
            return

        queue 'test_q', ->
          processed_order = []
          cbDefs = 'cb_name_1; cb_name_2; cb_name_3; cb_name_4'
          process_event($event, $el, $scope, cbDefs).then ->
            processed_order.should.eql(['cb_name_1', 'cb_name_2', 'cb_name_3', 'cb_name_4'])
            return

        queue 'test_q', ->
          processed_order = []
          cbDefs = 'cb_name_1; cb_name_2; cb_name_3; cb_name_4; cb_name_5'
          process_event($event, $el, $scope, cbDefs).then ->
            processed_order.should.eql(['cb_name_1', 'cb_name_2', 'cb_name_3', 'cb_name_4', 'cb_name_5'])
            return

        queue.promise 'test_q', ->


      it 'virtual events\' scenario', ->
        # mock event's handlers, that will set some outer-scope's value to true
        processed_order = []

        $scope.$$component =
          handlers:
            cb_name_1: (atts) ->
              processed_order.push('cb_name_1')
              new Promise (reject, resolve) ->
                thread ->
                  cbd = 'cb_name_4; cb_name_5; cb_name_6'
                  process_event($event, $el, $scope, cbd).then ->
                    resolve()

            cb_name_2: (atts) ->
              processed_order.push('cb_name_2')
              new Promise (reject, resolve) ->
                thread -> resolve()

            cb_name_3: (atts) ->
              processed_order.push('cb_name_3')
              new Promise (reject, resolve) ->
                thread -> resolve()

            cb_name_4: (atts) ->
              processed_order.push('cb_name_4')
              new Promise (reject, resolve) ->
                thread -> resolve()

            cb_name_5: (atts) ->
              processed_order.push('cb_name_5')
              new Promise (reject, resolve) ->
                thread ->
                  cbd = 'cb_name_7; cb_name_8'
                  process_event($event, $el, $scope, cbd).then ->
                    resolve()

            cb_name_6: (atts) ->
              processed_order.push('cb_name_6')
              new Promise (reject, resolve) ->
                thread -> resolve()

            cb_name_7: (atts) ->
              processed_order.push('cb_name_7')
              new Promise (reject, resolve) ->
                thread -> resolve()

            cb_name_8: (atts) ->
              processed_order.push('cb_name_8')
              new Promise (reject, resolve) ->
                thread -> resolve()

        cbDefs = 'cb_name_1; cb_name_2; cb_name_3;'
        process_event($event, $el, $scope, cbDefs).then ->
          processed_order.should.eql(['cb_name_1', 'cb_name_4', 'cb_name_5', 'cb_name_7', 'cb_name_8', 'cb_name_6', 'cb_name_2', 'cb_name_3'])
          return


    describe 'parsing values', ->
      it 'can parse variables', (done) ->
        $scope.$$component =
          handlers:
            cb_name: (atts) ->
              {key, deep} = atts

              key.should.equal 'value'
              deep.should.equal 'deep value'

              done()
              return


        cbDefs =
          'cb_name:
            key,
            deep.value as deep'

        process_event($event, $el, $scope, cbDefs)
        return

      it 'can parse numbers', (done) ->
        $scope.$$component =
          handlers:
            cb_name: (atts) ->
              {number1, number2} = atts

              number1.should.equal 4114.25
              number2.should.equal - 1425

              done()
              return


        cbDefs =
          'cb_name:
            4114.25 as number1,
            - 14.25 as number2'

        process_event($event, $el, $scope, cbDefs)
        return

      it 'can parse texts', (done) ->
        $scope.$$component =
          handlers:
            cb_name: (atts) ->
              {text1, text2} = atts

              text1.should.equal 'nazev1'
              text2.should.equal 'nazev2'

              done()
              return


        cbDefs =
          'cb_name:
            \'nazev1\' as text1,
            "nazev2" as text2'

        process_event($event, $el, $scope, cbDefs)
        return

      it 'can parse booleans', (done) ->
        $scope.$$component =
          handlers:
            cb_name: (atts) ->
              {bool1, bool2, bool3} = atts

              (bool1 is true).should.equal true
              (bool2 is false).should.equal true
              (bool3 is null).should.equal true

              done()
              return


        cbDefs =
          'cb_name:
            true as bool1,
            false as bool2,
            null as bool3'

        process_event($event, $el, $scope, cbDefs)
        return

      it 'can parse $scope as a self attribute', (done) ->
        $scope.$$component =
          handlers:
            cb_name: (atts) ->
              {self} = atts

              (self is $scope).should.equal true

              done()
              return


        cbDefs =
          'cb_name:
            false as bool2,
            self,
            null as bool3'

        process_event($event, $el, $scope, cbDefs)
        return




  describe 'call Components\' inner/outer scope', ->
    it.skip 'TODO', ->
