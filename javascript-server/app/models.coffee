{len, sum} = require './lib/extras'
{http} = require './lib/http'
{Model, RemoteModel} = require './lib/models'



class FormEntry extends RemoteModel
  @configure 'FormEntry',
    name: true
    age: true
    email: true

  @custom_entity_name: 'form-entries'

  validation_errors: ->
    errors = {}

    unless typeof @name is 'string'
      errors.name ?= []
      errors.name.push('Name must be type of `string`!')

    unless /\D{3,} \D{3,}/.test(@name)
      errors.name ?= []
      errors.name.push('Name is not in correct format')

    if /devil/i.test(@name)
      errors.name ?= []
      errors.name.push('Please, don\'t be Evil with name!')

    unless typeof @age is 'number'
      errors.age ?= []
      errors.age.push('Age must be type of `number!')

    if @age < 0
      errors.age ?= []
      errors.age.push('Age below zero is nonsense!')

    unless typeof @email is 'string'
      errors.email ?= []
      errors.email.push('Email msut be type of `string`!')

    unless /^\w+@\w+\.\w{2,5}$/.test(@email)
      errors.email ?= []
      errors.email.push('Email is not in correct format')

    errors


  is_valid: ->
    if len(@validation_errors())
      return false
    super



class CsvFile extends RemoteModel
  @configure 'CsvFile',
    name: true

  @custom_entity_name: 'csv-files'

  constructor: ->
    super
    @__defineGetter__ 'lines_count', -> sum(part.lines_count for part in @parts)


CsvFile.bind 'init', (file) ->
  CsvFilePart.fetch(wrapper_id: file.id)



class CsvFilePart extends RemoteModel
  @configure 'CsvFilePart',
    wrapper_id: true
    file_path: true
    lines_count: true

  @ForeignKey CsvFile,
    id_field: 'wrapper_id'
    self_field: 'wrapper'
    remote_field: 'parts'

  @custom_entity_name: 'csv-file-parts'

  eval_on_matching_lines: (test_fn, eval_fn) ->
    unless @file_path
      return

    http.get(@file_path, {}, parse_json: false).then (err, status_code, file) ->
      [columns, lines...] = file.split('\n')
      columns = columns.split(',')

      for line in lines
        if test_fn(line)
          line_struct = {}
          line = line.split(',')
          for column, i in columns
            line_struct[column] = line[i]

          eval_fn(line_struct)



class SearchQuery extends Model
  @configure 'SearchQuery',
    csf_file_ids: -> []
    query: true
    columns: -> {}

  @ManyToMany CsvFile,
    ids_field: 'csf_file_ids'
    self_field: 'csv_files'
    remote_field: 'search_queries'


  constructor: ->
    super
    @__defineGetter__ 'ordered_columns', ->
      columns = []
      for name, index of @columns
        columns[index] = name

      columns


  do_search: ->
    rgxp = new RegExp(@query)

    for file in @csv_files
      for part in file.parts
        part.eval_on_matching_lines \

            (line_raw) ->
              console.log 'line_raw', line_raw
              rgxp.test(line_raw)
            ,

            (line_struct) =>
              console.log 'line_struct', line_struct
              result_line = new SearchQueryResult(query_id: @id)

              for key, value of line_struct
                unless (column_n = @columns[key])?
                  column_n = len(@columns)
                  @columns[key] = column_n

                result_line.data[column_n] = value

              result_line.save()




class SearchQueryResult extends Model
  @configure 'SearchQueryResult',
    query_id: true
    data: -> []

  @ForeignKey SearchQuery,
    id_field: 'query_id'
    self_field: 'query'
    remote_field: 'results'




module.exports = {
  FormEntry
  CsvFile, CsvFilePart,
  SearchQuery, SearchQueryResult
}
