{CssSelectorGenerator} = require('css-selector-generator/build/css-selector-generator')

navigator = navigator or {userAgent: '', appVersion: '', vendor: '', platform: ''}



$ = (el, query) ->
  unless query
    [el, query] = [query, el]

  if el
    el = $(el)
  else
    el = document

  if typeof query is 'string'
    el.querySelector(query)
  else
    query


$$ = (el, query) ->
  unless query
    [el, query] = [query, el]

  if el
    el = $$(el)
  else
    el = document

  if typeof query is 'string'
    document.querySelectorAll(query)
  else
    query


if window?
  window.$  = $
  window.$$ = $$


$.when = (timer, selector, cb) ->
  unless cb
    [timer, selector, cb] = [50, timer, selector]

  if el = $(selector)
    cb(el)

  thread timer, -> $.when(timer, selector, cb)


$.get_offset = (el) ->
  {top, left} = @get_window_offset(el)
  top  += window.scrollY
  left += window.scrollX

  {top, left}


$.get_window_offset = (el) ->
  {top, left} = el.getBoundingClientRect()
  {top, left}



# str = (value, offset='', tab='  ') ->
#   result = []

#   if value instanceof Array
#     result.push '['

#     comma = ','
#     for item, i in value
#       comma = '' if value.length is (i + 1)
#       result.push "\n#{offset}#{tab}#{str(item, offset + tab, tab)}#{comma}"

#     result.push "\n#{offset}]"

#   else
#     switch typeof value
#       when 'object'
#         result.push '{'

#         comma = ''
#         for key, item of value
#           result.push "\n#{offset}#{tab}#{key}: #{str(item, offset + tab, tab)}#{comma}"

#         result.push "\n#{offset}}"


#       when 'number'
#         result.push "#{value}"

#       when 'string'
#         result.push "(string) #{value}"

#       when 'function'
#         result.push "(function)"

#   result.join ''


_selector = null

get_css_selector = ($el) ->
  _selector ?= new CssSelectorGenerator(selectors: ['tag', 'class', 'nthchild'])
  _selector.getSelector($el)



get_blob = (content, mimetype) ->
  if Builder = window?.BlobBuilder or window?.WebKitBlobBuilder or window?.MozBlobBuilder or window?.MSBlobBuilder
    builder = new Builder
    builder.append(content)

    builder.getBlob(mimetype)

  else
    new Blob([content], {type: mimetype})



is_el_attached_to_DOM = (el) ->
  while el
    if el is document.body
      return true

    el = el.parentElement

  false


measure = (callback, name='') ->
  t0 = new Date
  res = callback()
  time_spent = (new Date) - t0

  console.log "(measure:#{name}) #{time_spent}"

  res


get_value = (data, path) ->
  for p in path.split('.') when p.length and data
    # je to imho zbytecne
    # # test, jestli je to index (číslo)
    # try
    #   if p is str(int(p))
    #     p = int(p)

    data = data[p]

  data

uuid = ->
  s4 = ->
    Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1)

  s4() + s4() + '-' + s4() + '-' + s4() + '-' +
    s4() + '-' + s4() + s4() + s4()


flatten = (array) ->
  unless type(array) is 'array'
    array = [array]

  i = 0
  while i < array.length
    while type(array[i]) is 'array'
      array[i...(i + 1)] = array[i]

    i++

  array


distinct = (array) ->
  res = []
  for a in array when a not in res
    res.push(a)

  res


extend = (obj, ext...) ->
  for e in ext
    for key, value of e
      if key[0] is '#'
        key = int(key[1...])

      if typeof value is 'object' and typeof obj[key] is 'object'
        if !value? or !obj[key]?
          obj[key] = value
        else
          obj[key] = extend(obj[key], value)
      else
        obj[key] = dup(value)

  obj


issubclass = (C, B) ->
  while (c = C?.__super__?.constructor) and c isnt B
    C = c

  c is B


`
function memoize(func, resolver) {
  if (typeof func != 'function') {
    throw 'need a function';
  }
  var memoized = function() {
    var cache = memoized.cache,
      key = resolver ? resolver.apply(this, arguments) : '_' + arguments[0];

    return Object.prototype.hasOwnProperty.call(cache, key)
      ? cache[key]
      : (cache[key] = func.apply(this, arguments));
  }
  memoized.cache = {};
  return memoized;
};
`

cache = (resolver, fn) -> memoize(fn, resolver)



`
throttle_fn = function (wait, func) {
  var context, args, timeout, result;
  var previous = 0;
  var later = function() {
    previous = new Date();
    timeout = null;
    result = func.apply(context, args);
  };
  return function() {
    var now = new Date();
    var remaining = wait - (now - previous);
    context = this;
    args = arguments;
    if (remaining <= 0) {
      clearTimeout(timeout);
      timeout = null;
      previous = now;
      result = func.apply(context, args);
    } else if (!timeout) {
      timeout = setTimeout(later, remaining);
    }
    return result;
  };
};
`


_throttle =
  _ttl: {}  # if_run fn's TTL
  _last: {} # if_run fn's items

  run: (period, key, if_run, if_skip) ->
    now = new Date

    # jestliže byla nějaká funkce již naplánována a ještě neproběhla,
    # tak ani neproběhne (ale na stejný čas spuštění se naplánuje
    # aktuální funkce - pod stejným klíčem)
    #
    if prev = @_last[key]
      delete @_last[key]
      prev.th()
      prev.if_skip?()

    # spustí se rovnou
    if !@_ttl[key]? or @_ttl[key] < now
      @_ttl[key] = time_delta(period, now)
      if_run()

      # nic ke zrušení
      ->

    else
      # naplánování funkce se spožděním o čas, který zbýval u předchozí
      # naplánované funkce
      #
      curr = @_last[key] =
        if_skip: if_skip
        th : thread (@_ttl[key] - now), =>
          delete @_last[key]
          @run(period, key, if_run, if_skip)

      # zrušení čekajícího if_run callbacku a spuštění if_skip callbacku
      ->
        curr.th()
        curr.if_skip?()


throttle = (args...) -> _throttle.run(args...)


_debounce =
  _last: {} # if_run fn's items

  run: (period, key, if_run, if_skip) ->
    if prev = @_last[key]
      prev.th()
      prev.if_skip?()

    curr = @_last[key] =
      if_skip: if_skip
      th : thread (period), =>
        delete @_last[key]
        if_run()

    # cancel waiting if_run callback and run if_skip callback
    ->
      curr.th()
      curr.if_skip?()

debounce = (args...) -> _debounce.run(args...)



to_querystring = (obj) ->
  params = []
  for key, value of obj
    if type(value) in ['string', 'number', 'boolean']
      param = "#{encodeURI(key)}"
      if value?
        param += "=#{encodeURI(value)}"
      params.push param

    else
      for key2, value2 of value
        param = "#{encodeURI(key)}[#{encodeURI(key2)}]"
        if value2?
          param += "=#{encodeURI(value2)}"
        params.push param

  params = params.join '&'
  params = "?#{params}" if params.length

  params


parse_querystring = (value) ->
  res = {}
  for kv in value.split '&'
    continue unless kv
    if kv[0] is '?'
      kv = kv[1...]

    [key, value] = kv.split '='
    res[key] = value

  res



toCamelCase = (text, first_is_lowercase=true) ->
  output = (w[0].toUpperCase() + w[1...] for w in text.split('-')).join('')
  if first_is_lowercase
    output = output[0].toLowerCase() + output[1...]
  output


fromCamelCase = (text, space_char='-') ->
  words  = ['']
  cursor = 0

  for s in text
    if (c = s.toLowerCase()) isnt s
      words.push('')

    words[-1...][0] += c

  words.join(space_char)



_get_indexes = (_max, _indexes...) ->
  indexes     = [_indexes]
  _last_index = _indexes[-1...][0]

  for i in [(_last_index or 0)..._max]
    if _last_index?
      i++

    if i < _max
      indexes = indexes.concat _get_indexes(_max, _indexes..., i)

  indexes



variates = (array) ->
  array = dup(array)
  array = array.sort (x, y) -> x > y

  res = []
  _x  = {}
  _indexes = _get_indexes(array.length)
  _indexes = _indexes.sort (x, y) ->
    if x.length isnt y.length
      return y.length - x.length

    i = 0
    if x[i] isnt y[i]
      return x[i] - y[i]

    while x[i] is y[i] and x[i]?
      i++

    return x[i] - y[i]

  for indexes in _indexes
    r = []
    for i in indexes
      r.push(array[i])

    res.push(r)

  return res



    # res = res.concat _remove_ones(_array)


    # for start in [0...missing_n]
    #   _array[i...i] = []




    # 0..(array.length - missing_n)]
    # for n in [0...missing_n]




strip = (text, char=' ') ->
  return unless text

  while text[0] is char
    text = text[1...]

  while text[-1...][0] is char
    text = text[...-1]

  text



dup = (obj, deep=true) ->
  unless obj?
    return obj

  if type(obj) is 'array'
    if deep
      (dup(item) for item in obj)
    else
      (item for item in obj)

  else if type(obj) is 'object'
    new_obj = {}
    if deep
      (new_obj[key] = dup(value) for key, value of obj)
    else
      (new_obj[key] = value for key, value of obj)
    new_obj

  else
    obj


add_attrs = (obj, ext...) ->
  if type(obj) is 'array'
    extend(item, ext...) for item in obj

  else
    extend(obj, ext...)

  obj


rem_attrs = (obj, attrs...) ->
  if type(obj) is 'array'
    rem_attrs(item, attrs...) for item in obj
    return

  for attr in attrs
    delete obj[attr]

  obj


pop = (obj, key) ->
  res = obj[key]
  if type(obj) is 'array'
    obj = [].concat obj[...key], obj[key...]

  else if res?
    delete(obj[key])

  res


pop_js_objects_out_of_the_string = (text) ->
  objs = {}

  pairs =
    '{'  : '}'
    '['  : ']'
    '('  : ')'
    '"'  : '"'
    '\'' : '\''

  last_key = null
  to_close = []

  new_text = ''

  for c, i in text
    if text[i - 1] is '\\'
      # pass

    else if c is to_close[-1...][0]
      to_close.pop()

    else if c of pairs

      to_close.push(pairs[c])

      unless last_key
        last_key = '$value' + len(objs)
        new_text += last_key

      objs[last_key] ?= ''

    if last_key
      objs[last_key] += c

      unless to_close.length
        last_key = null

    else
      new_text += c


  [new_text, objs]



_pairs = [
  ['amp',   '&']
  ['nbsp',  ' ']
  ['lt',    '<']
  ['gt',    '>']
  ['cent',  '¢']
  ['pound', '£']
  ['yen',   '¥']
  ['euro',  '€']
  ['copy',  '©']
  ['reg',   '®']
]
_strip = (html) ->
  # odstrani tagy
  html = html.replace?(/<[^>]+>/gm, '')
  for pair in _pairs
    # odstrani html entity
    html = html.replace (new RegExp "\&#{pair[0]}\;", 'g'), pair[1]
  html

strip_html = (html) ->
  return unless typeof(html) is 'string'
  # vytvoření nových řádků
  html = html.replace(/<br[^>]*>/g, '\n')
  html = html.replace(/<\/p>/g, '</p>\n')
  while true
    _html = _strip(html)
    if html is _html
      break
    else
      html = _html

  html


time_delta = (delta, datetime=(new Date)) ->
  datetime = datetime.valueOf() + delta
  new Date(datetime)


nw = (value) ->
  value.replace(/\ /g, '&nbsp;')


escape_regex = (value) ->
  str(value)
    .replace(/\\/ig, '\\\\')
    .replace(/\+/ig, '\\+')
    .replace(/\./ig, '\\.')
    .replace(/\-/ig, '\\-')
    .replace(/\[/ig, '\\[')
    .replace(/\]/ig, '\\]')
    .replace(/\(/ig, '\\(')
    .replace(/\)/ig, '\\)')


humanize =
  time: (t, floor=false) ->
    t = t * 60
    _f = Math.floor

    d = t
    h = d % 86400
    m = h % 3600
    s = m % 60

    d = _f(d / 86400)
    h = _f(h / 3600)
    m = _f(m / 60)

    jednotky = ['d', 'hod', 'min', 's']

    res = (for n, i in [d, h, m, s] when n > 0
        "#{n}&#8239;#{jednotky[i]}"
    )
    if floor then res[0]
    else          res.join ', '



format_with = (value, length) ->
  value = String(value) if typeof value isnt 'string'
  while value.length < length
    value = ' ' + value

  value


class Timer
  constructor: ->
    @timers = {}

  start: (name) ->
    @timers[name] ?=
      is_running: false
      time: 0
      runs: 0
      min: Infinity
      max: 0

    unless (t = @timers[name]).is_running
      t.is_running = true
      t.t = new Date()
      t.runs++

  stop: (name) ->
    if (t = @timers[name]).is_running
      t.is_running = false
      delta   = (new Date) - t.t
      t.time += delta

      t.max = Math.max t.max, delta
      t.min = Math.min t.min, delta

  get_stats: ->
    fields = [
      '-------  callback name  -------'
      '    runs'
      '   total   '
      '   min   '
      '   avg   '
      '   max   '
    ]

    console.log(fields...)
    console.log(fields.join('.').replace(/./g, '-'))

    for name, t of @timers
      total = t.time
      if t.is_running
        total += (new Date) - t.t

      if t.is_running
        name += ' (is running)'

      console.log format_with(name, fields[0].length),
        format_with(t.runs, fields[1].length),
        format_with(total + ' ms', fields[2].length),
        format_with(t.min + ' ms', fields[3].length),
        format_with(round(total / t.runs, 2) + ' ms', fields[4].length),
        format_with(t.max + ' ms', fields[5].length)

    return


window?.timer = new Timer



looping = (timeout, callback) ->
  interval = setInterval(callback, timeout)

  # angular way of binding
  -> clearInterval(interval)


thread = (timeout, callback) ->
  if typeof timeout is 'function'
    [timeout, callback] = [1, timeout]

  if not timeout
    callback()
    return ->

  timer = setTimeout(callback, timeout)

  # angular way of binding
  -> clearTimeout(timer)


nice_tel = (tel) ->
  return tel unless /^\+420\d{9}$/.test(tel)

  "#{tel[4...7]}&nbsp;#{tel[7...10]}&nbsp;#{tel[10...13]}"


`
q = {
  _in:    function(value, array){ return   value in k(array)    },
  gt:     function(a, b){         return   a > b                },
  gte:    function(a, b){         return   a >= b               },
  lt:     function(a, b){         return   a < b                },
  lte:    function(a, b){         return   a <= b               },
  eq:     function(a, b){         return   a == b               },

  array_equal: function(a, b){
    if (len(a) == len(b)){

      for (var i in a){
        if (!eq(a[i], b[i])) return false
      }
      return true
    }
    return false
  },

  dict_equal: function(a, b){
    if (eq(from_k(a), from_k(b))){
      for (var i in a){
        if (!eq(a[i], b[i])) return false
      }
      return true
    }
    return false
  }
}


eq = function (a, b){
  if (typeof a == 'object' && a instanceof Array){
    if (!(typeof b == 'object' && b instanceof Array)) return false

    return q.array_equal(a, b)
  }

  else if (typeof a == 'object'){
    if (!(typeof b == 'object')) return false

    if ("same_as" in a) return a.same_as(b)
    return q.dict_equal(a, b)
  }

  return a == b
}


g = function (obj, where, value){
    /*
    python:                     js:
        a = range(5)
        >>> [0, 1, 2, 3, 4]
        a[-2:]                      x(a, '-2:')
        >>> [3, 4]
        a[1:4] = [5]                x(a, '1:4', 5)
        >>> [0, 5, 4]
        a[1:] = [2, 3]              x(a, '1:', [2, 3])
        >>> [0, 2, 3]
        a[-6]                       x(a, '-6')
        >>> 0
        a = range(10)
        >>> [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
        a[::3]                      x(a, '::3')
        [0, 3, 6, 9]
    */
    var s_arguments = str(arguments)
    try {
        where = where.split(':')
        where = where.map(function(x){
            if (x == '') x = undefined
            else {
                x = int(x)
                while (x < 0) x += len(obj)
                if (x > len(obj)) x = x % len(obj)
            }

            return x
        })
        var result = []

        if (value === undefined){
            switch (len(where)){
                case 1:
                    return obj[where[0]]
                    break

                case 0:
                case 2:
                    if (where[0] === undefined) where[0] = 0
                    if (where[1] === undefined) where[1] = len(obj)
                    return obj.slice(where[0], where[1])
                    break

                case 3:
                    if (where[0] === undefined) where[0] = 0
                    if (where[1] === undefined) where[0] = len(obj)

                    range(where[0], where[1], where[2]).forEach(function(i){
                        result.push(obj[i])
                    })
                    return result
                    break
            }
        } else {
            switch (len(where)){
                case 0:
                    result = [].concat(value)
                    break

                case 1:
                    result = obj.slice()
                    result[where[0]] = value
                    break

                case 2:
                    if (where[0] !== undefined){
                        for (var i in range(where[0])) result.push(obj[i])
                    }

                    result = result.concat(value)

                    if (where[1] !== undefined){
                        for (var i in k(range(where[1], len(obj)))) result.push(obj[i])
                    }

            }

            while (len(obj)) obj.pop()
            for (var i in result) obj.push(result[i])

            return obj
        }
    } catch(e){
        console.log('XXX chyba v g()', s_arguments)

        return false
    }
}


s = function(obj, where, value){
    var s_arguments = str(arguments)

    obj = str(obj)
    var ch;
    obj = (function() {
      var _i, _len, _results;
      _results = [];
      for (_i = 0, _len = obj.length; _i < _len; _i++) {
        ch = obj[_i];
        _results.push(ch);
      }
      return _results;
    })();
    obj = g(obj, where, value)

    if (obj === false){
        console.log('XXX chyba v s()', s_arguments)
        return false
    }

    if (obj instanceof Array) return obj.join('')
    return obj
}



replaces = {
    truncate: function(value, n, symb){
        if (n === undefined) n = 45
        else if (typeof n == 'string'){
            if (n in k(["people", "organizers"])) n = 60
        }

        if (len(value) > n){
            if (symb === undefined) symb = '...'
            value = s(value, (n -len(symb)) +':', symb)
            // n musí být > len(symb), jinak to dělá blbosti...
        }

        return value
    },


    intcomma: function(value, decimal_places, separator){
        if (decimal_places === undefined) decimal_places = 2
        if (separator === undefined) separator = [' ', ',']

        decimal_places = exp(10, decimal_places)

        value = str(Math.round(value *decimal_places) /decimal_places)
        var point = value.indexOf('.')
        if (point != -1){
            var tmp_value = value.slice(point)
            value = value.slice(0, point)
        } else {
            var tmp_value = ''
        }

        var value_length = value.length
        range(value_length /3 -1).forEach(function(i){
            x = value_length -(i +1)*3
            value = value.slice(0, x) + ',' + value.slice(x)
        })
        value = (value + tmp_value).replace(/\,/g, separator[0]).replace(/\./g, separator[1])
        return value
    },


    slugify: function(value){
        value = value.toLowerCase()
            .replace(/á/g, 'a')
            .replace(/č/g, 'c')
            .replace(/ď/g, 'd')
            .replace(/é/g, 'e')
            .replace(/ě/g, 'e')
            .replace(/í/g, 'i')
            .replace(/ó/g, 'o')
            .replace(/ř/g, 'r')
            .replace(/š/g, 's')
            .replace(/ť/g, 't')
            .replace(/ú/g, 'u')
            .replace(/ů/g, 'u')
            .replace(/ý/g, 'y')
            .replace(/ž/g, 'z')
            .replace(/\ /g, '-')
            .replace(/[^\w\d-]/g, '')

        if (value[0] == '-') value = s(value, '1:')
        if (s(value, '-1') == '-') value = s(value, ':-1')

        return value
    }
}



remove = function(obj, what){
    /*
    python:                     js:
        a = range(5)
        >>> [0, 1, 2, 3, 4]
        a.remove(0)                 remove(a, 0)
        >>> [1, 2, 3, 4]
    */
    var tmp = []
    while (len(obj)){
        var x = obj.pop()
        if (x != what) tmp.push(x)
    }
    tmp.reverse()

    for (var i in tmp){
        obj[i] = tmp[i]
    }
    return obj
}



var i = 0
str = function(value, print, path){
    var tab = '    '
    var trunc = 50

    if (path === undefined) path = ''
    else path += tab

    if (value === undefined) return '(undefined)'
    else if (value === null) return '(null)'
    else if (value === NaN) return '(NaN)'
    else if (value === true) return '(true)'
    else if (value === false) return '(false)'

    else if (typeof value == 'function') return '(function)'
    else if (typeof value == 'number') return value.toString()
    else if (typeof value == 'string') return print ? '"' +value +'"' : value

    else if (typeof value == 'object'){
        // tj. jedna se o modely
        if ('toJSON' in value) return str(value.toJSON(), print, path.slice(0, -len(tab)))
        else if (value instanceof Array) return (len(value)) ? '[\n' +value.map(function(s){return tab +path +str(s, true, path)}).join(',\n') +'\n' +path +']' : '[]'
        else if ('id' in value && 'innerHTML' in value){
            i += 1
            console.log(i, value)
            return '(HTMLElement-' +i +')'
        }
        // else if (value instanceof Interval || value instanceof Bulk) return str(value.export_flat())
        else {
            if (!len(from_k(value))) return '{}'
            var result = []
            for (var key in value){
                if (!(key in k(['parent', 'parentNode']))) result.push(tab +path +replaces.truncate(key, trunc) +': ' +str(value[key], true, path))
            }
            return '{\n' +result.join(',\n') +'\n' +path +'}'
        }
    }
    else return value.toString()
}


int = function(value){
   return Number(value)
}


len = function(obj){
    if (typeof obj == 'number') return len(str(obj))
    else if (typeof obj == 'boolean') return 1
    else if (typeof obj == 'string') return obj.length
    else if (typeof obj == 'object' && obj != null){
        if ('length' in obj) return obj.length
        return len(from_k(obj))
    }

    return -1
}


sum = function(obj){
    var result = 0
    obj.forEach(function(o){result += o})

    return result
}


exp = function(value, ex){
    value = int(value)
    var result = 1
    for (var i = 0; i < ex; i++){
        result *= value
    }
    return result
}


range = function(par1, par2, step){
    if (typeof par1 != 'number') par1 = len(par1)
    var result = []
    if (par2 == null && par1 >= 0){
        par2 = par1
        par1 = 0
    }
    else if (par2 == null) par2 = 0

    if (step == null){
        step = 1
    }
    for (var i = par1; i < par2; i += step){
        result.push(i)
    }
    return result
}


shuffle = function(pole){
    var len_pole = pole.length
    var lr = pole.length.toString().length
    if (Math.random().toString().length -2 <= lr){
        lr = Math.random().toString().length -2
    }
    for (var i in range(Math.round(len_pole *1.3))){
        var a = parseInt(Math.random().toString().slice(2, lr +2)) % len_pole
        var b = parseInt(Math.random().toString().slice(2, lr +2)) % len_pole

        var a = [a, pole[a]]
        var b = [b, pole[b]]

        pole[a[0]] = b[1]
        pole[b[0]] = a[1]
    }
    return pole
}


cycle = function(count, items, join){
    var result = []

    items = extend([items])
    var x = items.length

    for (var i in range(count)){
        result.push(items[i % x])
    }

    if (join === undefined) return result
    return result.join(join)
}



k = function(list, value){
    var result = {}

    for (var i in list){
        if (value === undefined) result[list[i]] = 1
        else if (typeof value == 'object' && len(from_k(value))) result[list[i]] = list[i] in value ? value[list[i]] : null
        else result[list[i]] = value
    }

    return result
}


from_k = function(dict, full){
    var result = []

    for (var key in dict){
        if (full) result.push([key, dict[key]])
        else result.push(key)
    }

    return result
}


re = function(text, pattern){
    // syntaxe jako v pythonu (?P<nazev skupiny>)

    var tmpPattern = pattern.replace('\\(', '').replace('\\)', '')
    var groups = tmpPattern.match(/\([a-zA-Z<>?\\]+\)/g)
    var groupsDict = {}

    for (var i in groups){
        if (/\?P<\w+>(.*)/.test(groups[i])){
            var group = groups[i].replace(/\(\?P<(\w+)>(.*)\)/, '$1')
            groupsDict[group] = i
        }
    }

    pattern = pattern.replace(/\?P<\w+>/g, '')
    pattern = new RegExp(pattern)
    text = text.match(pattern).slice(1)

    for (group in groupsDict){
        groupsDict[group] = text[groupsDict[group]]
    }

    return groupsDict
}
`


_funcs =
    'gt' : (val, limit) -> val >  limit
    'gte': (val, limit) -> val >= limit
    'lt' : (val, limit) -> val <  limit
    'lte': (val, limit) -> val <= limit

    'in': (val, list) -> val in list
    'of': (val, list) -> val of list
    'has': (list, val) ->
      if type(list) is 'array'
        @['in'](val, list)
      else
        @['of'](val, list)

    'contains': (text, value) -> (new RegExp value, 'g').test text
    'icontains': (text, value) -> (new RegExp value, 'ig').test text
    'startswith': (text, value) -> (new RegExp "^#{value}", 'g').test text
    'istartswith': (text, value) -> (new RegExp "^#{value}", 'ig').test text

    'pass_value': (val) -> val # specialni pripad pro check(), kdy chci dosadit
                               # konkretni hodnotu do dane fn a testuju ji jen T/F


parse_underscores = (args='', context=root) ->
   args = args.split '__'
   val  = context

   return val unless len(args)

   while len(args) and val?
      arg = args.shift()

      if typeof root[arg] is 'function'
         val = root[arg](val)

      else if typeof val[arg] is 'function'
         val = val[arg]()

      else val = val[arg]

   val


check = (context, args='', value=true) ->
  args = args.split '__'
  val  = context

  if args[-1..][0] of _funcs
    finish = args.pop()

  return val unless len(args)

  while len(args) and val?
    arg = args.shift()

    if val[arg]?
      if typeof val[arg] is 'function'
        if !len(args) and finish is 'pass_value'
          return val[arg](value)

        else val = val[arg]()

      else
        val = val[arg]

    else if typeof q[arg] is 'function'
      val = q[arg](val)

    else if typeof root[arg] is 'function'
      val = root[arg](val)

    else val = undefined

    if val?.is_queryset is true
      query = {}
      args.push finish if finish
      query[ args.join('__') ] = value

      return len( val.filter(query) ) > 0

  if finish? then _funcs[finish](val, value)
  else            eq(val, value)


filter = (args={}, revert=false) ->
   return args if typeof args is 'function'
   (rec) ->
      q = !!revert
      for key, value of args
         return q unless check(rec, key, value)

      !q


eq = (objs...) ->
  unless objs.every((o) -> o?)
    return false
  objs[1..].every (o) -> eq.auto(objs[0], o)


eq.auto = (item1, item2) ->
  # test, jestli náhodou jeden objekt není null/false aj. a druhý nikoli
  if not (!!item1 and !!item2) and (!!item1 or !!item2)
    return false

  switch type(item1)
    when 'array'  then eq.array  item1, item2
    when 'object' then eq.object item1, item2
    else item1 is item2



eq.set = (set1, set2) ->
  unless set1.length is set2.length
    return false

  for item in set1
    if set2.indexOf(item) is -1
      return false

  true


eq.array = (array1, array2) ->
  unless array1.length is array2.length
    return false

  for item, i in array1
    unless eq.auto(array2[i], item)
      return false

  true


eq.object = (dict1, dict2) ->
  for key, value of dict1
    unless eq.auto(dict2[key], value)
      return false

  for key of dict2
    unless key of dict1
      return false

  true



sum = (obj=[], geom=false) ->
  if geom
    result = 1
    result *= o for o in obj

  else
    result = 0
    result += o for o in obj

  result

_d = (args) ->
  if args then new Date(args)
  else         new Date


round = (value, step=1) ->
  value /= step
  Math.round(value) * step

ceil = (value, step=1) ->
  res = round(value, step)
  if res < value
    res += step

  res

floor = (value, step=1) ->
  res = round(value, step)
  if value < res
    res -= step

  res


int = (value) -> value * 1

bool = (value) ->
  if      value is  'true' then return true
  else if value is 'false' then return false
  else !!int(value)


exp = (value, ex=2) ->
   value = int(value)


from_k = (object, full=false) ->
   result = []

   for key, value of object
      if full then result.push [key, value]
      else result.push key

   result


getInt = (value) ->
   value = value.replace(/\ /g, '')
   parseInt(value)


len = (obj) ->
  return obj.length if obj?.length?
  switch type(obj)
    when 'number'  then len("#{obj}")
    when 'boolean' then  1 # bylo to i v půbodním?
    when 'object'  then len(from_k(obj))

    else -1




intcomma = (value, dec_places=2) ->
   value = "#{value}"
   if '.' in value
      i = value.search(/\./)
      _tmp_value = value[i...]
      value = value[...i]

      _tmp_value = _tmp_value[..dec_places] if len(_tmp_value) > dec_places


   else _tmp_value = ''

   len_value = len(value)
   if len_value > 3
      value = (i for i in value)
      for i in [0...(len_value / 3)]
         i = len_value - (i + 1) * 3
         value[i...i] = ','
      value = value.join ''

   value = value.replace(/,/g, ' ')

   return value + _tmp_value



cycle = (items, count) ->
   result = []
   items = [items] unless items instanceof Array
   count ?= len items
   x = len items

   for i in [0...count]
      result.push items[i % x]

   result



remove = (obj, what='') ->
   (x for x in obj when x isnt what)


type = (obj, other_types=false) ->
  return 'null' if obj is null
  return 'NaN'  if obj is NaN

  unless (t = typeof obj) is 'object'
    return t

  if obj.length?
    return 'array'

  unless other_types
    return 'object'

  classTypes = {}
  for name in 'Boolean Number String Function Array Date RegExp'.split(' ')
    classTypes["[object #{name}]"] = name.toLowerCase()

  classTypes[Object::toString.call(obj)] or 'object'



`
rtrim = function(str, characters) {
  var nativeTrimRight = String.prototype.trimRight;
  if (str == null) return '';
  if (!characters && nativeTrimRight) return nativeTrimRight.call(str);
  characters = defaultToWhiteSpace(characters);
  return String(str).replace(new RegExp(characters + '+$'), '');
}

prune = function(str, length, pruneStr) {
  if (str == null) return '';

  str = String(str); length = ~~length;
  pruneStr = pruneStr != null ? String(pruneStr) : '...';

  if (str.length <= length) return str;

  var tmpl = function(c){ return c.toUpperCase() !== c.toLowerCase() ? 'A' : ' '; },
    template = str.slice(0, length+1).replace(/.(?=\W*\w*$)/g, tmpl); // 'Hello, world' -> 'HellAA AAAAA'

  if (template.slice(template.length-2).match(/\w\w/)) {
    template = template.replace(/\s*\S+$/, '');
  } else {
    template = rtrim(template.slice(0, template.length-1));
  }

  return (template+pruneStr).length > str.length ? str : str.slice(0, template.length)+pruneStr;
}

BrowserDetect = {
    init: function () {
        this.browser = this.searchString(this.dataBrowser) || "An unknown browser";
        this.version = this.searchVersion(navigator.userAgent)
            || this.searchVersion(navigator.appVersion)
            || "an unknown version";
        this.OS = this.searchString(this.dataOS) || "an unknown OS";
    },
    searchString: function (data) {
        for (var i=0;i<data.length;i++) {
            var dataString = data[i].string;
            var dataProp = data[i].prop;
            this.versionSearchString = data[i].versionSearch || data[i].identity;
            if (dataString) {
                if (dataString.indexOf(data[i].subString) != -1)
                    return data[i].identity;
            }
            else if (dataProp)
                return data[i].identity;
        }
    },
    searchVersion: function (dataString) {
        var index = dataString.indexOf(this.versionSearchString);
        if (index == -1) return;
        return parseFloat(dataString.substring(index+this.versionSearchString.length+1));
    },
    dataBrowser: [
        {
            string: navigator.userAgent,
            subString: "Chrome",
            identity: "Chrome"
        },
        {   string: navigator.userAgent,
            subString: "OmniWeb",
            versionSearch: "OmniWeb/",
            identity: "OmniWeb"
        },
        {
            string: navigator.vendor,
            subString: "Apple",
            identity: "Safari",
            versionSearch: "Version"
        },
        {
            prop: typeof window !== "undefined" && window !== null ? window.opera : void 0,
            identity: "Opera",
            versionSearch: "Version"
        },
        {
            string: navigator.vendor,
            subString: "iCab",
            identity: "iCab"
        },
        {
            string: navigator.vendor,
            subString: "KDE",
            identity: "Konqueror"
        },
        {
            string: navigator.userAgent,
            subString: "Firefox",
            identity: "Firefox"
        },
        {
            string: navigator.vendor,
            subString: "Camino",
            identity: "Camino"
        },
        {       // for newer Netscapes (6+)
            string: navigator.userAgent,
            subString: "Netscape",
            identity: "Netscape"
        },
        {
            string: navigator.userAgent,
            subString: "MSIE",
            identity: "Explorer",
            versionSearch: "MSIE"
        },
        {
            string: navigator.userAgent,
            subString: "Gecko",
            identity: "Mozilla",
            versionSearch: "rv"
        },
        {       // for older Netscapes (4-)
            string: navigator.userAgent,
            subString: "Mozilla",
            identity: "Netscape",
            versionSearch: "Mozilla"
        }
    ],
    dataOS : [
        {
            string: navigator.platform,
            subString: "Win",
            identity: "Windows"
        },
        {
            string: navigator.platform,
            subString: "Mac",
            identity: "Mac"
        },
        {
               string: navigator.userAgent,
               subString: "iPhone",
               identity: "iPhone/iPod"
        },
        {
            string: navigator.platform,
            subString: "Linux",
            identity: "Linux"
        }
    ]

};
BrowserDetect.init();

`

get_browser = ->
  if not BrowserDetect.browser
    BrowserDetect.init()

  {browser, version, OS} = BrowserDetect
  {browser, version, OS}


spy = (cb) ->
  res = (args...) ->
    res.counter++
    cb(args...)
  res.counter = 0
  res


root = {
  spy
  measure
  get_css_selector
  get_blob
  is_el_attached_to_DOM
  get_value
  uuid
  flatten
  distinct
  extend
  issubclass
  cache
  throttle_fn
  throttle
  debounce
  to_querystring
  parse_querystring
  toCamelCase
  fromCamelCase
  variates
  strip
  dup
  add_attrs
  pop
  pop_js_objects_out_of_the_string
  strip_html
  time_delta
  nw
  escape_regex
  humanize
  looping
  thread
  nice_tel
  q
  eq  # 2x
  g
  s
  replaces # filters
  remove  # 2x
  str
  int  # 2x
  len  # 2x
  sum  # 2x
  exp  # 2x
  range
  shuffle
  cycle  # 2x
  k
  from_k  # 2x
  re
  parse_underscores
  check
  filter
  round
  ceil
  floor
  getInt
  intcomma   # 2x ()
  type
  rtrim
  prune
  get_browser
}
module.exports = root