{thread, debounce, eq, type, dup, len, fill_default, flatten, uuid} = require './extras'
{Binder, storage} = require './contrib'
{Promise, queue} = require './promise'
{http} = require './http'



ModelArray =
  new: -> @wrap []
  wrap: (array) ->
    array._push ?= array.push
    array._pop  ?= array.pop

    array.push    = -> throw new Error 'push is not allowed in ModelArray'
    array.pop     = -> throw new Error 'pop is not allowed in ModelArray'
    array.unshift = -> throw new Error 'unshift is not allowed in ModelArray'
    array.shift   = -> throw new Error 'shift is not allowed in ModelArray'

    array.toJSON  = -> @dup()
    array.dup = ->
      (r for r in array)

    array.check = (item) ->
      item?

    array.add = (item) ->
      unless @check(item)
        throw new Error "(ModelArray) type of value \"#{item}\" is wrong"

      unless item in @
        @_push item
      @length

    array.remove = (item) ->
      index = @indexOf item
      if index isnt -1
        # shuffle items
        for i in [index...@length]
          @[i] = @[i + 1]

        # remove last item
        @_pop()
      @length

    array.reverse = =>
      @wrap dup(array, false).reverse()

    array.sort = (get_key) =>
      if get_key
        compare = (a, b) ->
          if get_key(a) < get_key(b)      then -1
          else if get_key(a) > get_key(b) then  1
          else                                  0

      @wrap dup(array, false).sort(compare)

    array.filter = (args...) =>
      @wrap [].filter.apply(array, args)

    return array



warehouse = {}
@window?.warehouse = warehouse


class BaseModel extends Binder
  @configure: (@className, @fields) ->
    @id_map = {}
    @lazy_map = {}

    # register to global warehouse
    warehouse[@className] = @

    @objects = ModelArray.new()

    # add type checking
    @objects.check = (r) => r instanceof @

    # bind to add item to map
    @objects._add = @objects.add
    @objects.add  = -> throw new Error 'add is not allowed in Model.ModelArray'


    # bind to remove item from map
    @objects._remove = @objects.remove
    @objects.remove  = -> throw new Error 'add is not allowed in Model.ModelArray'


  @init: ->


  @toJSON: ->
    obj.toJSON() for obj in @objects


  @fromJSON: (objects) ->
    unless objects
      throw new Error('objects cannot be empty')

    new Promise (reject, resolve) =>
      if typeof objects is 'string'
        objects = JSON.parse(objects)

      if type(objects) is 'array'
        res = []

        while objects.length
          do (objs=objects[...10]) => queue "#{@className}:fromJSON", (done) =>
            thread =>
              x = (@create(value, trigger: 'init') for value in objs)
              res = res.concat(x)
              done()

          objects[...10] = []

        queue "#{@className}:fromJSON", =>
          resolve(res)

      else
        resolve(@create(objects, trigger: 'init'))


  @create: (atts, options={}) ->
    (new @(atts)).save(options)


  @get_or_create: (atts) ->
    items = @objects.filter (item) ->
      for key, value of atts when value isnt undefined
        if item[key] isnt value
          return false
      true

    if not item = items[0]
      item = @create(atts)

    item


  @get: (id, lazy=false) ->
    unless id?
      throw new Error('cannot get model without a given id')

    if res = @id_map[id]
      res
    else if lazy
      @lazy_map[id] ?= new @({id, $lazy: true})
      @lazy_map[id]

    else
      throw new Error "(#{@className}) no item with id #{JSON.stringify(id)} exists"


  @get_new_id: ->
    uuid()

  @_cb_sid: (event_name) ->
    [ "#{@className}:#{event_name}"
      "(model):#{event_name}"
    ]

  _cb_sid: (event_name) ->
    [ "#{@constructor.className}:#{event_name}:#{@id}"
      "#{@constructor.className}:#{event_name}"
      "(model):#{event_name}"
    ]

  trigger: (callbackName, options) ->
    if options?
      # re-map callbackName
      if options
        callbackName = options

      # don't trigger
      else
        return

    super callbackName, @
    super 'change', @, callbackName

  constructor: (atts={}) ->
    @load(atts)

  toJSON: ->
    @attributes()

  attributes: ->
    res = {id: @id}
    for key of @constructor.fields
      value = @[key]

      # automatický převod data na timestamp
      # TODO: aplikovat i na zanořený čas,
      #       stejně jako to umí .load()
      #
      if value instanceof Date
        res[key] = @to_ts(value)

      else
        res[key] = value?.toJSON?() or dup(value)

    res

  change_id: (new_id) ->
    if @constructor.lazy_map[@id]?
      @constructor.lazy_map[new_id] = @
      delete @constructor.lazy_map[@id]

    else if @constructor.id_map[@id]?
      @constructor.id_map[new_id] = @
      # let record to be found under it's old ids
      # delete @constructor.id_map[@id]


  to_ts:   (value) -> '~' + value.valueOf() / 1e3
  from_ts: (value) -> new Date(Number(value[1...]) * 1e3)


  load: (atts={}) ->
    if @id and atts.id and atts.id isnt @id
      @change_id(atts.id)


    fill = (orig, changes) =>
      # fill in given values
      for key, value of changes
        if value? and typeof value is 'object'
          unless orig[key]?.constructor is value?.constructor
            orig[key] = new value.constructor

          fill(orig[key], value)

        else
          # automatický převod timestampu na datum
          if /^~\d+(\.\d+)?$/.test(value)
            value = @from_ts(value)

          orig[key] = value

    fill(@, atts)

    # fill in default values
    for key, value of @constructor.fields
      if not @[key]? and typeof value is 'function'
        @[key] = value()


  restore: ->
    @load(@$backup)


  is_valid: (throw_error=false) ->
    for key, value of @constructor.fields
      if not @[key]? and value is true
        if throw_error
          throw new Error "(#{@constructor.className}) validation error: missing \"#{key}\" attribute"
        return false

    true

  is_modified: ->
    not eq(@attributes(), @$backup)

  save: (options={}) ->
    # fill in default values if any
    @load()

    # test validity
    try
      if @is_valid(true)
        unless @is_modified()
          if options.force_trigger
            @trigger 'unchanged', options.trigger
          return @

        item = @
        item.id ?= @constructor.get_new_id()
        if item.id of @constructor.id_map
          if item isnt old_item = @constructor.id_map[item.id]
            old_item.load(item.attributes())
            old_item.save(options)

          else
            @trigger 'update', options.trigger

        else
          if item.id of @constructor.lazy_map
            lazy = @constructor.lazy_map[item.id]

            delete lazy.$lazy
            lazy.load(item.attributes())
            item = lazy

            delete @constructor.lazy_map[item.id]

          @constructor.id_map[item.id] = item
          @constructor.objects._add(item)
          item.trigger 'create', options.trigger

        # save previous version
        item.$old = item.$backup

        # do backup for possible restoring later
        item.$backup = item.attributes()
        return item

    catch e
      @restore()
      throw e


  delete: (options={}) ->
    # save previous version
    @$old = @$backup

    @constructor.objects._remove(@)
    delete @constructor.id_map[@id]
    @$deleted = true
    @trigger 'delete', options.trigger


class Model extends BaseModel
  @configure: ->
    @fk_self   = {}
    @fk_remote = {}

    @m2m_self   = {}
    @m2m_remote = {}

    super


  @ForeignKey: (Remote, atts) ->
    # TODO: remote musi byt model
    # TODO: jestliže @ je RemoteModel, tak i Remote musí být RemoteModel (opačně to je jedno)
    # unless Remote
    # throw new Error

    {remote_field, self_field, id_field} = atts
    # TODO: automaticky generovat
    # remote_field = Remote.className

    throw new Error "(#{@className})(ForeignKey) needs a Remote defined" unless Remote
    throw new Error "(#{@className})(ForeignKey) needs a remote_field defined" unless remote_field
    throw new Error "(#{@className})(ForeignKey) needs a self_field defined" unless self_field
    throw new Error "(#{@className})(ForeignKey) needs a id_field defined" unless id_field

    unless id_field of @fields
      throw new Error "(#{@className})(ForeignKey) id_field is not contained in fields"

    @fk_self[self_field]           = {Remote, id_field, remote_field}
    Remote.fk_remote[remote_field] = {Remote: @, id_field}

    for item in @objects
      item.setup_relations()

    for item in Remote.objects
      item.setup_relations()


  @ManyToMany: (Remote, atts) ->
    # TODO: remote musi byt model
    # TODO: jestliže @ je ManyToMany, tak i Remote musí být ManyToMany (opačně to je jedno)
    # unless Remote
    # throw new Error

    {remote_field, self_field, ids_field} = atts
    # TODO: automaticky generovat
    # remote_field = Remote.className

    throw new Error "(#{@className})(ManyToMany) needs a Remote defined" unless Remote
    throw new Error "(#{@className})(ManyToMany) needs a remote_field defined" unless remote_field
    throw new Error "(#{@className})(ManyToMany) needs a self_field defined" unless self_field
    throw new Error "(#{@className})(ManyToMany) needs a ids_field defined" unless ids_field

    unless ids_field of @fields
      throw new Error "(#{@className})(ManyToMany) ids_field is not contained in fields"

    @m2m_self[self_field]           = {Remote, ids_field, remote_field}
    Remote.m2m_remote[remote_field] = {Remote: @, ids_field}

    for item in @objects
      item.setup_relations()

    for item in Remote.objects
      item.setup_relations()


  change_id: (new_id) ->
    super
    # přepis všech záznamů xxx_id
    for key, value of @constructor.fk_remote
      for rec in @[key]
        rec[value.id_field] = new_id
        rec.save()

    # přepis všech záznamů xxx_ids
    for key, value of @constructor.m2m_remote
      for rec in @[key]
        rec[value.ids_field].remove(@id)
        rec[value.ids_field].add(new_id)
        rec.save()


  load: (atts) ->
    super
    @setup_relations()


  setup_relations: ->
    unless @$lazy
      # setup ManyToMany - self side
      for key, value of @constructor.m2m_self then do (key, value) =>
        # allow add/remove methods
        @[value.ids_field] = ModelArray.wrap(@[value.ids_field])

        array  = (value.Remote.get(id, true) for id in @[value.ids_field])
        array  = ModelArray.wrap(array)
        @[key] = array

        array.check = (r) ->
          r instanceof value.Remote

        array._add = array.add
        array.add  = (item) =>
          @[value.ids_field].add(item.id)
          @save()

        array._remove = array.remove
        array.remove  = (item) =>
          @[value.ids_field].remove(item.id)
          @save()


    # setup ManyToMany - remote side
    for key, value of @constructor.m2m_remote then do (key, value) =>
      array  = value.Remote.objects.filter (r) => @id? and @id in r[value.ids_field]
      array  = ModelArray.wrap(array)
      @[key] = array

      array.check = (r) ->
        r instanceof value.Remote

      array._add = array.add
      array.add  = (item) =>
        unless array.check(item)
          throw new Error "(ModelArray) type of value \"#{item}\" is wrong"

        item[value.ids_field].add(@id)
        item.save()

      array._remove = array.remove
      array.remove  = (item) =>
        item[value.ids_field].remove(@id)
        item.save()


    # setup ForeignKey - self side
    unless @$lazy
      for key, value of @constructor.fk_self
        # nemusí být vždy definováno
        if @[value.id_field]?
          remote = value.Remote.get(@[value.id_field], true)
          @[key] = remote

    # setup ForeignKey - remote side
    for key, value of @constructor.fk_remote then do (key, value) =>
      array  = value.Remote.objects.filter (r) => @id? and r[value.id_field] is @id
      array  = ModelArray.wrap(array)
      @[key] = array

      array.check = (r) ->
        r instanceof value.Remote

      array._add = array.add
      array.add  = (item) =>
        unless array.check(item)
          throw new Error "(ModelArray) type of value \"#{item}\" is wrong"

        item[value.id_field] = @id
        item.save()

      array._remove = array.remove
      array.remove  = (item) =>
        # don't delete when it's already changed...
        if item[value.id_field] is @id
          delete item[value.id_field]

        item.save()


  save: ->
    @trigger_pause()

    prev = @$backup or (new @constructor)
    if item = super
      item.update_relations(prev)
    @trigger_unpause()
    item


  update_relations: (prev) ->
    # test, jestli se změnily nějaké fk/m2m
    # -------------------------------------
    # setup ForeignKey - self side
    for key, value of @constructor.fk_self
      if prev[value.id_field] isnt @[value.id_field]
        # nemusi byt definovano
        if prev[value.id_field]?
          prev_remote = value.Remote.get(prev[value.id_field], true)
          prev_remote[value.remote_field]._remove(@)
          # kdyby potom nebylo definováno
          delete @[key]

        if @[value.id_field]?
          remote = value.Remote.get(@[value.id_field], true)
          remote[value.remote_field]._add(@)
          # define
          @[key] = remote

    # setup ManyToMany - self side
    for key, value of @constructor.m2m_self
      unless eq.set(prev[value.ids_field], @[value.ids_field])
        to_remove = prev[value.ids_field].filter (id) => id not in @[value.ids_field]
        to_add    = @[value.ids_field].filter (id)    => id not in prev[value.ids_field]

        for id in to_remove
          remote = value.Remote.get(id, true)
          @[key]._remove(remote)
          remote[value.remote_field]._remove(@)

        for id in to_add
          remote = value.Remote.get(id, true)
          @[key]._add(remote)
          remote[value.remote_field]._add(@)

    @


  delete: (options) ->
    if prev = @$backup
      # removing fk relations array
      for key, value of @constructor.fk_self
        delete @[value.id_field]
        @update_relations(prev)

      for key, value of @constructor.fk_remote
        # hard reference (cascade delete)
        if value.Remote.fields[value.id_field] is true
          for remote in @[key].dup()
            remote.delete(options)

        # soft reference - just unlink
        else
          for remote in @[key].dup()
            delete remote[value.id_field]
            remote.save()

      # removing m2m relations array
      for key, value of @constructor.m2m_self
        @[value.ids_field] = ModelArray.new()
        @update_relations(prev)

      for key, value of @constructor.m2m_remote
        for remote in @[key].dup()
          remote[value.ids_field].remove(@id)
          res = remote.save()
          remote.update_relations(remote.$backup)

    super



class RemoteModel extends Model
  @pending: 1  # pro init, aby se rovnou mohly věšet eventy na .when

  @configure: ->
    super

    @bind '$ajax_done', => @pending = 0


  @init: ->
    super

    @bind 'create', (args...) => @create_remote(args...)
    @bind 'update', (args...) => @update_remote(args...)
    @bind 'delete', (args...) => @delete_remote(args...)

    if records = init_data?[@className.toLowerCase()]
      @fromJSON(records).then =>
        if not --@pending
          @trigger '$init_done'

    else if records isnt null
      --@pending  # fetch si přidává sám pending
      @fetch()

    else
      if not --@pending
        @trigger '$init_done'


  @get_url: (rec) ->
    url = "#{API_BASE}#{@custom_entity_name ? @className.toLowerCase()}/"
    if rec
      url = "#{url}#{rec.id}/"
    url


  @when: (callback) ->
    if @pending
      ub1 = @one '$ajax_done', =>
        ub2()
        callback()

      ub2 = @one '$init_done', =>
        ub1()
        callback()

    else
      callback()


  @fetch: (params={}) ->
    ++@pending
    http.get(@get_url(), params).then (err, status, remote) =>
      if err
        return

      @fromJSON(remote['results']).then =>
        if not --@pending
          @trigger '$ajax_done'


  @create_remote: (rec) ->
    # TODO
    params = rec.attributes()
    delete params.id

    ++@pending
    # queue.promise 'remote:model', =>
    queue 'remote:model', =>
      # false-positive - bylo vytvořeno už dříve
      if rec.has_remote_id()
        throw new Error("(#{@className}.create_remote) validation error: already has remote id")

      params = rec.attributes()
      delete params.id

      http.post(@get_url(), params).then (err, status, remote) =>
        if err
          rec.delete()

        else
          rec.load(remote)
          rec.save(trigger: 'soft-update')

        if not --@pending
          @trigger '$ajax_done'

        # if err
        #   throw err



  @update_remote: (rec) ->
    debounce 100, "remote:model:#{@className}:#{rec.id}", =>
      if rec.$deleted
        return

      ++@pending
      queue 'remote:model', =>
        params = rec.attributes()
        http.put(rec.get_url(), params).then (err, status, remote) =>
          if err
            remote = rec.$old

            rec.load(remote)
            rec.save()

          if not --@pending
            @trigger '$ajax_done'

          # if err
          #   throw err


  @delete_remote: (rec) ->
    unless rec.has_remote_id()
      return

    ++@pending
    queue 'remote:model', =>
      http.delete(rec.get_url()).then (err, remote) =>
        if err
          @create(rec.$old)

        if not --@pending
          @trigger '$ajax_done'

        # if err
        #   throw err


  get_url: ->
    @constructor.get_url(@)


  has_remote_id: ->
    typeof @id is 'number'



class PersistentModel extends Model
  @pending: true
  @init: ->
    super

    @bind 'change', (args...) => @save_state()

    storage.get("persistent_#{@className}").then (error, prev) =>
      if error
        return

      @fromJSON(prev or []).then =>
        @pending = false
        @trigger '$init_done'


  @when: (callback) ->
    if @pending
      @one '$init_done', -> callback()
    else
      callback()


  @save_state: ->
    storage.set "persistent_#{@className}", @toJSON()



module.exports = {Model, ModelArray, warehouse, RemoteModel, PersistentModel}