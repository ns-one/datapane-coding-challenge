{get_blob, add_attrs, type, uuid, thread, debounce} = require './extras'
{Promise, queue} = require './promise'


GLOBALS =
  data_prefix: ''
  max_img_side_size: 2048


localStorage = window?.localStorage or {}


if rFS = window?.RequestFileSystem #or window?.webkitRequestFileSystem
  fs = new Promise (reject, resolve) ->
    rFS (LocalFileSystem?.PERSISTENT or window?.PERSISTENT or 1), 0,
      (fs) -> resolve(fs)
      -> reject(new Error('fs error'))

  # cache for fileObj
  #
  files = {}
  wrap_files = (filename) ->
    files[filename] ?= new Promise (reject, resolve) ->
      fs.then (error, fs) ->
        if error
          reject(error)

        fs.root.getFile "#{filename}.json",
          create: true
          exclusive: false

          (fileObj) -> resolve(fileObj)
          -> reject(new Error('get fileObj error'))

    files[filename]


  storage =
    get: (key) ->
      new Promise (reject, resolve) ->
        queue 'fs', (done) ->
          wrap_files(key).then (error, fileObj) ->
            if error
              reject(error)

            fileObj.file (file) ->
              reader = new FileReader()
              reader.onload = (event) ->
                try
                  to_parse = event.target.result or 'null'
                  resolve(JSON.parse(to_parse))
                  done()
                catch e
                  console.log key, 'e', e, to_parse
                  done()
                  reject(e)

              reader.onerror = ->
                reject(new Error('file reading error'))
                done()

              reader.readAsText(file)


    __set: (key, value) ->
      new Promise (reject, resolve) ->
        queue 'fs', (done) ->
          wrap_files(key).then (error, fileObj) ->
            if error
              reject(error)

            fileObj.createWriter(
              (writer) ->
                writer.onwrite = (event) ->
                  done()
                  resolve()

                writer.onerror = ->
                  done()
                  reject(new Error('file writer.write error'))

                blob = get_blob(JSON.stringify(value), 'text/plain')
                writer.write(blob)

              ->
                done()
                reject(new Error('file writer error'))
            )


else
  storage =
    get: (key) ->
      new Promise (reject, resolve) ->
        queue 'fs', (done) ->
          thread ->
            resolve(JSON.parse(localStorage[key] or 'null'))
            done()

    __set: (key, value) ->
      new Promise (reject, resolve) ->
        queue 'fs', (done) ->
          thread ->
            localStorage[key] = JSON.stringify(value)
            done()
            resolve()


storage.keys = {}
storage.get('keys').then (error, keys) ->
  add_attrs(storage.keys, keys or {})


storage._set = (key, value) ->
  debounce 1e3, "fs:#{key}", ->
    storage.__set(key, value)


storage.set = (key, value) ->
  storage.keys[key] = true
  storage._set('keys', storage.keys)
  storage._set(key, value)


storage.purge = ->
  for key of storage.keys
    storage.__set(key, null)

  storage.__set('keys', {})


window?.storage = storage


class Session
  constructor: ->
    @_vals = {}

  get: (key, d=null) ->
    if @_vals[key]?
      @_vals[key]

    else
      d

  set: (key, value) ->
    @_vals[key] = value



class Persistent extends Session
  is_ready: false

  constructor: ->
    super

    promise = storage.get('persistent_session').then (error, persistent_session) =>
      if error
        throw error

      @_vals = persistent_session or {}
      @is_ready = true

    @when = (cb) ->
      promise.then -> thread -> cb()


  get: ->
    unless @is_ready
      throw new Error('Persistent storage is not ready yet')

    super


  set: ->
    unless @is_ready
      throw new Error('Persistent storage is not ready yet')

    super
    storage.set('persistent_session', @_vals)


session    = new Session
persistent = new Persistent

window?.session    = session
window?.persistent = persistent


__moduleKeywords = ['included', 'extended']
class Module
  @include: (obj) ->
    throw('include(obj) requires obj') unless obj
    for key, value of obj when key not in __moduleKeywords
      @::[key] = value
    obj.included?.apply(@)
    this

  @extend: (obj) ->
    throw('extend(obj) requires obj') unless obj
    for key, value of obj when key not in __moduleKeywords
      @[key] = value
    obj.extended?.apply(@)
    this

  @proxy: (func) ->
    => func.apply(@, arguments)

  proxy: (func) ->
    => func.apply(@, arguments)

  @throw: (msg) -> throw "(#{@className}) #{msg}"
  throw:  (msg) -> throw "(#{@constructor.className}) #{msg}"





sid_callbacks = {}

Bindings =
  _cb_fire: (cid, args...) ->
    if cbs = sid_callbacks[cid]  # then thread ->
      for uid, cb of cbs
        cb(args...)

    args


  _cb_bind: (cid, callback) ->
    sid = uuid()

    sid_callbacks[cid] ?= {}
    sid_callbacks[cid][sid] = callback

    # angular compatibility
    -> delete sid_callbacks[cid][sid]

  _cb_sids: (callbackName) ->
    sids = @_cb_sid(callbackName)
    if type(sids) isnt 'array'
      sids = [sids]
    sids

  _cb_sid: (callbackName) ->
    throw 'generator for sids of callbacks is not defined (_cb_sid)'

  trigger: (callbackName, args...) ->
    if @_trigger_paused
      @_triggers_stack.push {callbackName, args}

    else
      @_fire(callbackName, args)

  _fire: (callbackName, args) ->
    for cb_sid in @_cb_sids(callbackName)
      @_cb_fire cb_sid, args...

  trigger_pause: ->
    @_trigger_paused = true
    @_triggers_stack ?= []

  trigger_unpause: ->
    @_trigger_paused = false
    while @_triggers_stack?.length
      {callbackName, args} = @_triggers_stack.shift()
      @_fire(callbackName, args)


  bind: (callbackNames, callback) ->
    throw 'bind for undefined callback' unless callback

    # store pro lokální cbs - pro případ, že se zavolá jen .unbind()
    # definujeme až v každé instanci zvlášť - není to tak časté volání
    #
    @_callbacks ?= {}

    cbs = []
    for callbackName in callbackNames.split(' ')
      cb_sid = @_cb_sids(callbackName)[0]
      cb = @_cb_bind cb_sid, (args...) -> callback(args...)
      @_callbacks["#{callbackName}:#{callback}"] = cb
      cbs.push(cb)

    # zrusi vsechny bindy na call (zpetne kompatibilni)
    -> cb() for cb in cbs

  unbind: (callbackNames, callback) ->
    unless @_callbacks
      return

    if callbackNames
      for callbackName in callbackNames.split(' ')
        # run the cb from above (deregister listener)
        @_callbacks["#{callbackName}:#{callback}"]?()

    else
      for key, cb of @_callbacks
        cb()

  one: (callbackNames, callback) ->
    throw 'bind for undefined callback' unless callback
    cb = @bind callbackNames, (args...) ->
      cb()
      callback(args...)





class Binder extends Module
  @include(Bindings)
  @extend(Bindings)


module.exports = {Binder, GLOBALS, session, persistent, storage}