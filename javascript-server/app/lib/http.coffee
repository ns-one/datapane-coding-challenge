{add_attrs, thread, to_querystring} = require './extras'
{Promise} = require './promise'
{GLOBALS, Binder} = require './contrib'


localStorage = @localStorage or {}



http_fallback =
  _store: JSON.parse(localStorage.http_fallback or '{}')
  get: (key) -> @_store[key]

  set: (key, value) ->
    @_store[key]
    localStorage.http_fallback = JSON.stringify(@_store)


http = add_attrs (new Binder),
  cached_methods: {'GET'}
  tmp_error_requests_tries: 5


  _cb_sid: (callbackName) ->
    "HTTP:#{callbackName}"


  _cache: {}
  main: (url, data={}, options={}) ->
    unless options.method of @cached_methods
      return @_main(url, data, options)

    if not @_cache[url]? or options.use_cache is false
      @_cache[url] = new Promise (reject, resolve) =>
        @_handle_tmp_error_requests(
          [url, data, options] # argumenty pro @main()
          {reject, resolve}    # "defer" objekt
          http.tmp_error_requests_tries
        )

    @_cache[url]


  # postara se o docasnou chybu spojeni - request se snazi opakovat
  _handle_tmp_error_requests: (args, d, i) ->
    @_main(args...).then (error, data...) =>
      if error
        if --i # zbyva jeste pokus?
          console.log 'http error - opakujeme', {i, error, data}
          @_handle_tmp_error_requests(args, d, i)

        else
          d.reject(error)

      else
        d.resolve(data...)


  pending: 0
  _main: (url, data={}, options={}) ->
    promise = new Promise (reject, resolve) =>
      options.prefix ?= GLOBALS.data_prefix
      options.url     = options.prefix + url
      options.data    = data

      xhr = new XMLHttpRequest()
      xhr.open(options.method, options.url)
      xhr.setRequestHeader 'X-Requested-With', 'Bn.'

      if options.method of {'GET', 'DELETE'}
        data = null

      else
        xhr.setRequestHeader('Content-Type', 'application/json')
        data = JSON.stringify(data)

      self = @
      xhr.onreadystatechange = ->
        if @readyState is 4
          _url = options.url.replace(/\?.*/, '')
          if @status and self._enable_http_fallback
            http_fallback.set "#{options.method}-#{_url}", {@responseText, @status}

          if 200 <= @status < 300
            response = {@responseText, @status}

          # status is 0 → jsi offline
          else if @status is 0 and http._enable_http_fallback
            response = http_fallback.get("#{options.method}-#{_url}")

          else
            return reject new Error(JSON.stringify({@status, @responseText}))

          self._handle_request({reject, resolve}, response, options)

      # tj. startuje prvni request
      unless @pending
        @trigger('requestStart')

      # offline mod
      if @_work_offline and @_enable_http_fallback
        _url     = options.url.replace(/\?.*/, '')
        response = http_fallback.get("#{options.method}-#{_url}")

        unless response?
          return reject new Error("not cached request #{str {_url, url: options.url}}")

        @_handle_request({reject, resolve}, response, options)


      # requesty, ktere se maji radit do fronty (nikdy nepobezi paralelne)
      else if options.use_queue
        if @queue_pending
          binder = @bind 'requestDone', =>
            unless @queue_pending
              # kontrolujeme kazdy request, jestlize uz neni pending,
              # muzeme spoustet dalsi
              #
              # kdyz se jich nahodou spusti vice najednou, projde jen jeden
              # a ty dalsi se zase znova nabinduji
              @queue_pending = true
              binder()
              xhr.send(data)

        else
          # na nic neceka, takze request se muze rovnou spustit,
          # ale musi blokovat ty dalsi requesty
          @queue_pending = true

          @pending += 1
          xhr.send(data)

      else
        @pending += 1
        xhr.send(data)

      # samostatny xhr request, aby šel připadně abortovat
      thread =>
        promise.xhr = xhr

        promise.then =>
          @pending -= 1
          if options.use_queue
            @queue_pending = false

          @trigger('requestDone')
          # tj. ukoncil posledni bezici request
          unless @pending
            @trigger('ajaxDone')



  _handle_request: (d, response, options={}) ->
    {status, responseText} = response
    if options.parse_json
      try
        responseText = JSON.parse(responseText)
      catch e
        unless status in [201, 204]
          d.reject new Error('bad content')

    d.resolve(status, responseText)


  get: (url, data={}, options={}) ->
    options.method      = 'GET'
    options.parse_json ?= true
    options.use_cache  ?= true
    if data.id?
      url += "#{data.id}/"
      delete(data.id)

    params = to_querystring(data)
    url   += params

    @main url, data, options


  post: (url, data={}, options={}) ->
    options.method      = 'POST'
    options.parse_json ?= true
    options.use_queue  ?= true

    @main url, data, options


  put: (url, data={}, options={}) ->
    options.method      = 'PUT'
    options.parse_json ?= true
    options.use_queue  ?= true

    @main url, data, options


  delete: (url, data={}, options={}) ->
    options.method      = 'DELETE'
    options.parse_json ?= true
    options.use_queue  ?= true

    @main url, data, options



module.exports = {http}