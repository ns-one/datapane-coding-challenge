{pop_js_objects_out_of_the_string, toCamelCase, extend, dup, get_css_selector, variates, type, strip, flatten, distinct, thread, throttle, get_value} = require './extras'
{save_state, __get_scope_by_id, __get_paths, get_root_elements, is_scope, get_used_components, drop_scope, get_scope_el, apply_to_scope, __get_scope_by_el, __get_parent} = require './helper_funcs'
{compose, render_to} = require './rendering'
{queue, Promise} = require './promise'


# dummy body object for test purposes
unless @document
  @document =
    addEventListener: () ->

document = @document


# jsou to pointery na stále stejné objekty
__used_components = get_used_components()
__root_elements   = get_root_elements()


__global_handlers   = {}
set_global_handlers = (handlers) ->
  for event_name, cb of handlers
    __global_handlers[event_name] ?= []
    __global_handlers[event_name].push(cb)


###
  - veme event a použije všechny handlery, které najde:
    - tj. globální registrované,
    - "lokální" na komponentách (jeslti existují),
      která právě mohou provádět lokální změny s prefixem
      "local" - jestliže takové změny existují, je potřeba
                vyhledat danou/dané lokální scopes...

                ...nicméně i ty jsou počítány pouze jednou
                a až následně n-krát aplikovány - tj. scope-
                -specific věci se rozlišují až v jednotlivých
                view...
    - a "self", tj. pouze v dané instanci dané komponenty

  - jestlize zmenou zanikne nejaka scope, je nutne ji i dropnout
  - může se stát, že se vytvoří nový event (fire_event), ten se pak
    klasicky propaguje, ale všechny změny jsou vyrendrovány jen 1x
    (tj. event, který to vše spustí čeká i na navazující eventy)
###
__touched_scopes       = []
__new_events_to_handle = []


# při spouštění handlerů se můžou "venku" objevit další touched_scopes
# (např. při ukládání modelů aj.)
add_touched_scope = (scope) ->
  __touched_scopes.push(scope)


handle_scope_changes = (scope_changes, atts, component=null) ->
  if type(scope_changes) is 'array'
    return (handle_scope_changes(sc, atts, component) for sc in scope_changes)

  else if not scope_changes?
    return

  else if typeof scope_changes.then is 'function'
    return scope_changes.then (error, scope_changes) ->
      unless scope_changes
        return

      handled_sc = handle_scope_changes(scope_changes, atts, component)
      apply_handlers('$async_changes').then ->
        render_touched_scopes('$async_changes')
        handled_sc

  #########################
  #
  # handle classic changes
  #

  # if any new event
  if fire_event = scope_changes.fire_event
    delete scope_changes.fire_event

    # set default target (pro většinu (ne-li všechy) případů pak není
    # potřeba definovat target znovu)
    fire_event.target ?= atts.$event.target
    fire_event.preventDefault  ?= ->
    fire_event.stopPropagation ?= ->
    fire_event.$$is_generated = true
    fire_event.original = atts.$event

    __new_events_to_handle.push [fire_event.type, fire_event]

  # if any self changes....
  if self_changes = scope_changes.self
    delete scope_changes.self

  # if any local changes, we need to extract scopes, to change them
  if local_changes = scope_changes.local
    # do not propagate them in global changes
    delete scope_changes.local

  # first apply global changes
  __touched_scopes.push(apply_to_scope(scope_changes))

  if component
    # second apply local changes to all components
    if local_changes
      for $$scope in component.$$scopes
        __touched_scopes.push(apply_to_scope(local_changes, $$scope))

    # third (last) apply self changes to component, that fired event (if any)
    if self_changes
      unless $el = $(atts.$event.target)
        throw new Error("#{atts.$event.target} is unreachable")

      $$scope = __get_scope_by_el($el)

      while $$scope and $$scope.$$component isnt component
        $$scope = $$scope.$$parent

      if $$scope
        __touched_scopes.push(apply_to_scope(self_changes, $$scope))



call_event = (cb_name, atts={}) ->
  apply_handlers(cb_name, atts)
  render_touched_scopes(cb_name)

window?.call_event = call_event


apply_handlers = (cb_name, atts={}, firing_scope={}) ->
  waiting_for = []

  # apply on global handlers
  for handler in (__global_handlers[cb_name] or [])
    if scope_changes = handler(atts)
      waiting_for.push handle_scope_changes(scope_changes, atts)

  # apply on all used components' global handlers
  for $$component in __used_components
    if scope_changes = $$component.global_handlers?[cb_name]?.apply($$component, [atts])
      waiting_for.push handle_scope_changes(scope_changes, atts, $$component)

  # apply on current component's handler
  $scope = firing_scope
  while $scope
    if scope_changes = $scope.$$component?.handlers?[cb_name]?.apply($scope.$$component, [atts])
      break
    else
      $scope = $scope.$$parent

  if scope_changes
    waiting_for.push handle_scope_changes(scope_changes, atts, $scope.$$component)

  while __new_events_to_handle.length
    # nové eventy přidáváme na konec a taky je od konce odebíráme
    # → tím řešíme zachování stejného pořadí i při kaskadovitém
    # zanoření více eventů do sebe
    #
    event_args = __new_events_to_handle.pop()
    waiting_for.push handle_event_type(event_args...)

  waiting_for = flatten(waiting_for)
  Promise.all(waiting_for...)



render_touched_scopes = (cb_name) ->

  touched_scopes = flatten(__touched_scopes)
  touched_scopes = distinct(touched_scopes)

  # aplikovali jsme už všechny změny, tak můžeme uložit stav
  thread -> save_state()

  # nechame je uz pripadne bezet v dalsim procesu
  __touched_scopes = []

  #
  # UPDATE VIEWPORT
  #
  for $$scope in touched_scopes
    # scope přestane být scope v případě, že ji dropnul parent
    #   tj. parent byl už znovu-vykreslený
    # drop children
    drop_scope($$scope, false)

    # jestli scope už má pointer na $el, tak je potřeba jej odstranit
    # - ten el už nebude existovat, byť bude mít stále stejné id
    delete $$scope.$$el

  # všechny $$scopes, kde proběhly změny, a které nebyly dropnuté,
  # znovu vykreslíme
  #
  touched_scopes = touched_scopes.filter (ts) -> is_scope(ts)
  has_been_rendering = false
  for $$scope in touched_scopes
    # render complete component
    unless has_been_rendering
      fire_pre_render_hooks(cb_name)

    # debugger
    html = compose($$scope.$$component, $$scope)
    $$scope.$$el = render_to(html, get_scope_el($$scope))

    # fire hooks
    has_been_rendering = true


  if has_been_rendering
    fire_post_render_hooks(cb_name)



__pre_render_hooks = []
register_pre_render_hook = (cb) ->
  __pre_render_hooks.push(cb)

fire_pre_render_hooks = (event_name) ->
  for cb in __pre_render_hooks
    cb(event_name)


__post_render_hooks = []
register_post_render_hook = (cb) ->
  __post_render_hooks.push(cb)

fire_post_render_hooks = (event_name) ->
  for cb in __post_render_hooks
    cb(event_name)


_is_handling_event = false
handle_event_type = (eventType, $event, options={}) ->
  # vytvoření mockupu pro $event, aby se s ním lépe pracovalo
  # tento mockup pak putuje dál do call_event a jednotlivých
  # handlerů
  #
  if typeof $event.target isnt 'string'
    $el = $event.target
    $event =
      target: get_css_selector($el)
      type: $event.type

      # interní pojmenování, tj. např. místo 'mousedown' nebo 'touch' je 'click' aj.
      name: eventType

      # link original event (někdy je potřeba)
      original: $event

      # copy keys
      altKey: $event.altKey
      ctrlKey: $event.ctrlKey
      shiftKey: $event.shiftKey

  else
    $el = $($event.target)

  # rozšíření eventu s klavesami (alt, ctrl, shift)
  #
  ext = (ex for ex in ['alt', 'ctrl', 'shift'] when $event["#{ex}Key"])
  ext = variates(ext)

  # bez klaves
  #
  ext.push([])
  ext = (for ex in ext
          ex.unshift('on')
          ex.push(eventType)
          ex
        )
  ext = (toCamelCase(ex.join('-')) for ex in ext)

  while $el?.parentElement        # děláme match eventu, tj.:
    for ex in ext                 #  $event: ctrl + alt + click
      if cbDefs = $el.dataset[ex] #   match: ctrl + alt + click
        break                     #          ctrl +       click
                                  #                 alt + click
    break if cbDefs               #                       click
                                  #

    # už jsme na root elementu - nemůžeme jit výš
    if $el in __root_elements
      break

    $el = $el.parentElement  # jdeme na parentElement, když
                             # jsme nenašli daný $event na
                             # výchozím elementu
                             #

    # např. pro eventy mouse enter/leave je potřeba nastvit
    # strop, protože se vytvářejí z mousemove a nad tímto
    # stropem by bylo spuštěn event leave a hned na to enter
    #
    if options.break_on_el is $el
      break

  # TODO: zapracovat možnost $default při více handlerech (tj. chceme, aby
  #       se provedla defaultní akce (např. označit text dvojklikem), ale
  #       dodatečně chceme i spustit nějaké další akce...)
  #

  # nenašli jsme dany $event a nebo chceme
  # defaultní chování browseru
  #
  if not cbDefs or cbDefs is '$default'
    return

  # $scope, se kterou se pak spouštějí všechny handlery
  #
  $scope = __get_scope_by_el($el)

  # nikdy nezpracováváme dva eventy současně, nikdy! :-D
  #
  if options.dont_process_event or (_is_handling_event and not $event.$$is_generated)
    return true

  # test, jestli náhodou už nějaký cb na event se stejným
  # aliasem už neběží a jestli ano, tak v případě potřeby
  # jej zastavíme
  #
  throttle 50, "events:#{eventType}:#{$event.target}", ->
    # jestliže event nějakým způsobem zpracujeme, nasadíme lock
    unless $event.$$is_generated
      _is_handling_event = true

    process_event($event, $el, $scope, cbDefs).then ->
      unless $event.$$is_generated
        _is_handling_event = false



process_event = ($event, $el, $scope, cbDefs) ->
  # -------------------------------------------------------------------------
  # zpracování $eventů + vytažení proměnných ze scope s následným vykreslením
  #
  #
  # všechny handlery eventy spouštíme sekvenčně - tedy když je nějaký handler async,
  # čekáme než doběhne a teprve až potom spouštíme další; jestliže handler vytvoří
  # virtuální event, jeho handlery se spouštějí rovnou, také sekvenčně a takový handler
  # je dokončený až ve chvíli, kdy všechny handlery jeho virtuálního eventu nebo
  # virtuálních eventů jsou hotovy
  #
  # vykreslení nového stavu probíhá vždy na konci synchronních handlerů
  #
  have_been_prepared = false
  prepare_rendering  = ->
    if not have_been_prepared and not $event.$$is_generated
      have_been_prepared = true
      thread ->
        have_been_prepared = false
        render_touched_scopes(cbDefs.join('; ') + ';')

  # every time there's a nested event, it needs to have different queue_key, than it's parent,
  # to give them random queue_keys is way easier approach (may not be that memory efficient)
  #
  queue_key = String(Math.random())

  # queue si pozastavíme, aby jsme stihli naházet vše do fronty ve správném pořadí
  pauser = queue.pauser(queue_key)

  # zpracováváme opačně kvůli priority queue
  cbDefs = cbDefs.split(';').reverse()
  for cbDef in cbDefs
    do (cbDef) ->
      queue '!' + queue_key, ->
        prepare_rendering()

        # vytažení proměnných i jejich aliasů (param names)
        # $event handlery
        #
        [cb, defAtts...] = cbDef.split(':')
        cb      = strip(cb)
        defAtts = defAtts.join(':')
        atts    = {$event}

        if defAtts?.length
          [defAtts, objs_to_replace] = pop_js_objects_out_of_the_string(defAtts)

          defAtts = defAtts.split(',')

          for att in defAtts
            [key, alias] = att.split(' as ')
            key   = strip(key)
            alias = strip(alias) or key

            if /^(- ?)?\d/.test(key)
              objs_to_replace['$value-number'] = key
              key = '$value-number'

            if key is 'self'
              atts[alias] = $scope

            else if key of {'true', 'false', 'null'}
              atts[alias] = eval(key)

            # celý klíč je $value, tedy získáváme hodnotu daného prvku
            else if key is '$value'
              atts[alias] = get_el_value($el)

            # pouze začátek klíče je $value, klíč je $value0 ... atd., takže to je něco,
            # co jsme museli upravit a dělali jsme tam substituce, aby jsme mohli string
            # bezpečně dělit dle čárek atd.
            #
            else if key[...6] is '$value'
              for k, v of objs_to_replace
                key = key.replace(k, v)

              # eval's trick - e.g. result of eval("{plugs: {slug: 'button'}}") is 'button',
              # but for eval("({plugs: {slug: 'button'}})") is {plugs: {slug: 'button'}}
              #
              atts[alias] = eval("(#{key})")

            else
              atts[alias] = get_value($scope, key)

        apply_handlers(cb, atts, $scope)

  # všechny cbDefs už jsou definovány, můžeme frontu odbrzit a pustit je
  # všechny postupně...
  #
  pauser()

  # vrací promise, který je splněný až po splnění
  # všech async změn vyvolaných původním eventem
  #
  queue.promise queue_key, ->



# vrátí hodnotu elementu, jestli že je elementem form,
# vrátí dict, kde klíčem je name daného inputu a hodnotou
# hodnota inputu tj. například:
# {
#   csrf_token: 'asdf',
#   new_task : {
#     description: 'popisek dalsi',
#     type: 'dalsi'
#   }
# }
#
get_el_value = (el) ->
  unless el instanceof HTMLElement
    throw new Error('el is not instance of HTMLElement')

  else if el.tagName is 'FORM'
    res = {}
    for e in el.querySelectorAll('[name]')
      [paths..., name] = e.getAttribute('name').split('.')
      curr = res
      while paths.length
        p = paths.shift()
        curr[p] ?= {}
        curr     = curr[p]

      if e.tagName is 'INPUT' and e.type in ['radio', 'checkbox']
        if (value = get_el_value(e))?
          if typeof value is 'boolean' or e.type is 'radio'
            curr[name] = value

          else if e.type is 'checkbox'
            curr[name] ?= []
            curr[name].push(value)

      else
        curr[name] = get_el_value(e)

    res

  else if el.tagName is 'INPUT' and el.type is 'checkbox'
    if value = el.getAttribute('value')
      if el.checked
        value
    else
      el.checked

  else if el.tagName is 'INPUT' and el.type is 'radio'
    if value = el.getAttribute('value')
      if el.checked
        value
    else
      throw new Error('input[type="radio"] needs a [value] defined!')

  else if el.tagName in ['INPUT', 'TEXTAREA', 'SELECT']
    el.value

  # linked s jQuery aj., že jen předá hodnotu přes dataset
  else if el.dataset?.value?
    JSON.parse(el.dataset.value)

  # nějaká custom componenta (např. autocomplete nebo "hezký" select)
  else if el.id?
    sc = __get_scope_by_el(el)
    sc.$value

  # cokoli jiného - nejčastěji s contentEditable
  else
    el.innerHTML


# odstranění dodatečných kliknutí
last_event =
  event_source : 'mouse'  # | touch
  timestamp    : new Date


# setup event handlers
#
__create_handler = (eventName, options={}) ->
  document.addEventListener eventName, (event) ->
    # odstraní v PG dodatečné klikání myší po TouchEventu
    # (PG vysílá celkem 3 eventy!!!)
    #
    event_source = if event.touches? then 'touch' else 'mouse'
    timestamp    = new Date

    if last_event.event_source isnt event_source and
      (timestamp - last_event.timestamp) < 1e3
        return

    last_event = {event_source, timestamp}


    # vytvoření konečného názvu eventu
    event_name = options.alias?(event) ? options.alias ? eventName

    # jestli je definována testFn, provedeme test
    #
    if not options.testFn? or !!options.testFn(event)
      if handle_event_type(event_name, event)
        # původní event v případě potřeby zastavíme
        if options.preventDefault
          event.preventDefault()
          event.stopPropagation()


__create_handler 'mousedown',
  alias: 'click'
  preventDefault: true


get_parents = (el) ->
  res = [el]
  while el = el.parentElement
    res.unshift(el)
  res


last_target_selector = get_css_selector(document.body)

document.addEventListener 'mousemove', (event) ->
  new_target = event.target
  new_target_selector = get_css_selector(new_target)
  e_move =
    target: new_target_selector
    type: 'mouse-move'

    # interní pojmenování, tj. např. místo 'mousedown' nebo 'touch' je 'click' aj.
    name: 'mouse-move'

    # link original event (někdy je potřeba)
    original: event

    # copy keys
    altKey: event.altKey
    ctrlKey: event.ctrlKey
    shiftKey: event.shiftKey

    preventDefault: ->


  # jestliže původní target stále existuje (mohl se zrovna změnit DOM) a
  # jeslti nový target je jiný, tak je to teprve regulérní mousemove
  #
  if (last_target = $(last_target_selector)) and last_target isnt new_target
    new_parents  = get_parents(new_target)
    last_parents = get_parents(last_target)

    # najdeme "strop" ~ společného předka
    #
    for el, i in new_parents
      unless el is last_parents[i]
        break_on_el = el
        break

    do (last_target_selector, new_target_selector) ->
      e_enter = extend dup(e_move, false),
        type: 'mouse-enter'
        name: 'mouse-enter'

      e_leave = extend dup(e_move, false),
        target : last_target_selector
        type   : 'mouse-leave'
        name   : 'mouse-leave'

      if handle_event_type('mouse-enter', e_enter, {break_on_el})
        event.preventDefault()

      if handle_event_type('mouse-leave', e_leave, {break_on_el})
        event.preventDefault()

    last_target_selector = new_target_selector

  throttle 50, 'events:mouse-move', ->
    if handle_event_type('mouse-move', e_move)
      event.preventDefault()
      event.stopPropagation()



get_event_coords = (e) ->
  if event = e.changedTouches?[0]
    {pageX, pageY} = event
    [pageX, pageY]

  else
    {layerX, layerY} = e
    [layerX, layerY]


touched   = [-1, -1]
touch_ttl = new Date

__create_handler 'touchstart',
  testFn: (event) ->
    if 100 < ((new Date) - touch_ttl)
      touched = get_event_coords(event)

    false # return always false - waiting for touch end



__create_handler 'touchend',
  alias: 'click'
  testFn: (event) ->
    # když user znovu klikne za menší čas, než touch_ttl,
    # tak to bereme jako omyl
    #
    touch_ttl = new Date

    t = get_event_coords(event)

    delta_x = Math.abs(t[0] - touched[0])
    delta_y = Math.abs(t[1] - touched[1])

    unless delta_x < 10
      return false

    unless delta_y < 10
      return false

    true



__create_handler 'submit',
  preventDefault: true


__create_handler 'keyup'


__create_handler 'keydown',
  preventDefault: true
  alias: (e) ->
    if (alias = get_key(e)) is 'enter'
      # doplnění funkcionality "submit" na Macu
      switch e.target.tagName
        when 'INPUT'
          return 'submit'

        when 'TEXTAREA'
          if e.metaKey or e.ctrlKey
            return 'submit'

    alias


__create_handler 'change',
  preventDefault: true


__create_handler 'input',
  alias: 'change'
  preventDefault: true



get_key = (event) ->
  keyCodes[event.keyCode]


keyCodes =
  8: 'backspace'
  9: 'tab'
  13: 'enter'
  16: 'shift'
  17: 'ctrl'
  18: 'alt'
  19: 'pause/break'
  20: 'caps lock'
  27: 'escape'
  32: 'space'
  33: 'page up'
  34: 'page down'
  35: 'end'
  36: 'home'
  37: 'left'
  38: 'up'
  39: 'right'
  40: 'down'
  45: 'insert'
  46: 'delete'
  48: '0'
  49: '1'
  50: '2'
  51: '3'
  52: '4'
  53: '5'
  54: '6'
  55: '7'
  56: '8'
  57: '9'
  65: 'a'
  66: 'b'
  67: 'c'
  68: 'd'
  69: 'e'
  70: 'f'
  71: 'g'
  72: 'h'
  73: 'i'
  74: 'j'
  75: 'k'
  76: 'l'
  77: 'm'
  78: 'n'
  79: 'o'
  80: 'p'
  81: 'q'
  82: 'r'
  83: 's'
  84: 't'
  85: 'u'
  86: 'v'
  87: 'w'
  88: 'x'
  89: 'y'
  90: 'z'
  91: 'left window key'
  92: 'right window key'
  93: 'select key'
  96: '0'
  97: '1'
  98: '2'
  99: '3'
  100: '4'
  101: '5'
  102: '6'
  103: '7'
  104: '8'
  105: '9'
  106: '*'
  107: '+'
  # 109: '-'
  110: '.'
  111: '/'
  112: 'f1'
  113: 'f2'
  114: 'f3'
  115: 'f4'
  116: 'f5'
  117: 'f6'
  118: 'f7'
  119: 'f8'
  120: 'f9'
  121: 'f10'
  122: 'f11'
  123: 'f12'
  144: 'num lock'
  145: 'scroll lock'
  186: ';'
  187: '='
  188: ','
  # 189: '-'
  190: '.'
  191: '/'
  192: '`'
  219: '['
  220: '\\'
  221: ']'
  222: '\''



module.exports = {call_event, get_el_value, set_global_handlers, apply_handlers, render_touched_scopes, register_pre_render_hook, register_post_render_hook, fire_post_render_hooks, add_touched_scope, __create_handler, handle_event_type, process_event}
