{thread, get_value, type, flatten, distinct} = require './extras'
{warehouse, Model} = require './models'
{storage} = require './contrib'


__state    = {}
__scopes   = {}
window?.sc = __scopes


set_state = (state) ->
  __state    = load_state(state)
  window?.st = __state


load_state = (obj) ->
  res = {}
  # najde-li to model, vytvoří požadovaný (lazy) model
  if typeof obj is 'string' and obj[...7] is '(model)'
    [model, id] = obj[8...].split(':')

    # test, jestli to nahodou neni int id
    if /^\d+$/.test(id)
      id = Number(id)

    warehouse[model].get(id, true)

  # array rozpadne rekurzivně
  else if type(obj) is 'array'
    (load_state(val) for val in obj when val?)

  # objekt taky rozpadne rekurzivně, ale vynechá spec. hodnoty ($)
  else if typeof obj is 'object'
    for key, value of obj when value
      unless value?
        continue
      if key[0] is '$'
        continue
      res[key] = load_state(value)
    res

  else
    obj


serialize_state = (obj) ->
  # uloží reference modelů
  if obj instanceof Model
    if obj.id
      "(model):#{obj.constructor.className}:#{obj.id}"

  # neodchycené modely
  else if obj?.id? and obj?.constructor?.className?
    console.log 'sad', obj.id, obj.constructor.className, obj
    throw new Error 'sad... :('

  else if type(obj) is 'array'
    (serialize_state(val) for val in obj)

  else if typeof obj is 'object'
    res = {}
    for key, val of obj
      unless val?
        continue
      if key[...2] is '$$'
        continue
      res[key] = serialize_state(val)
    res

  else
    obj


save_state = ->
  storage.set 'state', serialize_state(__state)


__get_scope_by_id = ($$id) ->
  unless $$id of __scopes
    throw new Error "(__get_scope_by_id) there's no scope with id: #{$$id}"

  __scopes[$$id]


__root_elements = []
get_root_elements = ->
  get_root_elements


register_root_element = (element) ->
  if element not in __root_elements
    __root_elements.push(element)


__used_components = []
get_used_components = ->
  __used_components


register_scope = (scope) ->
  # add up/down relationship
  scope.$$parent?.$$children.push(scope)

  # register object
  __scopes[scope.$$id] = scope

  # register Component on it's first usage
  if scope.$$component not in __used_components
    __used_components.push(scope.$$component)
    scope.$$component.$$scopes = []

  # add Component relationship
  scope.$$component.$$scopes.push(scope)

  # add Models relationship
  if scope.$model
    scope.$model.$$scopes ?= []
    scope.$model.$$scopes.push(scope)



window?.ds = drop_scope = (scope, drop_self=true) ->
  # throw new Error 'drop scope'
  unless is_scope(scope)
    return  # uz byla dropnuta drive

  # reccursive drop
  for child in scope.$$children
    drop_scope(child)

  if drop_self
    # call $destroy handler if provided
    scope.$$component.handlers?.$destroy?({scope})

    # remove Component relationship
    scope.$$component.$$scopes =
      scope.$$component.$$scopes.filter (s) ->
        s isnt scope

    # remove Models relationship
    scope.$model?.$$scopes =
      scope.$model.$$scopes.filter (s) ->
        s isnt scope

    # remove up/down relationship
    scope.$$parent?.$$children =
      scope.$$parent.$$children.filter (s) ->
        s isnt scope

    # remove pointer
    delete __scopes[scope.$$id]

    # drop all scope-specific attrs
    for key of scope
      if key[...2] is '$$'
        delete scope[key]


__get_scope_by_el = ($el) ->
  unless $el
    throw new Error '(__get_scope_by_el) needs an element'

  $el = __get_parent($el)
  $el.$$scope ?= __get_scope_by_id($el.id)


__get_parent = ($el) ->
  while $el and $el.id not of __scopes
    $el = $el.parentElement
  $el


get_scope_el = (scope) ->
  unless res = scope.$$el ?= $("##{scope.$$id}")
    console.log scope
    throw new Error "scope has no element"
  res


get_scope = (path='') ->
  get_value(__state, path)


__get_paths = (data, path='') ->
  res = []

  if typeof data is 'object'
    for key, value of data
      res = res.concat __get_paths(value, path + '.' + key)

  else
    res.push path

  res


is_scope = (obj) ->
  obj.$$id?

__apply = (changes, obj, last_scope) ->
  touched_scopes = []

  if is_scope(obj)
    last_scope = obj

  for key, value of changes
    if type(value) is 'object' and value not instanceof Model
      # přepíšeme celý objekt
      if type(obj[key]) not in ['object', 'array']
        obj[key] = value

        # změny pro speciální parametry nepropagují render
        if key[0] isnt '$'
          touched_scopes.push(last_scope)

      # jdeme dál do hloubky
      else
        ts = __apply(value, obj[key], last_scope)

        # změny pro speciální parametry nepropagují render
        if key[0] isnt '$'
          touched_scopes.push(ts)

    else
      if obj[key] isnt value
        obj[key] = value

        # změny pro speciální parametry nepropagují render
        if key[0] isnt '$'
          touched_scopes.push(last_scope)

  touched_scopes


apply_to_scope = (changes, scope=null) ->
  scope = scope or get_scope()

  __apply(changes, scope, scope)


module.exports = {get_root_elements, register_root_element, is_scope,
                  register_scope, get_scope_el, drop_scope, __get_scope_by_id,
                  __get_scope_by_el, __get_parent, get_scope, apply_to_scope,
                  set_state, get_used_components, save_state, load_state}
