{register_root_element, set_state, get_scope, is_scope, drop_scope, get_scope_el, get_used_components} = require './helper_funcs'
{thread, debounce, eq, dup, len, fill_default, flatten, uuid} = require './extras'
{fire_post_render_hooks, add_touched_scope, set_global_handlers, render_touched_scopes, apply_handlers} = require './events'
{warehouse} = require './models'
{compose, render_to} = require './rendering'
{queue} = require './promise'
{GLOBALS, Binder, persistent, storage} = require './contrib'


modals_to_show = null
active_modal = null

show_modal = (atts) ->
  if active_modal
    hide_modal(open_next_modal: false)
    modals_to_show.push(active_modal)

  {modal_id} = atts

  active_modal = modal_id
  thread ->
    jQuery("##{modal_id}").modal()

  # update scope
  _modals: {active_modal, modals_to_show}


hide_modal = (atts) ->
  atts.open_next_modal ?= true

  if active_modal
    do (active_modal) -> thread ->
      jQuery("##{active_modal}").modal('hide')

    active_modal = null

  unless atts.open_next_modal
    return

  if modal_id = modals_to_show.pop()
    show_modal({modal_id})

  # update scope
  _modals: {active_modal, modals_to_show}


set_global_handlers
  $init: (atts) ->
    {scope} = atts

    modals_to_show = scope._modals?.modals_to_show or []
    active_modal   = scope._modals?.active_modal

    if modal_id = active_modal
      show_modal({modal_id})

    # update scope
    _modals: {modals_to_show, active_modal}


  soft_reset: ->
    content: null

set_global_handlers {show_modal, hide_modal}



bootstrap = (Component, options={}) -> storage.get('state').then (error, state) ->
  options.state ?= state or {}

  for k, v of (options.globals or {})
    GLOBALS[k] = v

  options.render_params ?= null
  options.selector ?= 'body'
  options.replace ?= false

  # load previous state
  set_state(options.state)
  # let's all init!
  for model_name, model of warehouse
    model.init()

  queue '$init', ->
    apply_handlers('$init', scope: get_scope())

    $element = $(options.selector)
    html = compose(Component, get_scope(), null, options.render_params)

    if options.replace
      $element = render_to(html, $element)
    else
      if $element.childNodes.length
        $element.innerHTML = ''

      child = document.createElement('div')
      $element.appendChild(child)
      $element = render_to(html, child)

    # fire hooks for init
    fire_post_render_hooks('$init')

    register_root_element($element)



# udělá všechny "post render hooky" při změně orientace tabletu/telefonu
window?.addEventListener 'orientationchange', ->
  debounce 100, 'orientation', ->
    fire_post_render_hooks('orientationchange1')

window?.onorientationchange = ->
  debounce 100, 'orientation', ->
    fire_post_render_hooks('orientationchange2')

window?.addEventListener 'resize', ->
  if navigator.vendor?.match(/google/i)
    return

  debounce 100, 'orientation', ->
    fire_post_render_hooks('orientationchange3')

#
# UPDATING VIEWPORT ON ANY MODEL CHANGE
#
binder = new Binder
binder._cb_sid = (callbackName) -> "(model):#{callbackName}"

# reakce na async eventy - sám provádí změny
binder.bind '$ajax_done', (args...) -> thread -> render_touched_scopes('$ajax_done')
binder.bind '$init_done', (args...) -> thread -> render_touched_scopes('$ajax_done')

# reakce na sync eventy - změny provede někdo jiný (je to uvnitř apply_handlers & render_touched_scopes)
__used_components = get_used_components()
binder.bind 'change', (rec, callbackName) -> # thread -> # debounce 50, 'model-binder-changer', ->
  # apply on active components' handlers
  for component in __used_components when component.$$scopes.length
    if should_update = component.should_update

      # backward compatibility approach
      if typeof should_update is 'function'
        # need scope for checking to (not) update
        if should_update.length is 3
          for scope in component.$$scopes
            if should_update(rec, callbackName, scope)
              add_touched_scope(scope)

        else
          if should_update(rec, callbackName)
            add_touched_scope(component.$$scopes)

      # new obj-notation approach
      else
        if should_update is true
          add_touched_scope(component.$$scopes)

        else
          if obj = should_update[rec.constructor.className]
            if obj is true
              add_touched_scope(component.$$scopes)

            else if obj_cb = obj[callbackName]
              if obj_cb is true
                add_touched_scope(component.$$scopes)

              else if typeof obj_cb is 'function'
                if obj_cb.length is 1
                  for scope in component.$$scopes
                    if obj_cb.apply(rec, [scope])
                      add_touched_scope(scope)

                else
                  if obj_cb.apply(rec, [])
                    add_touched_scope(component.$$scopes)

            else if typeof obj is 'function'
              if obj.length is 2
                for scope in component.$$scopes
                  if obj.apply(rec, [callbackName, scope])
                    add_touched_scope(scope)

              else
                if obj.apply(rec, [callbackName])
                  add_touched_scope(component.$$scopes)

  debounce 50, '$change', -> render_touched_scopes('$change')



module.exports = {bootstrap}
