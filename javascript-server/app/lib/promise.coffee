{thread} = require './extras'


class Promise
  # returns a promise which is resolved when all promises are resolved
  # if one of promises returns an error, rejects a returned promise
  #
  # return value is array of results of all given promises
  @all: (promises...) ->
    new Promise (reject, resolve) ->
      unless promises.length
        return resolve()

      results = []
      n = 0

      for promise, i in promises
        do (i) ->
          Promise.wrap(promise).then (error, data...) ->
            n += 1
            if error
              return reject(error)

            results[i] = data

            # validate - jestlize uz failnul drive, tak to stejne
            #            nic neudela
            #
            if n is promises.length
              resolve(results...)


  # returns a promise which is resolved when the first or promises is
  # resolved (resolution may be rejection)
  #
  # return value is a result of first resolved (rejected) promise
  @race: (promises...) ->
    new Promise (reject, resolve) ->
      unless promises.length
        return resolve()

      for promise in promises
        Promise.wrap(promise).then (error, data...) ->
          if error
            return reject(error)

          resolve(data...)


  # returns a promise which is resolved after all callbacks are resolved
  # in sequence
  #
  # return value is array of results of all given promises
  @chain: (promises...) ->
    new Promise (reject, resolve) ->
      unless promises.length
        return resolve()

      result = []

      get_next = (promises) ->
        unless promises.length
          resolve(result...)

        Promise.wrap(promises[0]()).then (error, data...) ->
          if error
            return reject(error)

          result.push(data)
          get_next(promises[1...])

      get_next(promises)


  # when given an instance of Promise or similar, returns promise
  # itself, otherwise returns a new promise with immediate resolution
  @wrap: (promise) ->
    if typeof promise?.then is 'function'
      promise

    else
      new Promise (reject, resolve) ->
        resolve(promise)


  constructor: (fn) ->
    @_callbacks = []
    @status    = 'pending'
    @error     = null
    @data      = []

    fn(@_rejecter, @_resolver)


  _resolver: (data...) =>
    if @status is 'pending'
      @status = 'resolved'
      @data   = data
      @_fire()


  _rejecter: (error) =>
    if @status is 'pending'
      @status = 'rejected'
      @error  = error
      @_fire()


  _fire_cb: (cb) ->
    if @error
      cb(@error, []...)

    else
      # a Promise is being resolved, so we wait for it...
      if @data.length is 1 and typeof @data[0]?.then is 'function'
        @data[0].then(cb)

      else
       cb(null, @data...)


  _fire: ->
    for cb in @_callbacks
      @_fire_cb(cb)


  _stack: (cb) ->
    if @status is 'pending'
      @_callbacks.push(cb)

    else
      @_fire_cb(cb)


  # returns a new promise which is resolved after given callback
  # is resolved with a resolved data of this promise instance
  then: (cb) ->
    result  = undefined
    error   = undefined
    awaited = false

    thread => @_stack (args...) ->
      try
        result = cb(args...)
      catch e
        error = e

        # we want to throw the error if there is no followin .then
        #
        unless awaited
          throw error

    lazy_promise =
      promise: null
      then: (args...) =>
        awaited = true

        unless lazy_promise.promise
          lazy_promise.promise = new Promise (reject, resolve) =>
            @_stack (args...) =>
              @constructor.wrap(result).then (err, args...) ->
                if err ? error
                  reject(err ? error)
                else
                  resolve((args[0]? and args or [result])...)

        lazy_promise.promise.then(args...)


queue = (key, fn) ->
  if has_priority = key[0] is '!'
    key = key[1...]

  queue.__queue_stack ?= {}
  queue.__queue_stack[key] ?= {pending: false, stack: []}
  if queue.__queue_stack[key].pending
    if has_priority
      queue.__queue_stack[key].stack.unshift(fn)
    else
      queue.__queue_stack[key].stack.push(fn)

  else
    queue.__queue_stack[key].pending = true

    fired = false
    cb    = (err, args...) ->
      if err
        throw err

      unless fired
        fired = true
        queue.__queue_stack[key].pending = false
        if fn = queue.__queue_stack[key].stack.shift()
          queue(key, fn)
        else
          delete queue.__queue_stack[key]  # GC

    # if is accepting done param...
    if fn.length is 1
      fn(cb)

    else
      fn()?.then?(cb) or cb()


queue.pauser = (key) ->
  queue_trigger = undefined

  # musí být definováno mimo queue, protože už tam něco může běžet...
  delaying_promise = new Promise (reject, resolve) ->
    queue_trigger = resolve

  # queue that promise
  queue '!' + key, ->
    delaying_promise

  queue_trigger


queue.promise = (key, fn) ->
  new Promise (reject, resolve) ->
    queue key, fn
    queue key, (done) ->
      resolve()
      done()


module.exports = {Promise, queue}