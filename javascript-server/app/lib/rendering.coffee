{is_scope, init_state, get_scope, register_scope, set_global_handlers} = require './helper_funcs'
{apply_to_scope} = require './helper_funcs'
{extend, add_attrs, thread, flatten, len, issubclass} = require './extras'
{GLOBALS} = require './contrib'
{Model} = require './models'
{vdom, render} = require './virtual-dom'


__filters =
  ne: (value) -> "ne-#{value}"
  neco: (value) -> "neco---#{value}"


register_filter = (name, fn) ->
  __filter[name] = fn


$render =
  __is_closing_tag: (tag) ->
    tag not of {'br', 'hr', 'img', 'input'}

  __element: (tag, _params=null, params={}, contents...) ->

    # extend params with _params (params with priority)
    extend_params = (_params) ->
      for attr_name, attr_value of _params
        if attr_name not of params or             # jednoduše rozšíříme, protože neexistuje
                                                  # --
          typeof params[attr_name] is 'boolean'   # když je to true, tak to nic nemění
            params[attr_name] = attr_value        # a když false, tak to stejně chceme
                                                  # přepsat .-)

        # některé typy přepisujeme bez milosti
        else if attr_name in ['id', 'name']
          params[attr_name] = attr_value

        else
          # musíme "nějak" kombinovat
          unless typeof params[attr_name] is 'object'
            # původní attr není objekt - uděláme jej
            tmp_obj = {}
            tmp_obj[params[attr_name]] = true
            params[attr_name] = tmp_obj

          switch typeof attr_value
            when 'object'
              for k, v of attr_value
                # kombinujeme původní objekt (class, style, ...)
                params[attr_name][k] = v

            when 'boolean'
              if attr_value is false
                # vypneme attribut (má přednost)
                delete params[attr_name]

              # jinak neděláme nic

            else
              # doplníme do původního objektu tak, aby se
              # vždy použila
              params[attr_name][attr_value] = true

    # strip out html comments from inner content
    #
    cc = []
    under_comment = false
    for c in contents
      if typeof c is 'string'
        f1 = f2 = true

        while f1? and f2?
          f1 = c.indexOf '<!--'
          f2 = c.indexOf '-->'

          if f1 < 0 then f1 = null
          if f2 < 0 then f2 = null else f2 += 3

          if under_comment
            if f2?
              c = c[f2...]
              under_comment = false

            else
              break
              continue

          else
            if f1? and f2?
              c = c[...f1] + c[f2...]

            else if f1?
              c = c[...f1]
              under_comment = true

      else
        if under_comment
          continue

      cc.push(c)

    if _params
      extend_params(_params)

    # if params.type is 'submit'
    #   delete params.type
    #   extend_params('on-click': 'fire_submit')

    for attr, value of params
      if typeof value is 'object'
        value = (k for k, v of value when !!v).join(
          attr[...3] is 'on-' and '; ' or  # možnost volání více eventů a pak jejich joinování
          ({'style': '; '})[attr] or ' '   # default att's values joint
        )

      if value?.length? and !value.length or !value
        delete params[attr]
        continue

      if attr is 'src' and value[0] is '/'
        value = GLOBALS.data_prefix + value

      params[attr] = String(value)

    for key, value of params
      if key[...3] is 'on-'
        delete params[key]
        params["data-#{key}"] = value

    if params.style
      params.style = add_prefixes(scale_elements(params.style))

    if params.innerHTML
      params.innerHTML = add_prefixes(scale_elements(params.innerHTML))

    vdom.element(tag, params, cc)


  __filter: (filter_key, value) ->
    __filters[filter_key](value)

  __spread: (args...) ->
    res = {}
    for one in args
      for key, value of one
        res[key] = value

    res

  __component: (component, parent, params={}, content...) ->
    unless component
      throw new Error "component is required"

    scope = params.scope ? {}
    delete params.scope

    if scope instanceof Model
      scope = {$model: scope}

    scope.$content = content

    return compose(component, scope, parent, params)



class Component
  @handlers: {}
  @set_handlers: (new_handlers) ->
    @handlers = extend {}, @handlers, new_handlers

  @global_handlers: {}
  @set_global_handlers: (new_handlers) ->
    @global_handlers = extend {}, @global_handlers, new_handlers

  constructor: (scope) ->
    @$$scope = scope
    for k of scope
      do (k) =>
        @__defineGetter__(k, -> scope[k])
        if k[...2] isnt '$$'
          @__defineSetter__(k, (v) -> scope[k] = v)

  template: ->
    throw new Error('template is not defined yet')


$id = 0
__get_new_id = ->
  "s-#{$id++}"



add_prefixes = (content) ->
  content.replace(/\ src=("|')\//ig, " src=\"#{GLOBALS.data_prefix}/")



pattern = /(width|height|size)(=|:)("|'|\s+)?(\d+)(px)?("|'|\s+|>)/
global  = new RegExp(pattern.source, 'ig')
local   = new RegExp(pattern.source, 'i')



scale_elements = (content) ->
  if (px_ratio ? 1) isnt 1
    if matched = content.match(global)
      # od nejdelšího po nejkratší, ev. obráceně,
      # podle toho, jestli budeme prvky zvětšovat,
      # nebo zmenšovat...
      matched.sort()
      if 1 < px_ratio
        matched.reverse()

      for match in matched
        m = match.match(local)
        m = m[1...]  # první matched je celý string
        m[3] = px_ratio * Number(m[3])

        content = content.replace(match, m.join(''))

  content



compose = (component, scope, parent=null, params=null) ->
  # rendering components with no scope defined
  # if len(scope) is 1 and scope.$content?
  #   scope.$$scope    = scope
  #   scope.$$render   = $render
  #   scope.$$params   = [params]
  #   scope.$$parent   = parent
  #   scope.$$children = []

  #   return component.apply(scope)

  # TODO: má přednost id ze scope nebo z params a proč?
  unless is_scope(scope)
    # init params (for id, etc)
    params = params or {}

    # TMP scopes' hotfix
    scope.$$scope     = scope

    scope.$$id        = params.id ?= __get_new_id()
    scope.$$render    = $render
    scope.$$parent    = parent
    scope.$$component = component
    scope.$$children  = []

    register_scope(scope)

  # do pre_render stuff if component requires (only to current scope)
  if scope_changes = scope.$$component.handlers?.$pre_render?.apply(scope.$$component, [{scope}])
    apply_to_scope(scope_changes, scope, false)

  if not params
    params = scope.$$last_used_params

  # id for popping in first element
  params.id ?= scope.$$id

  scope.$$last_used_params = params
  scope.$$params = [params]

  if issubclass(component, Component)
    html = (c = new component(scope)).template()
    # TODO: je to nutné? ...opravdu to potřebujeme aktualizovat i zpětně?
    # + je tu otázka, jestli náhodou nestačí jen toto (místo setters...)
    for key, value of c when key not of scope
      if component::[key]?
        continue

      if typeof value is 'function'
        continue

      scope[key] = value

    html
  else
    (component.template or component).apply(scope)



render_to = (vhtml, el) ->
  parent = el.parentElement
  next   = el.nextSibling

  pe = document.createElement('div')
  pe.appendChild(el)

  render(-> vhtml)(pe)
  el = pe.children[0]

  parent.insertBefore(el, next)
  el


get_dom_element = (vhtml) ->
  parent = document.createElement('div')
  parent.appendChild(el = parent.cloneNode())

  render_to(vhtml, el)



module.exports = {Component, compose, render_to, $render, register_filter, get_dom_element}