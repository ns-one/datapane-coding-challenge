{len} = require '../lib/extras'
{set_global_handlers} = require '../lib/events'
{Component} = require '../lib/rendering'

{FormEntry} = require '../models'



# example of simple component
#
EntryRow = ->
  <tr>
    <td>{ @model.name }</td>
    <td>{ @model.age }</td>
    <td>{ @model.email }</td>
    <td><a on-click="delete_entry: model.id as entry_id">x</a></td>
    <td><a on-click="edit_entry: model.id as entry_id">edit</a></td>
  </tr>


EntryRow.should_update =
  FormEntry:
    update: (scope) -> @id is scope.model.id


# example of global handlers
#
set_global_handlers
  delete_entry: (atts) ->
    {entry_id} = atts

    FormEntry.get(entry_id).delete()
    return



class EntryTable extends Component
  @should_update:
    FormEntry:
      init: true
      create: true
      delete: true

  # example of component-based handlers
  # they are active only when component is used + they can change local state
  #
  @set_handlers
    $pre_render: (atts) ->
      {sort_key, reversed} = atts.scope

      # we don't have to override (sort it) every time, we can leave it untouched
      # in scope, if we would have to deal with performance
      #
      sorted_entries = FormEntry.objects.sort (a) -> a[sort_key]
      if reversed
        sorted_entries = sorted_entries.reverse()

      entries: sorted_entries


    edit_entry: (atts) ->
      {entry_id} = atts

      self:
        new_entry: FormEntry.get(entry_id).attributes()


    create_or_update_entry: (atts) ->
      {id, name, age, email} = atts.$value

      age = Number(age)
      if /^\d+$/.test(id)
        id = Number(id)

      entry = new FormEntry({id, name, age, email})
      validation_errors = entry.validation_errors()
      unless len(validation_errors)
        entry.save()

        self:
          validation_errors: null
          new_entry: null

      else
        [
          self:
            validation_errors: null
        ,
          self:
            validation_errors: validation_errors
            new_entry: {id, name, age, email}
        ]


    sort_by: (atts) ->
      {sort_key, reversed} = atts

      self:
        sort_key: sort_key
        reversed: !!reversed


  template: ->
    <div>
      <form on-submit="create_or_update_entry: $value">
        <table>
          <tr>
            <th on-click={
              "sort_by: 'name' as sort_key": @reversed or @sort_key isnt 'name',
              "sort_by: 'name' as sort_key, true as reversed": @sort_key is 'name' and !@reversed,
            }>
              {if @sort_key is 'name'
                if @reversed
                  <span>^ </span>
                else
                  <span>v </span>
              }
              Name
            </th>

            <th on-click={
              "sort_by: 'age' as sort_key": @reversed or @sort_key isnt 'age',
              "sort_by: 'age' as sort_key, true as reversed": @sort_key is 'age' and !@reversed,
            }>
              {if @sort_key is 'age'
                if @reversed
                  <span>^ </span>
                else
                  <span>v </span>
              }
              Age
            </th>

            <th on-click={
              "sort_by: 'email' as sort_key": @reversed or @sort_key isnt 'email',
              "sort_by: 'email' as sort_key, true as reversed": @sort_key is 'email' and !@reversed,
            }>
              {if @sort_key is 'email'
                if @reversed
                  <span>^ </span>
                else
                  <span>v </span>
              }
              Email
            </th>

            <th colspan="2" />
          </tr>
          {for entry in @entries
            <EntryRow scope={ model: entry } />
          }
          <EntryForm scope={
            input_data        : @new_entry ? {}
            validation_errors : @validation_errors ? {}
          } />
        </table>
      </form>
    </div>



EntryForm = ->
  <tr>
    <td>
      <input type="hidden" name="id" value={ @input_data.id } />
      <input type="text" name="name" value={ @input_data.name } />
      {if error_lines = @validation_errors.name
        <ul>
        {for error in error_lines
          <li>{ error }</li>
        }
        </ul>
      }
    </td>
    <td>
      <input type="number" name="age" value={ @input_data.age } />
      {if error_lines = @validation_errors.age
        <ul>
        {for error in error_lines
          <li>{ error }</li>
        }
        </ul>
      }
    </td>
    <td>
      <input type="email" name="email" value={ @input_data.email } />
      {if error_lines = @validation_errors.email
        <ul>
        {for error in error_lines
          <li>{ error }</li>
        }
        </ul>
      }
    </td>
    <td colspan="2">
      <input type="submit" value="save" />
    </td>
  </tr>



module.exports = {EntryTable}
