{len} = require '../lib/extras'
{set_global_handlers} = require '../lib/events'
{Component} = require '../lib/rendering'

{CsvFile, SearchQuery, SearchQueryResult} = require '../models'



QueryOutput = ->
  console.log 'QueryOutput - render', @query, @query.results.length

  <table>
    <tr>
      {for column in @query.ordered_columns
        <th>{ column }</th>
      }
    </tr>
    {for result in @query.results
      <tr>
        {for cell in result.data
          <td>{ cell }</td>
        }
      </tr>
    }
  </table>

QueryOutput.should_update =
  SearchQueryResult: true



class CsvHandler extends Component
  @set_handlers
    $pre_render: (atts) ->
      query_output: atts.scope.query_output ? {}

    search: (atts) ->
      {query} = atts

      unless query_obj = (SearchQuery.objects.filter (rec) -> rec.query is query)[0]
        query_obj = SearchQuery.create({query})
        for file in CsvFile.objects
          query_obj.csv_files.add(file)

        query_obj.do_search()

      self:
        query_output:
          query: query_obj


  should_update:
    CsvFile: true
    CsvFilePart: true


  template: ->
    <div>
      <h2>files:</h2>
      <ul>
        {for file in CsvFile.objects
          <li>{ "#{file.name}: #{file.lines_count} - #{file.parts.length}" }</li>
        }
      </ul>
      <br />
      <h2>results:</h2>
      <input type="text" on-change="search: $value as query" />
      {if @query_output?.query
        <QueryOutput scope={ @query_output } />
      }
    </div>




module.exports = {CsvHandler}