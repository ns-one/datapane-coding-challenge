{EntryTable} = require './entry_form'
{CsvHandler} = require './csv_handler'



Base = ->
  <div>
    <CsvHandler scope={ @csv_handler ?= {} } />
    <EntryTable scope={ @entry_table ?= {} } />
  </div>



module.exports = {Base}
