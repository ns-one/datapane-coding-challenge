{bootstrap} = require './contrib/boostrap'
{Base}      = require './components'


models = require './models'



class App
  constructor: (options={}) ->
    bootstrap(Base, options)



module.exports = App