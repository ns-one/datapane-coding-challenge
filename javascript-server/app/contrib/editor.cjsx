{dup, add_attrs, thread} = require '../lib/extras'
{is_scope, get_scope_el} = require '../lib/helper_funcs'
{Component}              = require '../lib/rendering'

require './handlers'

MediumEditor = require('medium-editor/dist/js/medium-editor')

EMPTY_STRING = '<p><br></p>'


# TODO: proper editor
#
# _editors = {}
# class Editor extends Component
#   @editor_options: {}

#   @set_handlers
#     $destroy: (atts) ->
#       {scope} = atts
#       scope.editor?.removeElements([scope.get_editor_el()])


#   constructor: ->
#     _key = JSON.stringify(@constructor.editor_options)
#     if @editor = _editors[_key]
#       @editor.addElements([@get_editor_el()])

#     else
#       @editor = _editors[_key] = new MediumEditor(@get_editor_el(), @constructor.editor_options)



#   get_editor_el: ->
#     throw new Error('get_editor_el has to be defined')



# class LineEditor extends Editor
#   get_editor_el: ->
#     get_scope_el(@).querySelector('span')


editor_wrapper = (options={}) ->
  editor = null

  start = (el) ->
    unless editor
      editor = new MediumEditor(el, options)


  class Editor
    onBlur: =>
      content = @get_content()
      if !content.length or content is EMPTY_STRING
        console.log {content}
        el = @el.nextElementSibling
        el?.className = ''

    onFocus: =>
      el = @el.nextElementSibling
      el?.className = 'active'

    get_editor: ->
      editor

    bind_all: ->
      editor.subscribe('blur', @onBlur)
      editor.subscribe('focus', @onFocus)

    unbind_all: ->
      editor.unsubscribe('blur', @onBlur)
      editor.unsubscribe('focus', @onFocus)

    constructor: (@el) ->
      start(@el)
      editor.addElements([@el])
      @bind_all()

      # to set label's position
      @onFocus()
      @onBlur()

    destroy: ->
      @unbind_all()
      editor.removeElements([@el])

    get_content: (strip) ->
      content = editor.getContent(editor.elements.indexOf(@el)).trim()
      if strip
        while content[...3] is '<p>'
          content = content[3...]

        while content[-4...] is '</p>'
          content = content[...-4]

      content


bg = button_generator = (button_name, icon_name) ->
  {
    name: button_name
    contentDefault: "<i class=\"material-icons\">#{icon_name}</i>"
  }



base_options =
  disableExtraSpaces: true
  imageDragging: false

  # TODO: chtělo by to celé najednou přeložit do čj
  # placeholder:
  #   text: 'Pište zde...'

  toolbar:
    buttons: [
      bg 'bold', 'format_bold'
      bg 'italic', 'format_italic'
      bg 'underline', 'format_underline'
      bg 'anchor', 'insert_link'
      bg 'strikethrough', 'format_strikethrough'
      bg 'quote', 'format_quote'
      bg 'orderedlist', 'format_list_numbered'
      bg 'unorderedlist', 'format_list_bulleted'
      'h2'
      'h3'
      'h4'
      bg 'justifyCenter', 'format_align_center'
      bg 'justifyFull', 'format_align_justify'
      bg 'justifyLeft', 'format_align_left'
      bg 'justifyRight', 'format_align_right'
    ]
  paste:
    forcePlainText: true




LineEditor = editor_wrapper(
  add_attrs(
    {}
    base_options
    {toolbar: {buttons: null}}
    disableReturn: true
    toolbar:
      buttons: [
        bg 'bold', 'format_bold'
        bg 'italic', 'format_italic'
        bg 'underline', 'format_underline'
        bg 'anchor', 'insert_link'
        bg 'strikethrough', 'format_strikethrough'
      ]
  )
)


LE = ->
  <span
    on-enter="fire_submit"
    on-change="changed: editor, true as strip"
    innerHTML={@$value and "<p>#{@$value}</p>" or EMPTY_STRING}
    data-placeholder={@label}
  ></span>


LE.handlers =
  $destroy: (atts) ->
    {scope} = atts
    scope.editor?.destroy()


  $pre_render: (atts) ->
    {scope} = atts
    # wait for render
    thread ->
      if is_scope(scope)
        scope.editor = new LineEditor(get_scope_el(scope))
    return


  changed: (atts) ->
    {editor, strip, $event} = atts

    unless $value = editor.get_content(strip)
      $value = EMPTY_STRING
      e = editor.get_editor()
      e.getFocusedElement().innerHTML = $value
      e.selectAllContents()

    self: {$value}


ContentEditor = editor_wrapper(base_options)


CE = ->
  <div
    on-ctrl-enter="fire_submit"
    on-change="changed: editor"
    innerHTML={@$value or EMPTY_STRING}
  ></div>

CE.handlers = dup(LE.handlers)
CE.handlers.$pre_render = (atts) ->
  {scope} = atts
  # wait for render
  thread ->
    if is_scope(scope)
      scope.editor = new ContentEditor(get_scope_el(scope))
  return


module.exports = {LineEditor: LE, ContentEditor: CE}