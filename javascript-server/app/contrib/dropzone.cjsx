{call_event, set_global_handlers, __create_handler} = require '../lib/events'
{debounce, thread} = require '../lib/extras'
{Promise} = require '../lib/promise'
{http} = require '../lib/http'
{GLOBALS} = require '../lib/contrib'
{Component} = require 'lib/rendering'

require './handlers'

{Spinner} = require './spinner'



file_to_data_url = (file) ->
  new Promise (reject, resolve) ->
    reader = new FileReader
    reader.onload = (evt) =>
      resolve(evt.target.result)

    reader.readAsDataURL(file)



prepare_image = (img_url, mimetype='image/jpeg') ->
  max_size = GLOBALS.max_img_side_size

  new Promise (reject, resolve) ->
    img = new Image
    img.src = img_url
    img.onload = =>
      if img.width > max_size
        img.height *= max_size / img.width
        img.width   = max_size

      if img.height > max_size
        img.width *= max_size / img.height
        img.height = max_size

      mainCanvas = document.createElement('canvas')
      mainCanvas.width  = img.width
      mainCanvas.height = img.height

      ctx = mainCanvas.getContext('2d')
      ctx.drawImage(img, 0, 0, img.width, img.height)

      resolve
        url    : mainCanvas.toDataURL mimetype
        width  : img.width
        height : img.height



process_image = (img_file) ->
  new Promise (reject, resolve) ->
    file_to_data_url(img_file).then (error, dataurl) ->
      prepare_image(dataurl, img_file.type).then (error, img_obj) ->
        resolve(img_obj)



__create_handler 'drop',
  preventDefault: true


set_global_handlers
  $init: ->
    thread ->
      # set draggable view na celé window
      is_active = false

      return

      document.ondragover = (event) ->
        event.preventDefault()
        unless is_active
          is_active = true
          call_event('dragenter')

        do (event) -> debounce 100, 'dragover', ->
          # test, jestli se event dragover nepřestal generovat jen kvůli toho,
          # že se člověk zastavil (Chrome bugfix)
          {pageX, pageY} = event
          {innerWidth, innerHeight} = window

          w = Math.min(pageX, (innerWidth - pageX)) / innerWidth
          h = Math.min(pageY, (innerHeight - pageY)) / innerWidth

          threshold = 0.05

          if w < threshold or h < threshold
            is_active = false
            call_event('dragleave')

      # když to náhodou někdo pustí mimo drop-zone
      document.ondrop = (event) ->
        event.preventDefault()
        is_active = false
        call_event('dragleave')
        console.log 'drop - document'



class ImageDropZone extends Component
  @has_camera: navigator.camera?

  template: ->
    <div name="dropzone" style="position: relative;"
      on-click="$default"
      on-submit="fire_change"
      on-drop="drop"
    >
      {if @$content?.length
        @$content
      else if @status is 'processing'
        <Spinner scope={{}} />
      else if @$value
        <img src={@$value.url} style="max-width: 300px; max-height: 300px" />
      else
        <i class="fa fa-fw fa-file-image-o fa-3x"></i>
      }
      {if @has_camera or GLOBALS.dont_use_file_input
        <div on-click="ask_for_picture"
          style="position: absolute; top: 0; right: 0; bottom: 0; left: 0;"
        >&nbsp;</div>
      else
        <input type="file"
          style="opacity: 0; position: absolute; top: 0; right: 0; bottom: 0; left: 0;"
          on-change="change"
          accept="image/*"
          multiple={false}
        />
      }
    </div>


  @handle_file: (file, atts) ->
    console.log '(handle_file)'

    promise = new Promise (reject, resolve) -> thread ->
      process_image(file).then (error, img_obj) ->
        if error
          return reject(error)

        console.log {img_obj}

        resolve
          fire_event:
            type: 'submit'

          self:
            $value: img_obj
            status: 'ready'

    [
      promise,
      fire_event:
        type: 'processing'

      self:
        status: 'processing'
    ]


  @set_handlers
    # drop: (atts) ->
    #   file = atts.$event.dataTransfer.files[0]

    #   [ local:
    #       is_active: false

    #   ]
    #   @handle_file(file, atts)


    ask_for_picture: (atts) ->
      console.log 'ask_for_picture'

      new Promise (reject, resolve) ->
        navigator.camera.getPicture(
          (file_url) ->
            file_url = 'data:iamge/jpeg;base64,' + file_url
            promise  = new Promise (rj, rs) -> thread ->
              prepare_image(file_url).then (error, img_obj) ->
                console.log 'inside prepare_image', error
                if error
                  return rj(error)

                console.log {img_obj}

                rs
                  fire_event:
                    type: 'change'

                  self:
                    $value: img_obj
                    status: 'ready'

            resolve [
              promise,
              fire_event:
                type: 'processing'
              self:
                status: 'processing'
            ]

          ->   # error callback
          {    # options
            quality: 50
            sourceType: navigator.camera.PictureSourceType.PHOTOLIBRARY
            destinationType: navigator.camera.DestinationType.DATA_URL
            encodingType: navigator.camera.EncodingType.JPEG
          }
        )


    change: (atts) ->
      if file = atts.$event.original?.target.files?[0]
        @handle_file(file, atts)


    # dragenter: ->
    #   console.log '(dragenter)'

    #   local:
    #     is_active: true

    # dragleave: ->
    #   console.log '(dragleave)'

    #   local:
    #     is_active: false



module.exports = {ImageDropZone}