{register_post_render_hook} = require '../lib/events'
{bootstrap} = require '../lib/api'

{get_css_selector, throttle, thread} = require '../lib/extras'
{handle_event_type, set_global_handlers} = require '../lib/events'



# věci pro zpětnou kompatibilitu u materializecss a jQuery
#
register_post_render_hook ->
  jQuery(document).trigger('post-render')
  jQuery('select').material_select?()
  jQuery('.parallax').parallax()
  jQuery('.materialize-textarea').trigger('autoresize')

  jQuery('[data-on-sort]').sortable(
    axis     : 'y'
    distance : 5
    handle   : '.sort-handle'
    items    : '[data-sort-id]'

    update   : (event) ->
      sorted_ids = jQuery(@).sortable('toArray', {attribute: 'data-sort-id'})
      @.dataset.value = JSON.stringify(sorted_ids)

      handle_event_type 'sort',
        target: get_css_selector(@)
        type: 'sort'
        name: 'sort'
        original: event
  )
  jQuery('[data-on-sort]').disableSelection()



register_post_render_hook ->
  # odkazy vrámci webu se otevírají ve stejném okně, odkazy vedoucí pryč se otevírají
  # v novém okně
  #
  for anchor in $$('a[href]')
    if 0 <= anchor.href.indexOf('//') and anchor.href.indexOf(location.hostname) < 0
      anchor.setAttribute('target', '_blank')

    else
      anchor.removeAttribute('target')

  # vytváří "pravé menu"
  menu_items = ''
  for anchor, i in $$('a[name]')
    if i is 1 # it's second anchor (first one is #top)
      $('a.arrow_down')?.href = '#' + anchor.name

    if name = anchor.dataset.name
      slug = anchor.name
      menu_items += "<li class=\"right-button\"><a href=\"##{slug}\" title=\"#{name}\"><span class=\"right-nav-title\">#{name}</span></a></li>"

  if menu_items.length
    menu_items = '<ul>' + menu_items + '</ul>'

  nav = $('nav.parallax-nav')
  nav?.innerHTML = menu_items


# to save some selectors time :-)
data_name_anchors = undefined
nav_buttons       = undefined
parallax_boxes    = undefined
nav               = undefined
nav_coords        = undefined

register_post_render_hook ->
  if nav = $('.parallax-nav')
    data_name_anchors = $$('a[name][data-name]')
    nav_buttons       = $$('.parallax-nav a')
    parallax_boxes    = $$('.paralax_inner_splash')
    nav_coords        = {top: 0, height: 0}

    # need some time to get proper dimmensions
    thread ->
      nav_coords = nav.getBoundingClientRect()


document.addEventListener 'scroll', _updateRightNav = -> throttle 100, 'updateRightNav', ->
  # no need to compute anything
  return unless nav

  # update (not)active class for list items
  for anchor, i in data_name_anchors
    if $.get_window_offset(anchor).top <= window.innerHeight / 2
      nav_buttons[i].className = 'active'
      if i
        nav_buttons[i - 1].className = ''

    else
      nav_buttons[i].className = ''

  # update white/black color based on undercover image
  nav.className = 'parallax-nav'
  for parallax in parallax_boxes
    {top, height} = parallax.getBoundingClientRect()

    if nav_coords.top < top < (nav_coords.top + nav_coords.height) or
      nav_coords.top < (top + height) < (nav_coords.top + nav_coords.height) or
      top < nav_coords.top < (top + height) or
      top < (nav_coords.top + nav_coords.height) < (top + height)

        nav.className = 'parallax-nav parallax-nav-white'
        break


register_post_render_hook (event_name) ->
  # alters font-size of headlines to do something...IQ Kuchyně, help!
  for h in (h for h in $$('h1')).concat $$('h2')
    if h.offsetWidth < h.scrollWidth
      fs  = parseFloat(getComputedStyle(h, null).getPropertyValue('font-size'))
      fs /= h.scrollWidth / h.offsetWidth
      h.style.fontSize = fs + 'px'

  # console.log 'post_render_hooks', event_name
  # $('[autofocus]')?.focus()



set_global_handlers
  scroll_to: (atts) ->
    {y, hash} = atts

    scroll_to y, ->
      if hash
        window.location.hash = hash

    return


_scrolling = undefined
_move      = (y0, y1, t0, t1, cb) ->
  _scrolling = thread -> requestAnimationFrame ->
    y = document.body.scrollTop
    t = (new Date).valueOf()

    # TODO: hezčí zápis?
    tt = (t - t0) / (t1 - t0)
    yy = (y1 - y0) * tt + y0

    console.log {tt, yy, y}
    document.body.scrollTop += yy - y

    if tt <= 1
      _move(y0, y1, t0, t1, cb)
      return

    cb?()



scroll_to = (y, cb) ->
  y0 = document.body.scrollTop
  y1 = Math.round(y)
  t0 = (new Date).valueOf()
  t1 = t0 + 500

  _scrolling?()
  _move(y0, y1, t0, t1, cb)



register_post_render_hook ->
  for a in $$('a[href]')
    if hash = a.href.match(new RegExp("(#{window.locationpathname})?#(.*)"))?[2]
      if anchor = $("a[name=\"#{hash}\"]")
        y = $.get_offset(anchor).top

        a.removeAttribute('href')
        a.dataset.onClick = "scroll_to: #{y} as y, '#{hash}' as hash"



_is_scrolled = false
register_post_render_hook (event_name) ->
  # console.log {event_name}
  # if is init event or it's still right after loading
  #
  if event_name in ['$init', '$ajax_done'] and not _is_scrolled
    # if have anchor in url
    #
    if hash = location.hash[1...]
      # find anchor
      #
      if $el = $("[name=\"#{hash}\"]")
        # scroll to anchor
        scroll_to($.get_offset($el).top)
        _is_scrolled = true



{storage} = require 'lib/contrib'


window.empty_it = ->
  for ds_item in warehouse.DataStackItem.objects.dup()
    ds_item.delete()

  for ds in warehouse.DataStack.objects.dup()
    ds.delete()


window.q = (args...) ->


window.rm_state = ->
  storage.set('state', null)


window.show_state = ->
  storage.get('state').then (error, state) ->
    console.log {error, state}



module.exports = {bootstrap}