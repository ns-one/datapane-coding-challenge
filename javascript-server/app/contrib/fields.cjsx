{Component} = require '../lib/rendering'
{extend, uuid} = require '../lib/extras'

{LineEditor, ContentEditor} = require './editor'
{fire_event} = require './handlers'
{ImageDropZone} = require './dropzone'
{Spinner} = require './spinner'



class Field extends Component
  constructor: ->
    super
    @field_id = "f#{uuid()[...5]}"

    # for peace in mind
    @$value   = @get_value()

  get_value: ->
    @$model.input_value ? @$model.init_value

  get_label: ->
    @$model.label ? @$model.field_name



class TextField extends Field
  type: 'text'
  template: ->
    <div class="input-field col s12">
      <input id={@field_id} name={@$model.field_name} type={@type} value={@get_value()} />
      <label for={@field_id}>{@get_label()}</label>
    </div>



class RichTextField extends Field
  @set_handlers
    change: (atts) ->
      $value = @get_value(atts)
      self: {$value}

    submit: (atts) ->
      $value = @get_value(atts)
      self: {$value}

  @get_value: (atts) ->
    {$value} = atts
    $value = @clean_value($value)
    $value = @strip_value($value)

    $value

  @clean_value: (value) ->
    # remove unwanted styles
    strings_to_remove = (value.match(/\ style="[^"]+" ?/g) ? [])
      .map (string) ->
        ending = string.split('"')[2]
        out    = ''

        if match = string.match(/text-align: ?\w+/)?[0]
          out = ' style="' + match + ';"'

        out += ending
        {string, out}

    for {string, out} in strings_to_remove
      value = value.replace(string, out)

    # replace ,. to &nbsp;
    value = value?.replace(/,\./ig, '&nbsp;')
    value



  @strip_value: (value) ->
    value
      .replace(/^(<p>|<\/p>|<br ?\/?>)+\s*/, '')
      .replace(/\s*(<p>|<\/p>|<br ?\/?>)+$/, '')


  template: ->
    <div class="input-field col s12"
      name={@$model.field_name}
    >
      <LineEditor
        id={@field_id}
        class="input"
        on-change="change: $value; fire_change_on_parent"
        on-submit="submit: $value; fire_submit_on_parent"
        scope={
          $value : @get_value()
          label  : @get_label()
        }
      />
      <label for={@field_id}>{@get_label()}</label>
    </div>

  get_value: ->
    @$value = @constructor.strip_value(@$value ? super)
    @$value.replace(/&nbsp;/g, ',.')
    # console.log 'RichTextField - get_value', {id: @$$id, scope: @, global: sc[@$$id]}
    # @$value



class TextareaField extends Field
  template: ->
    <div class="input-field col s12">
      <textarea id={@field_id} class="materialize-textarea" name={@$model.field_name} length={@max_length}>
        {@get_value()}
      </textarea>
      <label for={@field_id}>{@get_label()}</label>
    </div>



class RichTextareaField extends RichTextField
  @strip_value: (value) ->
    value

  template: ->
    <div class="input-field col s12"
      name={@$model.field_name}
    >
      <ContentEditor
        id={@field_id}
        class="input"
        on-change="change: $value; fire_change_on_parent"
        on-submit="submit: $value; fire_submit_on_parent"
        scope={
          $value : @get_value()
          label  : @get_label()
        }
      />
      <label for={@field_id}>{@get_label()}</label>
    </div>



class NumberField extends TextField
  type: 'number'



# TODO attr value
class CheckBoxField extends Field
  template: ->
    <div class="input-field col s12">
      <p class="checkbox">
        <input id={@field_id} name={@$model.field_name} type="checkbox" checked={@get_value()} />
        <label for={@field_id}>{@get_label()}</label>
      </p>
    </div>



class CheckBoxesField extends Field
  @set_handlers
    $pre_render: (atts) ->
      {scope} = atts

      boxes: [
        {$model: field_name: scope.$model.field_name, label: 'ahoj', init_value: true, input_value: 'keyt'}
        {$model: field_name: scope.$model.field_name, label: 'ahojte', init_value: false, input_value: 'key'}
      ]

  template: ->
    <div class="col s12">
      <label>{@get_label()}</label>
      {for box in @boxes
        <CheckBoxField scope={box} />
      }
    </div>



# TODO attr value
class RadioField extends Field
  template: ->
    <p>
      <input class="with-gap" id={@field_id} name={@$model.field_name} type="radio" value={@$model.input_value} checked={@$model.init_value} />
      <label for={@field_id}>{@get_label()}</label>
    </p>



class RadiosField extends Field
  @set_handlers
    $pre_render: (atts) ->
      {scope} = atts

      radios: [
        {$model: field_name: scope.$model.field_name, label: 'ahoj', init_value: true, input_value: 'keyt'}
        {$model: field_name: scope.$model.field_name, label: 'ahojte', init_value: false, input_value: 'key'}
      ]

  template: ->
    <div class="col s12">
      <label>{@get_label()}</label>
      {for radio in @radios
        <RadioField scope={radio} />
      }
    </div>



class SelectField extends Field
  @set_handlers
    $pre_render: (atts) ->
      {scope} = atts

      scope.$model.init_value ?= []

      options: (for option in scope.$model.init_value
        $model:
          field_name: scope.$model.field_name
          init_value: scope.$model.input_value is option[0]
          input_value: option[0]
          label: option[1]
      )


  has_valid_value: ->
    @$model.init_value?.some? (option) => option[0] is @$model.input_value

  template: ->
    # <div class="input-field col s12">
    #   <select name={@$model.field_name}>
    #     <option value="" disabled selected={!@has_valid_value()}>Vyberte</option>
    #     {for option in @options
    #       <option value={option.$model.input_value} selected={option.$model.init_value}>{option.$model.label}</option>
    #     }
    #   </select>
    #   <label>{@get_label()}</label>
    # </div>
    #

    #                    no input-filed !!! CSS
    <div class="col s12" style="margin-top: 1rem">
      <label>{@get_label()}</label>
      <select name={@$model.field_name} class="browser-default">
        <option value="" disabled selected={!@has_valid_value()}>Vyberte</option>
        {for option in @options
          <option value={option.$model.input_value} selected={option.$model.init_value}>{option.$model.label}</option>
        }
      </select>
    </div>



FileField = ->
  <p>FileField</p>



class ImageField extends Field
  @set_handlers
    _update: (atts) ->
      {$value} = atts
      self: {$value, rand: Math.random()}

  template: ->
    <div class="input-field col s12"
      name={@$model.field_name}
    >
      <ImageDropZone
        scope={$value: @get_value()}
        on-change="_update: $value; fire_change_on_parent"
        on-submit="_update: $value; fire_submit_on_parent"
        name={null}
        class="dropzone-field"
      >
        <div class="form-control">
          {if @get_value() is 'processing'
            <Spinner scope={{}} />
          else if image = @get_value()
            <img src={image.url} style={@calculate_styles(image)} />
          else
            <span class="icon icon-image text-muted">no photo yet</span>
          }
        </div>
      </ImageDropZone>
    </div>

  calculate_styles: (image) ->
    padding   = 7
    edge_size = 336 - 2 * padding
    {width, height} = image

    if width > edge_size
      height *= edge_size / width
      width   = edge_size

    if height > edge_size
      width *= edge_size / height
      height = edge_size

    """
      position: absolute;
      top: #{padding + (edge_size - height) / 2}px;
      left: #{padding + (edge_size - width) / 2}px;
      width: #{width}px;
      height: #{height}px;
      background: rgba(13, 183, 191, .5);
      box-shadow: 0 0 15px rgba(0, 0, 0, .5);
    """



class Autocomplete extends Component
  class @Results extends Component
    @set_handlers
      $pre_render: (atts) ->
        items: atts.scope.items ? []


    template: ->
      <ul>
        {for item, i in @items
          <li on-mouse-enter={"select_item: #{i} as index, 0 as diff, items; fire_change;"} class={
            'item': true
            'selected': i is @index
          } style={'font-weight: bold': i is @index}>{item.label}</li>
        }
      </ul>


  @get_items: (query) ->
    # {label: '<display name>', value: <item.id, etc.>}
    throw new Error('Autocomplete.get_items() has to be defined')

  @set_handlers
    $pre_render: (atts) ->
      results:
        items: @get_items(atts.scope.$query ? '')

    filter_items: (atts) ->
      {$query} = atts
      console.log '(filter_items)', {$query}

      self:
        $query: $query
        results:
          items: @get_items($query)
          index: undefined


    select_item: (atts) ->
      {index, diff, items} = atts
      console.log '(select_item)', {index, diff, items}

      unless items.length
        return
          self:
            $value: undefined
            results:
              index: undefined


      _max = items.length - 1
      _min = 0

      index += diff
      if isNaN(index)
        index = (diff < 0) and _max or _min

      if items.length <= index
        index = _min
      else if index < 0
        index = _max

      self:
        $value: items[index].value
        results: {index}


    submit: (atts) ->
      {index} = atts

      if index?
        atts.parent = true
        atts.type   = 'submit'

        extend {}, fire_event(atts),
          self:
            $query: undefined
            results:
              items: undefined


  constructor: ->
    super

    # to throw an error, if not yet defined
    @constructor.get_items('')
    @Results = @constructor.Results


  template: ->
    console.log '(Autocomplete)'

    <div class="autocomplete" name="autocomplete">
      <input autofocus={@autofocus} type="text"
        on-submit="submit: results.index as index"
        on-change="filter_items: $value as $query"
        on-down="select_item: results.index as index, 1 as diff, results.items as items; fire_change_on_parent"
        on-up="select_item: results.index as index, -1 as diff, results.items as items; fire_change_on_parent"
        value={@$query}
        placeholder={@placeholder}
      />
      <@Results scope={@results ?= {}} style="cursor: pointer" on-click="submit: index" />
    </div>



module.exports = {TextareaField, RichTextareaField, TextField, RichTextField, NumberField, CheckBoxField, CheckBoxesField, RadiosField, SelectField, FileField, ImageField, Autocomplete}