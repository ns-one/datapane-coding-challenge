{set_global_handlers} = require '../lib/events'
{get_css_selector}    = require '../lib/extras'
{storage}             = require '../lib/contrib'


fire_event = (atts) ->
  {$event, type, parent} = atts

  target = $event.target
  if parent
    target = get_css_selector($(target).parentElement)

  fire_event: {target, type}


set_global_handlers
  fire_event: fire_event

  fire_submit: (atts) ->
    atts.type = 'submit'
    fire_event(atts)

  fire_change: (atts) ->
    atts.type = 'change'
    fire_event(atts)

  fire_submit_on_parent: (atts) ->
    atts.parent = true
    atts.type   = 'submit'

    fire_event(atts)

  fire_change_on_parent: (atts) ->
    atts.parent = true
    atts.type   = 'change'

    fire_event(atts)

  save_state: ->
    storage.get('state').then (err, state) ->
      storage.__set('state', state)
      return


module.exports = {fire_event}