echo 'prepare app-cov';
cp app app-tmp -R;
find app-tmp/ -name *.coffee -exec coffee -c {} \;;
find app-tmp/ -name *.coffee -exec rm {} \;;
jscoverage app-tmp app-cov;

echo 'prepare tests, which would request coveraged files';
rm test-cov -R;
mkdir test-cov;
cd test;
touch tmp-sh.sh;
find ./ -type d -exec echo "mkdir ../test-cov/{};" >> tmp-sh.sh \;;
find ./ -type f -exec echo "sed 's/\.\.\/app/..\/app-cov/g' {} > ../test-cov/{};" >> tmp-sh.sh \;;

sh tmp-sh.sh;
rm tmp-sh.sh;
cd ..;

echo 'run tests';
./.BnX/mocha/bin/mocha test-cov --compilers coffee:.BnX/coffee-script/register --reporter html-cov --require '.BnX/should' > tmp.html;
cat tmp.html | sed 's/&quot;/"/g' | sed 's/&lt;/</g' | sed 's/&gt;/>/g' > public/coverage.html;
rm tmp.html;
rm app-* -R;
rm test-* -R;
echo 'done, coverage results at http://localhost/hem/coverage.html';
