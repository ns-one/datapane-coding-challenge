// Generated by CoffeeScript 1.10.0
(function() {
  var flatten;

  exports.flatten = flatten = function(array, results) {
    var i, item, len;
    if (results == null) {
      results = [];
    }
    for (i = 0, len = array.length; i < len; i++) {
      item = array[i];
      if (Array.isArray(item)) {
        flatten(item, results);
      } else {
        results.push(item);
      }
    }
    return results;
  };

  exports.toArray = function(value) {
    if (value == null) {
      value = [];
    }
    if (Array.isArray(value)) {
      return value;
    } else {
      return [value];
    }
  };

}).call(this);
