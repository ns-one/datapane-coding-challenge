from django.core.validators import MinValueValidator, MinLengthValidator, RegexValidator
from django.db import models

from contrib.models import SessionUser


class FormEntry(models.Model):
    session_user = models.ForeignKey(SessionUser, on_delete=models.CASCADE)

    name = models.CharField(max_length=50, validators=[RegexValidator(r'\w{3,} \w{3,}')])
    age = models.IntegerField(validators=[MinValueValidator(0)])
    email = models.EmailField()

    class Meta:
        ordering = ('id',)

    @classmethod
    def get_dummy(cls, **kwargs):
        entry, _ = cls.objects.get_or_create(session_user=SessionUser.get_dummy(id=kwargs.get('session_user_id', None)),
                                             name=kwargs.get('name', 'John Mayer'),
                                             age=kwargs.get('age', 42),
                                             email=kwargs.get('email', 'johhny42@gmail.com'))
        return entry
