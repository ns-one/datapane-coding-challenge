from django.conf import settings
from django.test import TestCase

from contrib.models import SessionUser
from drf_utils.tests import Client
from form_entry.models import FormEntry


class FormEntryDifferentSessionsTestCase(TestCase):
    def setUp(self):
        self.client1 = Client(None)
        self.client2 = Client(None)

        client_1_session_user_id = SessionUser.get_user_id_by_session_key(self.client1.session.session_key)
        client_2_session_user_id = SessionUser.get_user_id_by_session_key(self.client2.session.session_key)

        self.client1_records = [
            FormEntry.get_dummy(session_user_id=client_1_session_user_id, age=1),
            FormEntry.get_dummy(session_user_id=client_1_session_user_id, age=2),
        ]

        self.client2_records = [
            FormEntry.get_dummy(session_user_id=client_2_session_user_id, age=3),
            FormEntry.get_dummy(session_user_id=client_2_session_user_id, age=4),
            FormEntry.get_dummy(session_user_id=client_2_session_user_id, age=5),
        ]

    @property
    def _api_endpoint(self):
        return f'{settings.API_BASE.rstrip("/")}/form-entries/'

    def test_clients_can_see_only_their_records(self):
        response1 = self.client1.get(f'{self._api_endpoint}')
        response2 = self.client2.get(f'{self._api_endpoint}')

        self.assertEqual(len(self.client1_records), response1.data['count'])
        self.assertEqual(len(self.client2_records), response2.data['count'])

        for i, entry in enumerate(self.client1_records):
            self.assertEqual(entry.id, response1.data['results'][i]['id'])

        for i, entry in enumerate(self.client2_records):
            self.assertEqual(entry.id, response2.data['results'][i]['id'])

    def test_clients_can_update_only_their_records(self):
        ok_response = self.client1.patch(f'{self._api_endpoint}{self.client1_records[0].id}/', {'age': 8})
        ko_response = self.client1.patch(f'{self._api_endpoint}{self.client2_records[0].id}/', {'age': 8})

        self.assertEqual(200, ok_response.status_code)
        self.assertEqual(404, ko_response.status_code)

        self.assertEqual(8, ok_response.data['age'])

        ok_response = self.client2.patch(f'{self._api_endpoint}{self.client2_records[0].id}/', {'age': 8})
        ko_response = self.client2.patch(f'{self._api_endpoint}{self.client1_records[0].id}/', {'age': 8})

        self.assertEqual(200, ok_response.status_code)
        self.assertEqual(404, ko_response.status_code)

        self.assertEqual(8, ok_response.data['age'])
