from django.db.models import QuerySet

from contrib.models import SessionUser
from drf_utils.views import MultipleRetrieveViewSet
from form_entry.models import FormEntry
from form_entry.serializers import FormEntrySerializer


class FormEntryViewSet(MultipleRetrieveViewSet):
    serializer_class = FormEntrySerializer
    queryset = FormEntry.objects.all()

    def filter_queryset(self, queryset: QuerySet):
        session_key = self.request.session.session_key
        if not session_key:
            self.request.session.save()
            session_key = self.request.session.session_key
        session_user_id = SessionUser.get_user_id_by_session_key(session_key)
        queryset = queryset.filter(session_user_id=session_user_id)
        return queryset
