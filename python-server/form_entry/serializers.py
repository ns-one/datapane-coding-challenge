from contrib.models import SessionUser
from drf_utils.serializers import IdModelSerializer
from form_entry.models import FormEntry


class FormEntrySerializer(IdModelSerializer):
    class Meta:
        model = FormEntry
        fields = (
            'id',
            'name',
            'age',
            'email',
        )

    def create(self, validated_data):
        session_user_id = SessionUser.get_user_id_by_session_key(self.context['request'].session.session_key)
        validated_data['session_user_id'] = session_user_id

        instance = FormEntry.objects.create(**validated_data)
        return instance
