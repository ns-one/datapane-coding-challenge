from django.db import models
from django.db.models.signals import post_delete, post_save
from django.dispatch import receiver

from redis_utils.helpers import get_redis_connection


redis = get_redis_connection()


SESSION_USER_REDIS_PREFIX = 'session_key:'


class SessionUser(models.Model):
    session_key = models.TextField()

    @classmethod
    def get_user_id_by_session_key(cls, session_key):
        try:
            session_user_id = redis[f'{SESSION_USER_REDIS_PREFIX}{session_key}']
        except KeyError:
            user = cls.objects.create(session_key=session_key)
            session_user_id = user.id

        return int(session_user_id)

    @classmethod
    def get_dummy(cls, **kwargs):
        user, _ = cls.objects.get_or_create(id=kwargs.get('id', None))
        return user

    def save(self, *args, **kwargs):
        if self.id:
            raise ValueError('`SessionUser` cannot be updated')
        return super(SessionUser, self).save(*args, **kwargs)


@receiver([post_save, post_delete], sender=SessionUser)
def delete_user_groups(sender, instance: SessionUser = None, signal=None, **kwargs):
    if signal is post_save:
        redis[f'{SESSION_USER_REDIS_PREFIX}{instance.session_key}'] = instance.id

    elif signal is post_delete:
        del redis[f'{SESSION_USER_REDIS_PREFIX}{instance.session_key}']
