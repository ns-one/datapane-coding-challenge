from django.contrib.auth.models import User


SUPERUSER_EMAIL = 'admin@admin.com'
SUPERUSER_PASSWORD = 'admin'


def create_dev_superuser():
    if not User.objects.filter(username=SUPERUSER_EMAIL).exists():
        User.objects.create_superuser(SUPERUSER_EMAIL, SUPERUSER_EMAIL, SUPERUSER_PASSWORD)
    return User.objects.get(username=SUPERUSER_EMAIL)
