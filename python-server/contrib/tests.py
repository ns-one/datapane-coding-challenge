from django.conf import settings
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.test import TestCase

from contrib.helpers import create_dev_superuser, SUPERUSER_EMAIL, SUPERUSER_PASSWORD
from contrib.models import SessionUser, SESSION_USER_REDIS_PREFIX
from redis_utils.helpers import TestCaseWithRedis

API_BASE = settings.API_BASE


class SuperUserTestCase(TestCase):
    def setUp(self):
        self.superuser = create_dev_superuser()

    def test_user_is_superuser(self):
        self.assertTrue(self.superuser.is_superuser)
        self.assertTrue(self.superuser.is_staff)
        self.assertTrue(self.superuser.is_active)

    def test_we_know_his_credentials(self):
        user = authenticate(username=SUPERUSER_EMAIL, password=SUPERUSER_PASSWORD)
        self.assertIsInstance(user, User)
        self.assertEqual(self.superuser.id, user.id)


class SessionUserTestCase(TestCaseWithRedis):
    def setUp(self):
        super(SessionUserTestCase, self).setUp()

        self.user = SessionUser.get_dummy()

    def test_when_none_as_id_is_provided_no_error_is_thrown(self):
        user = SessionUser.get_dummy(id=None)
        self.assertIsInstance(user, SessionUser)

    def test_session_user_create(self):
        session_user_id = self.redis[f'{SESSION_USER_REDIS_PREFIX}{self.user.session_key}']
        self.assertEqual(self.user.id, int(session_user_id))

    def test_session_user_delete(self):
        self.user.delete()
        self.assertEqual(0, len(self.redis.keys('*')))

    def test_get_session_user_id_by_session_key(self):
        session_user_id = SessionUser.get_user_id_by_session_key(self.user.session_key)
        self.assertEqual(self.user.id, session_user_id)

    def test_create_new_session_user_when_asked_for_non_existing_session_key(self):
        self.assertEqual(1, SessionUser.objects.count())
        fake_session_key = 'random-string'
        new_session_user_id = SessionUser.get_user_id_by_session_key(fake_session_key)
        self.assertEqual(2, SessionUser.objects.count())
        self.assertEqual(fake_session_key, SessionUser.objects.get(pk=new_session_user_id).session_key)
