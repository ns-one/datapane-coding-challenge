from rest_framework import serializers

from contrib.models import SessionUser
from csv_handler.models import CsvFile, CsvFilePart
from drf_utils.serializers import IdModelSerializer, IdPrimaryKeyRelatedField


class CsvFileSerializer(IdModelSerializer):
    lines_count = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = CsvFile
        # TODO: dat id pryc? jo nebo ne?
        fields = ('id', 'name', 'lines_count')

    @staticmethod
    def get_lines_count(instance: CsvFile):
        return instance.lines_count

    def create(self, validated_data):
        session_user_id = SessionUser.get_user_id_by_session_key(self.context['request'].session.session_key)
        validated_data['session_user_id'] = session_user_id

        instance = CsvFile.objects.create(**validated_data)
        return instance


class CsvFilePartSubSerializer(IdModelSerializer):
    lines_count = serializers.SerializerMethodField(read_only=True)
    file = serializers.FileField(write_only=True)
    file_path = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = CsvFilePart
        fields = ('id', 'file', 'file_path', 'lines_count',)

    @staticmethod
    def get_lines_count(instance: CsvFilePart):
        return instance.lines_count

    def get_file_path(self, instance: CsvFilePart):
        paths = instance.file.name.split('/')
        abs_uri = self.context['request'].build_absolute_uri()
        host_with_scheme = '/'.join(abs_uri.split('/')[:3])

        return f'{host_with_scheme}/{paths[0]}/{paths[2]}/{"/".join(paths[3:])}'


class CsvFilePartSerializer(CsvFilePartSubSerializer):
    class Meta(CsvFilePartSubSerializer.Meta):
        fields = CsvFilePartSubSerializer.Meta.fields + ('wrapper',)

    def get_fields(self, *args, **kwargs):
        fields = super(CsvFilePartSerializer, self).get_fields(*args, **kwargs)

        session_key = self.context['request'].session.session_key
        if not session_key:
            self.context['request'].session.save()
            session_key = self.context['request'].session.session_key
        session_user_id = SessionUser.get_user_id_by_session_key(session_key)

        fields['wrapper_id'].queryset = CsvFile.objects.filter(session_user_id=session_user_id)
        return fields
