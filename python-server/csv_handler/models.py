import os

from django.db import models

from contrib.models import SessionUser


def get_upload_path(instance: 'CsvFilePart', filename):
    return os.path.join(
        'uploads',
        instance.wrapper.session_user.session_key,
        instance.wrapper.name,
        str(CsvFilePart.objects.last().id + 1) + '.' + filename.split('.')[-1])


class CsvFile(models.Model):
    session_user = models.ForeignKey(SessionUser, on_delete=models.CASCADE)
    name = models.SlugField()
    _lines_count = models.IntegerField(null=True)

    class Meta:
        ordering = ('name',)
        unique_together = (('session_user', 'name'),)

    def __str__(self):
        return self.name

    @property
    def lines_count(self):
        if self._lines_count is None:
            self._lines_count = sum(part.lines_count for part in self.parts.all())
            self.save()

        return self._lines_count


class CsvFilePart(models.Model):
    wrapper = models.ForeignKey(CsvFile, on_delete=models.CASCADE, related_name='parts')
    file = models.FileField(upload_to=get_upload_path)
    _lines_count = models.IntegerField(null=True)

    class Meta:
        ordering = ('id',)

    def __str__(self):
        return f'{self.wrapper}:{self.id}'

    @property
    def lines_count(self):
        if self._lines_count is None:
            self._lines_count = len(self.file.readlines())
            self.save()

        return self._lines_count
