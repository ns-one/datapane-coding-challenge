# Generated by Django 2.2 on 2019-11-28 20:18

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('contrib', '0006_sessionuser'),
        ('csv_handler', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='csvfile',
            options={'ordering': ('name',)},
        ),
        migrations.AlterModelOptions(
            name='csvfilepart',
            options={'ordering': ('id',)},
        ),
        migrations.AlterUniqueTogether(
            name='csvfile',
            unique_together={('session_user', 'name')},
        ),
    ]
