from django.db.models import QuerySet

from contrib.models import SessionUser
from csv_handler.models import CsvFile, CsvFilePart
from csv_handler.serializers import CsvFileSerializer, CsvFilePartSerializer, CsvFilePartSubSerializer
from drf_utils.views import ReadOnlyMultipleRetrieveViewSet, MultipleRetrieveViewSet


class CsvFileView(MultipleRetrieveViewSet):
    serializer_class = CsvFileSerializer
    queryset = CsvFile.objects.all()

    def filter_queryset(self, queryset: QuerySet):
        session_key = self.request.session.session_key
        if not session_key:
            self.request.session.save()
            session_key = self.request.session.session_key
        session_user_id = SessionUser.get_user_id_by_session_key(session_key)

        queryset = queryset.filter(session_user_id=session_user_id)
        return queryset


class CsvFilePartSubViewSet(ReadOnlyMultipleRetrieveViewSet):
    serializer_class = CsvFilePartSubSerializer
    queryset = CsvFilePart.objects.all()

    def filter_queryset(self, queryset: QuerySet):
        session_key = self.request.session.session_key
        if not session_key:
            self.request.session.save()
            session_key = self.request.session.session_key
        session_user_id = SessionUser.get_user_id_by_session_key(session_key)

        wrapper_id = self.request.query_params.get('wrapper_id', None)
        if not wrapper_id:
            return queryset.none()

        queryset = queryset.filter(wrapper__session_user_id=session_user_id, wrapper_id=wrapper_id)
        return queryset


class CsvFilePartViewSet(CsvFilePartSubViewSet, MultipleRetrieveViewSet):
    serializer_class = CsvFilePartSerializer
