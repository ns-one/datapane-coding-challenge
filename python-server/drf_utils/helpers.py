import json

from rest_framework.renderers import JSONRenderer


def jsonable_dict(data):
    """
    When using Django REST Framework, we're obtaining `validated_data` in `create` and `update`
    view methods. These `validated_data` are not serializable by native JSON library, so we are
    passing the data through DRF's serializer.
    """
    return json.loads(JSONRenderer().render(data).decode('utf-8'))


def string_to_bool(string):
    """
    When sending bool values via JavaScript, they are usually lower-case, while we're not supporting
    others, than Python style `True` or `False` values.
    """
    return {'true': True, 'false': False, 'null': None}[string.lower()]
