#!/bin/bash

set -e

# Wait for Postgres
until PGPASSWORD=${DB_PASSWORD} psql -h ${DB_HOST} -U ${DB_USERNAME} -c '\q' ${DB_NAME}; do
  echo "Postgres is unavailable - sleeping"
  sleep 1
done

# Commands available using `docker-compose run backend [COMMAND]`
case "$1" in
    python)
        echo "Run migrations and collect static files"
        /venv/bin/python manage.py migrate
        /venv/bin/python manage.py collectstatic --no-input

        /venv/bin/python manage.py shell
    ;;
    test)
        /venv/bin/python manage.py test
    ;;
    *)
        # TODO: gunicorn
        echo "Run migrations and collect static files"
        /venv/bin/python manage.py migrate
        /venv/bin/python manage.py collectstatic --no-input

        echo "Running Server..."
        /venv/bin/python manage.py runserver ${APP_HOST}:${APP_PORT}
    ;;
esac
