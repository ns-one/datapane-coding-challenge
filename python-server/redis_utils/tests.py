import pickle
import random
from importlib import import_module

from django.conf import settings

from redis_utils.helpers import TestCaseWithRedis


class RedisCacheTestCase(TestCaseWithRedis):
    def setUp(self):
        super(RedisCacheTestCase, self).setUp()
        engine = import_module(settings.SESSION_ENGINE)
        self.session_store = engine.SessionStore()

    def test_set_and_get_session_values(self):
        random_number = random.random()

        self.session_store['test_key'] = 'test_value'
        self.session_store['random_number'] = random_number

        self.session_store.save()
        session_key = self.session_store.session_key

        pickled_session = self.redis.get(':1:django.contrib.sessions.cached_db{}'.format(session_key))
        saved_session = pickle.loads(pickled_session)

        self.assertTrue(type(saved_session) is dict)
        self.assertDictEqual(saved_session, {
            'test_key': 'test_value',
            'random_number': random_number,
        })
