import redis

from django.conf import settings
from django.test import TestCase
from redis_cache.utils import parse_connection_kwargs as _p


def parse_connection_kwargs(location):
    kwargs = _p(location)
    for k, v in kwargs.items():
        if not v:
            kwargs[k] = None

    return kwargs


def get_redis_connection(redis_location=settings.REDIS_LOCATION) -> redis.StrictRedis:
    kwargs = parse_connection_kwargs(redis_location)
    return redis.StrictRedis(**kwargs)


class TestCaseWithRedis(TestCase):
    def __init__(self, *args, **kwargs):
        self.redis: redis.StrictRedis = None
        super(TestCaseWithRedis, self).__init__(*args, **kwargs)

    def setUp(self):
        self.redis = get_redis_connection()

    def tearDown(self):
        self.redis.flushdb()
