from drf_utils.serializers import IdModelSerializer
from general.models import Page


class PageSerializer(IdModelSerializer):
    class Meta:
        model = Page
        fields = (
            'id',
            'name',
            'url_path',
            'order',
        )
