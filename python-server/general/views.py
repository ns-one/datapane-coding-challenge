from drf_utils.views import ReadOnlyMultipleRetrieveViewSet
from general.models import Page
from general.serializers import PageSerializer


class PageViewSet(ReadOnlyMultipleRetrieveViewSet):
    serializer_class = PageSerializer
    queryset = Page.objects.all()
