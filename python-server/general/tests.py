from drf_utils.tests import GenericLookupsTestCase
from general.models import Page


class LookupsTestCase(GenericLookupsTestCase):
    def setUp(self):
        super(LookupsTestCase, self).setUp()

        self.entity = Page.get_dummy()
        self.api_endpoint = '/pages/'
