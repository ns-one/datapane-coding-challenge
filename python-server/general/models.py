from django.db import models


class Page(models.Model):
    name = models.CharField(max_length=50, unique=True)
    url_path = models.SlugField(unique=True)
    order = models.IntegerField()

    class Meta:
        ordering = ('id',)

    @classmethod
    def get_dummy(cls, **kwargs):
        page, _ = cls.objects.get_or_create(name=kwargs.get('name', 'Dummy page'),
                                            url_path=kwargs.get('url_path', 'dummy-page'),
                                            order=kwargs.get('order', 0))
        return page

    def __str__(self):
        return f'/{self.url_path}/: {self.name}'
